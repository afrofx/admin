@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllManufacturers') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item active">{{ trans('labels.Manufacturer') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                       <b> Lista de Fabricantes/Marcas</b>
                    </div>
                    <div class="card-tools right">
                        <a href="{{ URL::to('admin/manufacturers/add') }}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{trans('labels.ID')}}</th>
                                    <th>{{trans('labels.Name')}}</th>
                                    <th>{{trans('labels.Image')}}</th>
                                    <th>{{trans('labels.OtherInfo')}}</th>
                                    <th>{{trans('labels.Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(count($manufacturers)>0)
                                        @foreach ($manufacturers  as $key=>$manufacturer)
                                            <tr>
                                                <td>{{ $manufacturer->id }}</td>
                                                <td>{{ $manufacturer->name }}</td>
                                                <td><img src="{{asset($manufacturer->path)}}" alt="" width=" 100px"></td>
                                                <td>
                                                    <strong>{{ trans('labels.URL') }}: </strong>{{ $manufacturer->url }} <br>
                                                </td>
                                                <td>
                                                    <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="{{ URL::to('admin/manufacturers/edit/'.$manufacturer->id)}}" class="btn btn-secondary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a id="manufacturerFrom" manufacturers_id='{{ $manufacturer->id }}' data-toggle="tooltip" data-placement="bottom" title="Delete" data-href="{{url('admin/manufacturers/delete')}}" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            @if($manufacturers != null)
                                <div class="col-md-12 text-right">
                                    {{$manufacturers->links()}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- deleteManufacturerModal -->
    <div class="modal fade" id="manufacturerModal" tabindex="-1" role="dialog" aria-labelledby="deleteManufacturerModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteManufacturerModalLabel">{{ trans('labels.DeleteManufacturer') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/manufacturers/delete', 'name'=>'deleteManufacturer', 'id'=>'deleteManufacturer', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                    {!! Form::hidden('manufacturers_id',  '', array('class'=>'form-control', 'id'=>'manufacturers_id')) !!}
                    <div class="modal-body">
                        <p>{{ trans('labels.DeleteManufacturerText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection

