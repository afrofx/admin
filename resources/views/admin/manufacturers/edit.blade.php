@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditCurrentManufacturer') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item"><a href="{{ URL::to('admin/listingManufacturer')}}">{{ trans('labels.Manufacturer') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.EditManufacturer') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/manufacturers/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title"><b>{{ trans('labels.EditManufacturerInfo') }} </b></h3>
                    </div>
                    
                    <div class="card-body">
                        @if (session('update'))
                            <div class="alert alert-success alert-dismissable custom-success-card" style="margin: 15px;">
                                <a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>
                                <strong> {{ session('update') }} </strong>
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif
                        
                        {!! Form::hidden('id',  $editManufacturer->id , array('class'=>'form-control', 'id'=>'id')) !!}
                        {!! Form::hidden('oldImage',  $editManufacturer->image , array('id'=>'oldImage')) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Name') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('name',  $editManufacturer->name , array('class'=>'form-control field-validate', 'id'=>'name'), value(old('name'))) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ManufacturerNameExample') }}</span>
                                       
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.slug') }} </label>
                                    <div class="col-md-12">
                                        <input type="hidden" name="old_slug" value="{{$editManufacturer->slug}}">
                                        <input type="text" name="slug" class="form-control field-validate" value="{{$editManufacturer->slug}}">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.slugText') }}</span>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ManufacturerURL') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('manufacturers_url',  $editManufacturer->url , array('class'=>'form-control', 'id'=>'manufacturers_url'), value(old('manufacturers_url')))  !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ManufacturerURLText') }}</span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }}</label>
                                    <div class="col-md-12">
                                        <div class="modal fade embed-images" id="Modalmanufactured" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                        <button type="button" class="close" data-bs-dismiss="modal" id ="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body manufacturer-image-embed">
                                                        @if(isset($allimage))
                                                            <select class="image-picker show-html " name="image_id" id="select_img">
                                                                <option  value=""></option>
                                                                @foreach($allimage as $key=>$image)
                                                                    <option data-img-src="{{asset($image->path)}}"  class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                                @endforeach
                                                            </select>
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary btn-flat pull-left" >{{ trans('labels.Add Icon') }}</a>
                                                        <button type="button" class="btn btn-default btn-flat refresh-image"><i class="fa fa-refresh"></i></button>
                                                        <button type="button" class="btn btn-success btn-flat" id="selectedICONE" data-bs-dismiss="modal">{{ trans('labels.Done') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id ="imageselected">
                                            {!! Form::button(trans('labels.Add Image'), array('id'=>'newImage','class'=>"btn btn-primary btn-flat", 'data-bs-toggle'=>"modal", 'data-target'=>"#Modalmanufactured" )) !!}
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 3px;">{{ trans('labels.LanguageFlag') }}</span>
                                            <div class="closimage">
                                                <button type="button" class="close pull-right" id="image-close" style="display: none; position: absolute;left: 91px; top: 54px; background-color: black; color: white; opacity: 2.2;" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div  id="selectedthumbnail"></div>
                                            <br>
                                            {!! Form::hidden('oldImage', $editManufacturer->image, array('id'=>'oldImage')) !!}
                                            @if(($editManufacturer->path!== null))
                                                <img width="80px" src="{{asset($editManufacturer->path)}}">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        <a href="{{ URL::to('admin/manufacturers/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
@section('addjs')
    <script>
        $("#select_img").imagepicker()
     </script>
@endsection
