@extends('admin.layout')
@section('title-dash')Lista de Países...@endsection
@section('title-link') 
<li class="breadcrumb-item active">Lista de Países</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/countries/filter')}}">
                            <input type="hidden"  value="{{csrf_token()}}">
                            <div class="input-group search-panel ">
                                <select type="button" class="btn btn-default btn-flat dropdown-toggle form-control" data-bs-toggle="dropdown" name="FilterBy" id="FilterBy"  >
                                    <option value="" selected disabled hidden>{{trans('labels.Filter By')}}</option>
                                    <option value="CountryName"  @if(isset($name)) @if  ($name == "CountryName") {{ 'selected' }} @endif @endif>{{trans('labels.Name')}}</option>
                                    <option value="ISOCode2" @if(isset($name)) @if  ($name == "ISOCode2") {{ 'selected' }}@endif @endif>{{trans('labels.ISOCode2')}}</option>
                                    <option value="ISOCode3" @if(isset($name)) @if  ($name == "ISOCode3") {{ 'selected' }}@endif @endif>{{trans('labels.ISOCode3')}}</option>
                                </select>
                                <input type="text" class="form-control input-group" name="parameter" placeholder="Pesquisar..." id="parameter" @if(isset($param)) value="{{$param}}" @endif >
                                <span class="input-group-append">
                                    <button class="btn btn-primary btn-flat " id="submit" type="submit"><span class="fa fa-search"></span></button>
                                    @if(isset($param,$name))  <a class="btn btn-danger btn-flat " href="{{url('admin/countries/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-tools">
                        <a href="{{url('admin/countries/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>@sortablelink('countries_id', trans('labels.ID') )</th>
                                        <th>@sortablelink('countries_name', trans('labels.CountryName') )</th>
                                        <th>@sortablelink('countries_iso_code_2', trans('labels.ISOCode2') )</th>
                                        <th>@sortablelink('countries_iso_code_3', trans('labels.ISOCode3') )</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(count($countryData['countries'])>0)
                                        @foreach ($countryData['countries'] as $key=>$countries)
                                            <tr>
                                                <td>{{ $countries->countries_id }}</td>
                                                <td>{{ $countries->countries_name }}</td>
                                                <td>{{ $countries->countries_iso_code_2 }}</td>
                                                <td>{{ $countries->countries_iso_code_3 }}</td>
                                                @php $id =$countries->countries_id;   @endphp
                                                <td>
                                                    <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{url('admin/countries/edit',$id)}}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a  data-bs-toggle="tooltip" data-placement="bottom" title=" {{ trans('labels.Delete') }}" id="deleteCountryId" countries_id ="{{ $countries->countries_id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            @if($countryData['countries'] != null)
                                <div class="col-md-12 text-right">
                                    {{$countryData['countries']->links()}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 

    <div class="modal fade" id="deleteCountryModal" tabindex="-1" role="dialog" aria-labelledby="deleteCountryModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteCountryModalLabel">{{ trans('labels.DeleteCountry') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/countries/delete', 'name'=>'deleteCountry', 'id'=>'deleteCountry', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'countries_id')) !!}
                <div class="modal-body">
                    <p>{{ trans('labels.DeleteCountryText') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                    <button type="submit" class="btn btn-primary btn-flat" id="deleteCountry">{{ trans('labels.DeleteCountry') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
