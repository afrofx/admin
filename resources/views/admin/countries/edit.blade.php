@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditCountry') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/countries/display')}}">{{ trans('labels.ListingCountries') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.EditCountry') }} </li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/countries/update', 'method'=>'post', 'class' => 'form-horizontal field-validat', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.EditCountry') }}</b></h3>
                </div>
                
                <div class="card-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session()->get('message') }}
                        </div>
                    @endif

                    <div class="row">
                        {!! Form::hidden('id',  $country->countries_id , array('class'=>'form-control', 'id'=>'id')) !!}
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.CountryName') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('countries_name', $country->countries_name, array('class'=>'form-control field-validat', 'id'=>'countries_name'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                        {{ trans('labels.CountryNameText') }}</span>
                                </div>
                            </div>
                        </div>
                            
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.ISOCode2') }}
                                </label>
                                <div class="col-md-12">
                                    {!! Form::text('countries_iso_code_2', $country->countries_iso_code_2, array('class'=>'form-control field-validat', 'id'=>'countries_iso_code_2'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ISOCode2Text') }}</span>
                                </div>
                            </div>
                        </div>
                            
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.ISOCode3') }}
                                </label>
                                <div class="col-md-12">
                                    {!! Form::text('countries_iso_code_3', $country->countries_iso_code_3, array('class'=>'form-control field-validat', 'id'=>'countries_iso_code_3'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ISOCode3Text') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{ URL::to('admin/countries/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
