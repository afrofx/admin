@extends('admin.layout')
@section('title-dash'){{ trans('labels.pushNotification') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.pushNotification') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.PushNotificationOnesignal') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-6" hidden>
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.defaultNotification') }}</label>
                                <div class="col-md-12">
                                    <select name="default_notification" id="default_notification" class="form-control">
                                        <option @if($result['commonContent']['setting']['linked_in'] == 'fcm') selected @endif value="fcm"> {{ trans('labels.fcm') }}</option>
                                        <option @if($result['commonContent']['setting']['linked_in'] == 'onesignal') selected @endif value="onesignal" selected> {{ trans('labels.onesignal') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.defaultNotificationText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="onesignal_content" >
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.OnesignalAppid') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('onesignal_app_id', $result['commonContent']['setting']['onesignal_app_id'] , array('class'=>'form-control', 'id'=>'onesignal_app_id')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">  {{ trans('labels.OnesignalAppidText') }}</span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.OnesignalSenderid') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('onesignal_sender_id',  $result['commonContent']['setting']['onesignal_app_id'], array('class'=>'form-control', 'id'=>'onesignal_sender_id')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;"> {{ trans('labels.OnesignalSenderidText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="fcm_content" @if($result['commonContent']['setting']['linked_in'] == 'fcm') style="display: block;" @endif style="display: none;">
                                <hr>
                                <h5>{{ trans('labels.PushNotificationSetting') }} </h5>
                                <hr>
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.AppKey') }} </label>
                                    <div class="col-md-12">
                                        {!! Form::text('phone_no',  $result['commonContent']['setting']['phone_no'], array('class'=>'form-control', 'id'=>'phone_no')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.AppKeyText') }}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.SenderId') }} </label>
                                    <div class="col-md-12">
                                        {!! Form::text('fcm_android_sender_id',  $result['commonContent']['setting']['fcm_android_sender_id'], array('class'=>'form-control', 'id'=>'fcm_android_sender_id')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.SenderIdText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection