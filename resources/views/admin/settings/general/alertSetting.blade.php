@extends('admin.layout')
@section('title-dash'){{ trans('labels.alertSetting') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.alertSetting') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateAlertSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.alertSetting') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}Error:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        {!! Form::hidden('alert_id',  $result['setting'][0]->alert_id, array('class'=>'form-control', 'id'=>'alert_id')) !!}

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.createcustomeremail') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="create_customer_email" value="1" class="flat-red" @if($result['setting'][0]->create_customer_email==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class=" control-label">
                                        <input type="radio" name="create_customer_email" value="0" class="flat-red" @if($result['setting'][0]->create_customer_email==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.createcustomeremailtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.createcustomernotification') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="create_customer_notification" value="1" class="flat-red" @if($result['setting'][0]->create_customer_notification==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="create_customer_notification" value="0" class="flat-red" @if($result['setting'][0]->create_customer_notification==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.createcustomernotificationtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.orderemail') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="order_email" value="1" class="flat-red" @if($result['setting'][0]->order_email==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="order_email" value="0" class="flat-red" @if($result['setting'][0]->order_email==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.orderemailText') }}</span>
                                </div>
                            </div>
                        </div>

                        <hr>    

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.ordernotification') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="order_notification" value="1" class="flat-red" @if($result['setting'][0]->order_notification==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="order_notification" value="0" class="flat-red" @if($result['setting'][0]->order_notification==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ordernotificationText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.orderstatusemail') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="order_status_email" value="1" class="flat-red" @if($result['setting'][0]->order_status_email==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="order_status_email" value="0" class="flat-red" @if($result['setting'][0]->order_status_email==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.createcustomeremailtext') }}</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.orderstatusnotification') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="order_status_notification" value="1" class="flat-red" @if($result['setting'][0]->order_status_notification==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="order_status_notification" value="0" class="flat-red" @if($result['setting'][0]->order_status_notification==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.orderstatusnotificationtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <hr>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.newproductemail') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="new_product_email" value="1" class="flat-red" @if($result['setting'][0]->new_product_email==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="new_product_email" value="0" class="flat-red" @if($result['setting'][0]->new_product_email==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.newproductemailtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.newproductnotification') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="new_product_notification" value="1" class="flat-red" @if($result['setting'][0]->new_product_notification==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="new_product_notification" value="0" class="flat-red" @if($result['setting'][0]->new_product_notification==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.newproductnotificationtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.forgotemail') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="forgot_email" value="1" class="flat-red" @if($result['setting'][0]->forgot_email==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="forgot_email" value="0" class="flat-red" @if($result['setting'][0]->forgot_email==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.newproductemailtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.forgotemailnotification') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="forgot_notification" value="1" class="flat-red" @if($result['setting'][0]->forgot_notification==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="forgot_notification" value="0" class="flat-red" @if($result['setting'][0]->forgot_notification==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.forgotemailnotificationtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.newsemail') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="news_email" value="1" class="flat-red" @if($result['setting'][0]->news_email==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="news_email" value="0" class="flat-red" @if($result['setting'][0]->news_email==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.newsemailText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.newsnotification') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="news_notification" value="1" class="flat-red" @if($result['setting'][0]->news_notification==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="news_notification" value="0" class="flat-red" @if($result['setting'][0]->news_notification==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.newsnotificationtext') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shippingEnvironment" class="col-md-12 control-label" style="">{{ trans('labels.EmailContactUs') }}</label>
                                <div class="col-md-12">
                                    <label class=" control-label">
                                        <input type="radio" name="email_contact_us" value="1" class="flat-red" @if($result['setting'][0]->contact_us_email==1) checked @endif > &nbsp;{{ trans('labels.Yes') }}
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <label class=" control-label">
                                        <input type="radio" name="email_contact_us" value="0" class="flat-red" @if($result['setting'][0]->contact_us_email==0) checked @endif >  &nbsp;{{ trans('labels.No') }}
                                    </label>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.newsnotificationtext') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection