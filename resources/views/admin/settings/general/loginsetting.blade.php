@extends('admin.layout')
@section('title-dash'){{ trans('labels.Login Setting') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.Login Setting') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.Login Setting') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif

                    <div class="row">
                        <div class="col-dm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.emaillogin') }}</label>
                                <div class="col-md-12">
                                    <select name="email_login" class="form-control">
                                        <option @if($result['commonContent']['setting']['email_login'] == '1') selected @endif value="1"> {{ trans('labels.enable') }}</option>
                                        <option @if($result['commonContent']['setting']['email_login'] == '0') selected @endif value="0"> {{ trans('labels.disable') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.emaillogintext') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-dm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.phonelogin') }}</label>
                                <div class="col-md-12">
                                    <select name="phone_login" class="form-control">
                                        <option @if($result['commonContent']['setting']['phone_login'] == '1') selected @endif value="1"> {{ trans('labels.enable') }}</option>
                                        <option @if($result['commonContent']['setting']['phone_login'] == '0') selected @endif value="0"> {{ trans('labels.disable') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.phonelogintext') }}</span>
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-dm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.verificationstype') }}</label>
                                <div class="col-md-12">
                                    <select name="phone_verificatio_type" class="form-control">
                                        <option @if($result['commonContent']['setting']['phone_verificatio_type'] == 'firebase') selected @endif value="firebase"> {{ trans('labels.phonefirebase') }}</option>
                                        <!-- <option @if($result['commonContent']['setting']['phone_verificatio_type'] == 'twillio') selected @endif value="twillio"> {{ trans('labels.twillio') }}</option> -->
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.verificationstypetext') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>                                                
                </div>
            </div>
        </div>
    </div> 
    {!! Form::close() !!}
</section>
@endsection
