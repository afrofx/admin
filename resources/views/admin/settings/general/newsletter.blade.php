@extends('admin.layout')
@section('title-dash'){{ trans('labels.mailchimp_setting') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.mailchimp_setting') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.mailchimp_setting') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}Error:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.MailChimp') }}</label>
                                <div class="col-md-12">
                                    <select name="newsletter" class="form-control">
                                        <option @if($result['commonContent']['setting']['newsletter'] == '1') selected @endif value="1"> {{ trans('labels.enable') }}</option>
                                        <option @if($result['commonContent']['setting']['newsletter'] == '0') selected @endif value="0"> {{ trans('labels.Disable') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Newsletter Text') }}</span>
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.MailChimp API') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('mail_chimp_api', $result['commonContent']['setting']['mail_chimp_api'], array('class'=>'form-control', 'id'=>'mail_chimp_api')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.MailChimp API') }}</span>
                                </div>
                            </div>
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.MailChimp LIST ID') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('mail_chimp_list_id',$result['commonContent']['setting']['mail_chimp_list_id'], array('class'=>'form-control', 'id'=>'mail_chimp_list_id')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.MailChimp LIST ID') }}</span>
                                </div>
                            </div>
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group" id="imageIcone">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Newsletter Image') }}</label>
                                <div class="col-md-12">
                                    <!-- Modal -->
                                    <div class="modal fade embed-images" id="ModalmanufacturedICone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                </div>
                                                
                                                <div class="modal-body manufacturer-image-embed">
                                                    @if(isset($allimage))
                                                    <select class="image-picker show-html " name="newsletter_image" id="select_img">
                                                        <option value=""></option>
                                                        @foreach($allimage as $key=>$image)
                                                          <option data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                        @endforeach
                                                    </select>
                                                    @endif
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary btn-flat pull-left" >{{ trans('labels.Add Image') }}</a>
                                                    <button type="button" class="btn btn-default btn-flat refresh-image"><i class="fa fa-refresh"></i></button>
                                                    <button type="button" class="btn btn-success" id="selectedICONE" data-bs-dismiss="modal">{{ trans('labels.Done') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="imageselected">
                                      {!! Form::button(trans('labels.Add Image'), array('id'=>'newIcon','class'=>"btn btn-primary btn-flat field-validate", 'data-bs-toggle'=>"modal", 'data-target'=>"#ModalmanufacturedICone" )) !!}
                                      <br>
                                      <div id="selectedthumbnailIcon" class="selectedthumbnail col-md-5" style="display: none"> </div>
                                      <div class="closimage">
                                          <button type="button" class="close pull-left image-close " id="image-Icone"
                                            style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                    </div>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px; text-align: left">{{ trans('labels.Newsletter Image') }}</span>

                                    <br>
                                </div>
                            </div>  
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">  </label>
                                <div class="col-md-12">
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.OldImage') }}</span>
                                    <br>
                                    {!! Form::hidden('oldImage',  $result['commonContent']['setting']['newsletter_image'] , array('id'=>'newsletter_image')) !!}
                                    <img src="{{asset($result['commonContent']['setting']['newsletter_image'])}}" alt="" width="80px">
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
                
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
