@extends('admin.layout')
@section('title-dash'){{ trans('labels.facebook_settings') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.facebook_settings') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.facebook_settings') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.facebookLogin') }}</label>
                                <div class="col-md-12">
                                    <select name="facebook_login" class="form-control">
                                        <option @if($result['commonContent']['setting']['facebook_login'] == '1') selected @endif value="1"> {{ trans('labels.enable') }}</option>
                                        <option @if($result['commonContent']['setting']['facebook_login'] == '0') selected @endif value="0"> {{ trans('labels.disable') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.facebookLoginText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.FacebookAppID') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('facebook_app_id', $result['commonContent']['setting']['facebook_app_id'], array('class'=>'form-control', 'id'=>'facebook_app_id')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.FacebookAppIDText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.FacebookAppID') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('facebook_app_id', $result['commonContent']['setting']['facebook_app_id'], array('class'=>'form-control', 'id'=>'facebook_app_id')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.FacebookAppIDText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.FacebookSecretID') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('facebook_secret_id',$result['commonContent']['setting']['facebook_secret_id'], array('class'=>'form-control', 'id'=>'facebook_secret_id')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.FacebookSecretIDText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.FacebookAppURL') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('fb_redirect_url',$result['commonContent']['setting']['fb_redirect_url'], array('class'=>'form-control', 'id'=>'fb_redirect_url')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.FacebookAppURLText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
