@extends('admin.layout')
@section('title-dash'){{ trans('labels.google_settings') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.google_settings') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.google_settings') }} </h3>
                </div>

                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.googleLogin') }}</label>
                                <div class="col-md-12">
                                    <select name="google_login" class="form-control">
                                        <option @if($result['commonContent']['setting']['google_login']  == '1') selected @endif value="1"> {{ trans('labels.enable') }}</option>
                                        <option @if($result['commonContent']['setting']['google_login']  == '0') selected @endif value="0"> {{ trans('labels.disable') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.googleLoginText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.GoogleAppID') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('google_client_id',$result['commonContent']['setting']['google_client_id'], array('class'=>'form-control', 'id'=>'google_client_id')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.GoogleAppIDText') }}</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.GoogleAppSecret') }} </label>
                                <div class="col-md-12">
                                    {!! Form::text('google_client_secret',$result['commonContent']['setting']['google_client_secret'], array('class'=>'form-control', 'id'=>'google_client_secret')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.GoogleAppSecretText') }}</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.GoogleAppUrl') }} </label>
                                <div class="col-md-12"> 
                                    {!! Form::text('google_redirect_url', $result['commonContent']['setting']['google_redirect_url'], array('class'=>'form-control', 'id'=>'google_redirect_url')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.GoogleAppURLText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
