@extends('admin.layout')
@section('title-dash'){{ trans('labels.seo content') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.seo content') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.seo content') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.SEO Title') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('seo_title',  stripslashes($result['commonContent']['setting']['seo_title']), array('class'=>'form-control', 'id'=>'seo_title')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.SEOTitleText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.SEO Meta-Tag') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('seo_metatag',  stripslashes($result['commonContent']['setting']['seo_metatag']), array('class'=>'form-control', 'id'=>'seo_metatag')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.metatagText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.SEO Keyword') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('seo_keyword',  stripslashes($result['commonContent']['setting']['seo_keyword']), array('class'=>'form-control', 'id'=>'seo_keyword')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.seokeywordText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.SEO Description') }}</label>
                                <div class="col-md-12">
                                    {!! Form::textarea('seo_description',  stripslashes($result['commonContent']['setting']['seo_description']), array('class'=>'form-control', 'id'=>'editor')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.descriptionText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection