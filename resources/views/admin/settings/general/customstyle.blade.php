@extends('admin.layout')
@section('title-dash'){{ trans('labels.custom_style_js') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.custom_style_js') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.custom_style_js') }} </h3>
                </div>
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Before Head Tag') }}</label>
                                <div class="col-md-12">
                                    {!! Form::textarea('before_head_tag',  stripslashes($result['commonContent']['setting']['before_head_tag']), array('class'=>'form-control', 'id'=>'before_head_tag')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Before Head Tag Text') }}</span>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.End of Body Tag') }}</label>
                                <div class="col-md-12">
                                    {!! Form::textarea('end_body_tag',  stripslashes($result['commonContent']['setting']['end_body_tag']), array('class'=>'form-control', 'id'=>'end_body_tag')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.End of Body Tag Text') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>                
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
   
@endsection

@section('addjs')
<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('editor');
        $(".textarea").wysihtml5();
    });
</script>
@endsection