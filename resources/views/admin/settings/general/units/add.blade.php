@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddUnit') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item"><a href="{{ URL::to('admin/units')}}">{{ trans('labels.units') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.AddUnit') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/addnewunit', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.AddUnit') }}</h3>
                    </div>
                    
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif

                        <div class="row">
                            @foreach($result['languages'] as $key=>$languages)
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.UnitName') }} ({{ $languages->name }})</label>
                                        <div class="col-md-12">
                                            <input type="text" name="UnitName_<?=$languages->languages_id?>" class="form-control field-validate"  >
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.UnitNameMessage') }} ({{ $languages->name }}).</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="is_active">
                                            <option value="1">{{ trans('labels.Active') }}</option>
                                            <option value="0">{{ trans('labels.InActive') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusUnitText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                            <a href="{{ URL::to('admin/units')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection