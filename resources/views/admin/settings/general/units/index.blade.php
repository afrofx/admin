@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingUnits') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item active">{{ trans('labels.units') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.ListingUnits') }} </h3>
                    <div class="card-tools pull-right">
                        <a href="{{ URL::to('admin/addunit')}}" type="button" class="btn btn-block btn-primary btn-sm btn-flat">{{ trans('labels.AddUnit') }}</a>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{ trans('labels.ID') }}</th>
                                        <th>{{ trans('labels.UnitName') }}</th>
                                        <th>{{ trans('labels.Status') }}</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($result['units'])>0)
                                        @foreach ($result['units'] as $key=>$unit)
                                            <tr>
                                                <td>{{ $unit->unit_id }}</td>
                                                <td>{{ $unit->units_name }}</td>
                                                <td>
                                                    @if($unit->is_active==1)
                                                        <strong class="badge badge-success">{{ trans('labels.Active') }}</strong>
                                                    @else
                                                        <strong class="badge badge-danger">{{ trans('labels.InActive') }}</strong>
                                                    @endif</td>
                                                <td><a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="editunit/{{ $unit->unit_id }}" class="badge badge-secondary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteUnitsId" unit_id ="{{ $unit->unit_id }}" class="badge badge-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3"  style="text-transform:none"><strong>{{ trans('labels.Units are not added yet') }}</strong></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                            <div class="col-xs-12 text-right">
                                {{$result['units']->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- /.row -->
    <!-- deleteOrderStatusModal -->
    <div class="modal fade" id="deleteUnitModal" tabindex="-1" role="dialog" aria-labelledby="deleteUnitsModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteOrderStatusModalLabel">{{ trans('labels.DeleteUnit') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/deleteunit', 'name'=>'deleteunits', 'id'=>'deleteunits', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                    {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'unit_id')) !!}
                    <div class="modal-body">
                        <p>{{ trans('labels.DeleteUnitText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="deleteUnits">{{ trans('labels.Delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection