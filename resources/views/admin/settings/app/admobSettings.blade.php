@extends('admin.layout')
@section('title-dash'){{ trans('labels.admobSettings') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.admobSettings') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.admobSettings') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.admobID') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('admob_id',  $result['commonContent']['setting']['admob_id'], array('class'=>'form-control', 'id'=>'admob_id')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.admobIDText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.unitIdBanner') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('ad_unit_id_banner',  $result['commonContent']['setting']['ad_unit_id_banner'], array('class'=>'form-control', 'id'=>'ad_unit_id_banner')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.unitIdBannerText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.unitIdInterstitial') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('ad_unit_id_interstitial',$result['commonContent']['setting']['ad_unit_id_interstitial'], array('class'=>'form-control', 'id'=>'ad_unit_id_interstitial')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.unitIdInterstitialText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.admobStatus') }}</label>
                                <div class="col-md-12">
                                    <select name="admob" class="form-control">
                                        <option @if($result['commonContent']['setting']['admob'] == '1') selected @endif value="1"> {{ trans('labels.Show') }}</option>
                                        <option @if($result['commonContent']['setting']['admob'] == '0') selected @endif value="0"> {{ trans('labels.Hide') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.admobStatusText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <hr>
                    <h5>{{ trans('labels.admobSettingIOS') }} </h5>
                    <hr>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.admobID') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('ios_admob_id', $result['commonContent']['setting']['ios_admob_id'] , array('class'=>'form-control', 'id'=>'ios_admob_id')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.admobIDText') }}</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.unitIdBanner') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('ios_ad_unit_id_banner', $result['commonContent']['setting']['ios_ad_unit_id_banner'], array('class'=>'form-control', 'id'=>'ios_ad_unit_id_banner')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.unitIdBannerText') }}</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.unitIdInterstitial') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('ios_ad_unit_id_interstitial', $result['commonContent']['setting']['ios_ad_unit_id_interstitial'], array('class'=>'form-control', 'id'=>'ios_ad_unit_id_interstitial')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.unitIdInterstitialText') }}</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.admobStatus') }}</label>
                                <div class="col-md-12">
                                    <select name="ios_admob" class="form-control">
                                        <option @if($result['commonContent']['setting']['ios_admob'] == '1') selected @endif value="1"> {{ trans('labels.Show') }}</option>
                                        <option @if($result['commonContent']['setting']['ios_admob'] == '0') selected @endif value="0"> {{ trans('labels.Hide') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.admobStatusText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection