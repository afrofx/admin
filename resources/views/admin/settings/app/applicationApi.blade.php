@extends('admin.layout')
@section('title-dash'){{ trans('labels.apiSetting') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.apiSetting') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.apiSetting') }} </h3>
                </div>
                <div class="card-body">
                    <div class="alert alert-success" hidden id="generateSuccessfully" role="alert">
                        <span class="icon fa fa-check" aria-hidden="true"></span>
                        <span class="sr-only">{{ trans('labels.api') }}:</span>
                        {{ trans('labels.updateapisettingmessage') }}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Licensa</label>
                                <div class="col-md-12">
                                    {!! Form::text('consumer_key', $result['commonContent']['setting']['consumer_key'], array('readonly'=>'readonly', 'class'=>'form-control', 'id'=>'consumer_key')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.consumerKeyText') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Secret Key</label>
                                <div class="col-md-12">
                                    {!! Form::text('consumer_secret',$result['commonContent']['setting']['consumer_secret'], array('readonly'=>'readonly', 'class'=>'form-control', 'id'=>'consumer_secret')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.consumerSecretText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-success btn-flat" id="generate-key">{{ trans('labels.generateKey') }} </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection