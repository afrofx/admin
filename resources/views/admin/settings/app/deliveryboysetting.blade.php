@extends('admin.layout')
@section('title-dash'){{ trans('labels.DeliveryboySetting') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.DeliveryboySetting') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.DeliveryboySetting') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Setting') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-md-12">                    
                            <div class="form-group">
                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.MediaType') }}</label>
                                <div class="col-sm-10 col-md-4">
                                    <select name="maptype" class="form-control">
                                        <option @if($result['commonContent']['setting']['maptype'] == 'internal') selected @endif value="internal"> {{ trans('labels.internal') }}</option>
                                        <option @if($result['commonContent']['setting']['maptype'] == 'external') selected @endif value="external"> {{ trans('labels.external') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Please choose MediaType') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
