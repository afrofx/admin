@extends('admin.layout')
@section('title-dash'){{ trans('labels.ManageLabel') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.ManageLabel') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateAppLabel', 'name'=>'form', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header"> 
                    <h3 class="card-title">{{ trans('labels.ManageLabel') }} </h3>
                    <div class="card-tools pull-right">
                        <a href="{{ URL::to('admin/addappkey')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNewKey') }}</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div><br>
                        @endif
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session()->get('message') }}
                        </div><br>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            @foreach ($result['labels'] as $key=>$data)
                                @foreach ($data as $labels_data)
                                    <?php $labels_data1 = $labels_data->values->toArray(); ?>

                                    <hr>
                                    <h5><strong>Nome do Campo:</strong> {{ $labels_data->label_name }}</h5>
                                    <hr>
                                    
                                    <?php $j=0;?>

                                    @foreach($result['languages'] as $key=>$languages)
                                        @if(!empty($labels_data1[$j]->language_id) and $languages->languages_id == $labels_data1[$j]->language_id)
                                            <input type="hidden" name="label_id_<?=$labels_data->label_id?>" value="{{ $labels_data->label_id }}">

                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">{{ trans('labels.LabelValue') }} ({{ $languages->name }})</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="label_value_<?=$languages->languages_id?>_<?=$labels_data->label_id?>" class="form-control" @if(!empty($labels_data1[$j]->language_id)) value="{{ $labels_data1[$j]->label_value}}" @endif>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.LabelValue') }} ({{ $languages->name }}).</span>
                                                </div>
                                            </div>
                                        @else
                                            <input type="hidden" name="label_id_<?=$labels_data->label_id?>" value="{{ $labels_data->label_id }}">

                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">{{ trans('labels.LabelValue') }} ({{ $languages->name }})</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="label_value_<?=$languages->languages_id?>_<?=$labels_data->label_id?>" class="form-control">
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.LabelValue') }} ({{ $languages->name }}).</span>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        <?php
                                            $j++;
                                        ?>
                                    @endforeach
                                @endforeach
                                <div class="card-footer text-center">
                                    <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="pull-left btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                                    <button type="submit" class="btn btn-primary btn-flat pull-right">{{ trans('labels.Submit') }}</button>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection