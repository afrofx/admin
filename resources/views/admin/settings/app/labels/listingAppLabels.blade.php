@extends('admin.layout')
@section('title-dash')Lista de Labels...@endsection
@section('title-link') 
<li class="breadcrumb-item active">Lista de Labels</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.ListingAllLabels') }} </h3>
                    <div class="card-tools pull-right">
                        <a href="addAppLabel" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNewLabel') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{ trans('labels.Name') }}</th>
                                        <th>{{ trans('labels.ValueEnglish') }}</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($result['labels'])>0)
                                    @foreach ($result['labels'] as $key=>$label)
                                        <tr>
                                            <td>{{ $label->label_name }}</td>
                                            <td>{{ $label->label_value }}</td>
                                            <td><a data-bs-toggle="tooltip" data-placement="bottom" title="Edit" href="editAppLabel/{{ $label->label_id }}" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">{{ trans('labels.NoRecordFound') }}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="col-md-12 text-right">
                            {{$result['labels']->links('vendor.pagination.default')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="deleteLabelModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabelModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteLabelModalLabel">{{ trans('labels.DeleteLabel') }}</h4>
            </div>
            {!! Form::open(array('url' =>'admin/deleteLabel', 'name'=>'deleteLabel', 'id'=>'deleteLabel', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('label_id',  '', array('class'=>'form-control', 'id'=>'label_id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeleteLabelText') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                <button type="submit" class="btn btn-primary btn-flat" id="deleteLabel">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection