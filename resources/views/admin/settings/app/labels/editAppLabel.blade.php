@extends('admin.layout')
@section('title-dash')Editar Label...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/listingAppLabels') }}">Lista de Labels</a></li>
<li class="breadcrumb-item active">Editar Label</li>
@endsection
@section('content')
<section class="content">
	{!! Form::open(['url' => 'admin/updateAppLabel', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<h3 class="card-title">Editar Labels </h3>
				</div>
				
				<div class="card-body">
					@if (count($errors) > 0)
						@if ($errors->any())
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert"
									aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ $errors->first() }}
							</div>
						@endif
					@endif

					@if (session()->has('message'))
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-bs-dismiss="alert"
								aria-label="Close"><span aria-hidden="true">&times;</span></button>
							{{ session()->get('message') }}
						</div>
					@endif

					<div class="row">
						{!! Form::hidden('id', $result['labels_value'][0]->label_id, ['class' => 'form-control', 'id' => 'id']) !!}
						<div class="col-md-12">
							<div class="form-group">
								<label for="name" class="col-sm-2 col-md-3 control-label">Label Key</label>
								<div class="col-sm-10 col-md-4">
									<select class="form-control" name="label_id" disabled>
										@foreach ($result['labels'] as $labels)
											<option value="{{ $labels->label_id }}"
												@if ($result['labels_value'][0]->label_id == $labels->label_id) selected @endif> {{ $labels->label_name }}
											</option>
										@endforeach
									</select>
									<span class="help-block hidden">Este é o nome geral para identificar o idioma de valor múltiplo.</span>
								</div>
							</div>
						</div>

						<?php $i = 0;
						$j = 0; ?>
						
						@foreach ($result['languages'] as $key => $languages)
							@if (!empty($result['labels_value'][$j]->language_id))
								<input type="hidden" name="label_value_id_<?= $languages->languages_id ?>" value="{{ $result['labels_value'][$i]->label_value_id }}">
							@endif
							<div class="col-md-12">
								<div class="form-group">
									<label for="name" class="col-sm-2 col-md-3 control-label">Valor da Label ({{ $languages->name }})</label>
									<div class="col-sm-10 col-md-4">
										<input type="text" name="label_value_<?= $languages->languages_id ?>" class="form-control field-validate" @if (!empty($result['labels_value'][$j]->language_id)) value="{{ $result['labels_value'][$j]->label_value }}" @endif>
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Valor da Label ({{ $languages->name }}).</span>
									</div>
								</div>
							</div>

							<?php
							if (count($result['labels_value']) > 1) {
								$i++;
							}
							$j++;
							?>
							@endforeach
						</div>
					</div>
				</div>

				<div class="card-footer text-center">
					<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
					<a href="{{ URL::to('admin/listingAppLabels') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
				</div>
			</div>
		</div>
	</div>
{!! Form::close() !!}
</section>
@endsection
