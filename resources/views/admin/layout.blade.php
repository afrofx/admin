<!DOCTYPE html>
<html>

@include('admin.common.meta')
	<body class="hold-transition sidebar-mini layout-fixed">
		<div class="wrapper">
			<div class="se-pre-con" id="loader" style="/* display: none; */">
				<div class="pre-loader">
					<div class="la-line-scale">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<p>@lang('labels.Loading')...</p>
				</div>
			</div>
			
			@include('admin.common.header')

			@include('admin.common.sidebar')

			<div class="content-wrapper">
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-sm-6">
								<ol class="breadcrumb float-sm-left">
									<li class="breadcrumb-item active"> @yield('title-dash')</li>
								</ol>
							</div>
							
							<div class="col-sm-6">
								<ol class="breadcrumb float-sm-right">
									<li class="breadcrumb-item"><a href="{{ URL::to('admin/dashboard/this_month') }}">Inicio</a></li>
									@yield('title-link')
								</ol>
							</div>
						</div>
					</div>
				</div> 

				@yield('content')
			</div>
			
			@include('admin.common.controlsidebar')

			@include('admin.common.footer')
		</div>

		<div id="snackbar">{{ trans('labels.Cache Cleared Successfully') }}</div>

		@include('admin.common.scripts')
	</body>
</html>
