@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllProducts') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item active">{{ trans('labels.Products') }}</li>
@endsection
@section('content')
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-danger card-outline">
                        <div class="card-header">
                           <div class="card-title form-inline">
                                <form  name='registration' id="registration" class="registration" method="get">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="input-group-form search-panel ">
                                        <select id="FilterBy" type="button" class="btn btn-default dropdown-toggle form-control input-group-form " data-toggle="dropdown" name="categories_id">
                                            <option value="" selected disabled hidden>{{trans('labels.ChooseCategory')}}</option>
                                            @foreach ($results['subCategories'] as  $key=>$subCategories)
                                                <option value="{{ $subCategories->id }}"
                                                    @if(isset($_REQUEST['categories_id']) and !empty($_REQUEST['categories_id']))
                                                        @if( $subCategories->id == $_REQUEST['categories_id']) selected @endif
                                                    @endif
                                                >{{ $subCategories->name }}</option>
                                            @endforeach
                                        </select>
                                        <input type="text" class="form-control input-group-form " name="product" placeholder="Search term..." id="parameter"  @if(isset($product)) value="{{$product}}" @endif />
                                        <button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
                                        @if(isset($product,$categories_id))  <a class="btn btn-danger btn-flat" href="{{url('admin/products/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                    </div>
                                </form>
                                <div class="col-lg-4 form-inline" id="contact-form12"></div>
                            </div>
                            <div class="card-tools right">
                                <a href="{{ URL::to('admin/products/add') }}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                            </div>
                        </div>
                        

                        <div class="card-body">
                            @if (count($errors) > 0)
                                @if($errors->any())
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{$errors->first()}}
                                    </div>
                                @endif
                            @endif

                            <div class="row">
                                <div class="col-xs-12">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>@sortablelink('products_id', trans('labels.ID') )</th>
                                            <th>{{ trans('labels.Image') }}</th>
                                            <th>@sortablelink('categories_name', trans('labels.Category') )</th>
                                            <th>@sortablelink('products_name', trans('labels.Name') )</th>
                                            <th>{{ trans('labels.Additional info') }}</th>
                                            <th>@sortablelink('created_at', trans('labels.ModifiedDate') )</th>
                                            <th>Operação</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($results['products'])>0)
                                            @php  $resultsProduct = $results['products']->unique('products_id')->keyBy('products_id');  @endphp
                                            @foreach ($resultsProduct as  $key=>$product)
                                                <tr>
                                                    <td>{{ $product->products_id }}</td>
                                                    <td><img src="{{asset($product->path)}}" alt="" height="50px"></td>
                                                    <td>{{ $product->categories_name }}</td>
                                                    <td>
                                                        {{ $product->products_name }} @if(!empty($product->products_model)) ( {{ $product->products_model }} ) @endif
                                                    </td>
                                                    <td>
                                                        <strong>{{ trans('labels.Product Type') }}:</strong>
                                                        @if($product->products_type==0)
                                                            {{ trans('labels.Simple') }}
                                                        @elseif($product->products_type==1)
                                                            {{ trans('labels.Variable') }}
                                                        @elseif($product->products_type==2)
                                                            {{ trans('labels.External') }}
                                                        @endif
                                                        <br>
                                                        <!--@if(!empty($product->manufacturers_name))-->
                                                        <!--    <strong>{{ trans('labels.Manufacturer') }}:</strong> {{ $product->manufacturers_name }}<br>-->
                                                        <!--@endif-->
                                                        <strong>{{ trans('labels.Price') }}: </strong>   
                                                        @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $product->products_price }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                                                        <!--<br>-->
                                                        <!--<strong>{{ trans('labels.Weight') }}: </strong>  {{ $product->products_weight }}{{ $product->products_weight_unit }}<br>-->
                                                        <!--<strong>{{ trans('labels.Viewed') }}: </strong>  {{ $product->products_viewed }}<br>-->
                                                        <!--@if(!empty($product->specials_id))-->
                                                        <!--    <strong class="badge bg-light-blue">{{ trans('labels.Special Product') }}</strong><br>-->
                                                        <!--    <strong>{{ trans('labels.SpecialPrice') }}: </strong>  {{ $product->specials_products_price }}<br>-->

                                                        <!--    @if(($product->specials_id) !== null)-->
                                                        <!--        @php  $mytime = Carbon\Carbon::now()  @endphp-->
                                                        <!--        <strong>{{ trans('labels.ExpiryDate') }}: </strong>-->

                                                        <!--        {{-- @if($product->expires_date > $mytime->toDateTimeString()) --}}-->
                                                        <!--            {{  date('d-m-Y', $product->expires_date) }}-->
                                                        <!--        {{-- @else-->
                                                        <!--            <strong class="badge bg-red">{{ trans('labels.Expired') }}</strong>-->
                                                        <!--        @endif --}}-->
                                                        <!--        <br>-->
                                                        <!--    @endif-->
                                                        <!--@endif-->
                                                    </td>
                                                    <td>
                                                        {{ $product->productupdate }}
                                                    </td>

                                                    <td width="220px">
                                                      <a class="btn btn-primary btn-flat" href="{{url('admin/products/edit')}}/{{ $product->products_id }}"><i class="fa fa-pencil"></i></i></a>
                                                      @if($product->products_type==1)
                                                          <a class="btn btn-info btn-flat" href="{{url('admin/products/attach/attribute/display')}}/{{ $product->products_id }}"><i class="fa fa-list"></i></a>
                                                      @endif
                                                      <a class="btn btn-warning btn-flat" href="{{url('admin/products/images/display/'. $product->products_id) }}"><i class="fa fa-picture-o"></i></a>
                                                      <a class="btn btn-danger btn-flat" id="deleteProductId" products_id="{{ $product->products_id }}"><i class="fa fa-trash"></i></a>
                                                      </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>

                                </div>


                            </div>
                                <div class="row">
                                  @php
                                    if($results['products']->total()>0){
                                        $fromrecord = ($results['products']->currentpage()-1)*$results['products']->perpage()+1;
                                    }else{
                                        $fromrecord = 0;
                                    }
                                    if($results['products']->total() < $results['products']->currentpage()*$results['products']->perpage()){
                                        $torecord = $results['products']->total();
                                    }else{
                                        $torecord = $results['products']->currentpage()*$results['products']->perpage();
                                    }
                                  @endphp
                                  <div class="col-md-6" style="padding:15px; border-radius:5px;">
                                    <div>
                                        Showing {{$fromrecord}} to {{$torecord}} of  {{$results['products']->total()}} entries
                                    </div>
                                  </div>
                                    <div class="col-md-6"style="padding-top:15px;">
                                        <div class="pull-right">
                                            {{$results['products']->links()}}
                                        </div>
                                    </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="deleteproductmodal" tabindex="-1" role="dialog" aria-labelledby="deleteProductModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="deleteProductModalLabel">{{ trans('labels.DeleteProduct') }}</h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        {!! Form::open(array('url' =>'admin/products/delete', 'name'=>'deleteProduct', 'id'=>'deleteProduct', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                            {!! Form::hidden('products_id',  '', array('class'=>'form-control', 'id'=>'products_id')) !!}
                            <div class="modal-body">
                                <p>{{ trans('labels.DeleteThisProductDiloge') }}?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="deleteProduct">{{ trans('labels.DeleteProduct') }}</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
@endsection
