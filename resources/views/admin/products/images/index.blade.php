@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddProductImages') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/products/display') }}">{{ trans('labels.ListingAllProducts') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.AddImages') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-danger">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.ListingAllProductsImages') }}</b></h3>
                    <div class="card-tools">
                        <a type="button" class="btn btn-block btn-primary btn-flat" href="{{url('/admin/products/images/add/')}}/{{$products_id}}">Adicionar Imagens</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    
                    <div class="row">
                        @if (count($result['products_images']) > 0)
                        @foreach($result['products_images'] as $products_image)
                        <div class="col-md-3 margin-bottomset" >
                            <div class="thumbnail">
                                <div class="caption">
                                    <a class="btn btn-primary btn-flat editProductImagesModal"  href="{{url('admin/products/images/editproductimage/')}}/{{$products_image->id}}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a products_id = '{{ $products_image->products_id }}' id = "{{ $products_image->id }}" class="btn btn-danger btn-flat deleteProductImagesModal"><i class="fa fa-trash " aria-hidden="true"></i></a></td>
                                </div>
                                <img width="200px" height="200px"src="{{asset($products_image->path)}}" alt="...">
                                <br>
                                <b>Imagem Nº : </b> {{ $products_image->sort_order}}
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ URL::to("admin/products/display")}}" class="btn btn-primary btn-flat">{{ trans('labels.Save_And_complete') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- deleteProductImageModal -->
<div class="modal fade" id="deleteProductImageModal" tabindex="-1" role="dialog" aria-labelledby="deleteProductImageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content deleteImageEmbed">
        </div>
    </div>
</div>
@endsection
