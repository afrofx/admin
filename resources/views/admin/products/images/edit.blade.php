@extends('admin.layout')
@section('title-dash')Editar Imagem...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/products/display') }}">{{ trans('labels.ListingAllProducts') }}</a></li>
<li class="breadcrumb-item active">Editar Imagem</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/products/images/updateproductimage', 'name'=>'editImageFrom', 'id'=>'editImageFrom', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><b>Editar Imagem </b></h3>
                </div>
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::hidden('products_id',  $products_images[0]->products_id, array('class'=>'form-control', 'id'=>'products_id')) !!}
                            {!! Form::hidden('id',  $products_images[0]->id, array('class'=>'form-control', 'id'=>'id')) !!}
                            {!! Form::hidden('oldImage',  $products_images[0]->image , array('id'=>'oldImage')) !!}
                            {!! Form::hidden('sort_order',  $products_images[0]->sort_order, array('class'=>'form-control', 'id'=>'sort_order')) !!}
                                    
                            <div class="form-group" id="imageIcone">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Icon') }}</label>
                                <div class="col-md-12">
                                    <div class="modal fade embed-images" id="ModalmanufacturedICone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                    <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body manufacturer-image-embed">
                                                    @if(isset($allimage))
                                                    <select class="image-picker show-html " name="image_id" id="select_img">
                                                        <option value=""></option>
                                                        @foreach($allimage as $key=>$image)
                                                        <option data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                        @endforeach
                                                    </select>
                                                    @endif
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary btn-flat pull-left" >{{ trans('labels.Add Image') }}</a>
                                                    <button type="button" class="btn btn-default btn-flat refresh-image"><i class="fa fa-refresh"></i></button>
                                                    <button type="button" class="btn btn-success btn-flat" id="selectedICONE" data-dismiss="modal">{{ trans('labels.Done') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="imageselected">
                                        {!! Form::button(trans('labels.Add Icon'), array('id'=>'newIcon','class'=>"btn btn-primary btn-flat field-validate", 'data-bs-toggle'=>"modal", 'data-bs-target'=>"#ModalmanufacturedICone" )) !!}
                                        <br>
                                        <div id="selectedthumbnailIcon" class="selectedthumbnail col-md-5"> </div>
                                            <div class="closimage">
                                                <button type="button" class="close pull-left image-close " id="image-Icone" style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.CategoryIconText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label"></label>
                                <div class="col-md-12">
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.OldImage') }}</span>
                                    <img src="{{asset($products_images[0]->path)}}" alt="" width=" 100px">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Ordem de Apresentação</label>
                                <div class="col-md-12">
                                    {!! Form::text('sort_order',  $products_images[0]->sort_order, array('class'=>'form-control', 'id'=>'sort_order')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }}</label>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        {!! Form::textarea('htmlcontent',  $products_images[0]->htmlcontent, array('class'=>'form-control', 'id'=>'htmlcontent', 'colspan'=>'3' )) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ImageDescription') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-danger addError" style="display: none; margin-bottom: 0;" role="alert"><i class="icon fa fa-ban"></i>{{ trans('labels.ImageDescription') }}</div>
                            <div class="alert alert-danger addError" style="display: none; margin-bottom: 0;" role="alert"><i class="icon fa fa-ban"></i> {{ trans('labels.ChooseImageText') }} </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <a type="button" class="btn btn-default btn-flat"  href="{{url('admin/products/images/display')}}/{{$products_images[0]->products_id}}">{{ trans('labels.Close') }}</a>
                    <button type="submit" class="btn btn-primary btn-flat" >{{ trans('labels.Submit') }}</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection

@section('addjs')
    <script>
        $("#select_img").imagepicker()
     </script>
@endsection