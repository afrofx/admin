@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddProduct') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/products/display')}}">{{ trans('labels.ListingAllProducts') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.AddProduct') }}</li>
@endsection

@section('addcss')
<style>
.list-group-item {
    padding: 0 !important;
}
</style>
    
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/products/add', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title"><b>{{ trans('labels.AddNewProduct') }}</b></h3>
                    </div>

                    <div class="card-body">
                        @if(session()->has('message.level'))
                            <div class="alert alert-{{ session('message.level') }} alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! session('message.content') !!}
                            </div>
                        @endif

                        @if( count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    <span class="fa fa-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">{{ trans('labels.Error') }}:</span>
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="col-md-12 control-label">{{ trans('labels.Product Type') }}<span style="color:red;">*</span></label>
                                            <div class="col-md-12">
                                                <select class="form-control field-validate prodcust-type" name="products_type" onChange="prodcust_type();">
                                                    <option selected hidden disabled>{{ trans('labels.Choose Type') }}</option>
                                                    <option value="0">{{ trans('labels.Simple') }}</option>
                                                    <option value="1">{{ trans('labels.Variable') }}</option>
                                                    <option value="2">{{ trans('labels.External') }}</option>
                                                </select>
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Product Type Text') }}.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="col-md-12 control-label">{{ trans('labels.Manufacturers') }} </label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="manufacturers_id">
                                                    <option  selected hidden disabled>{{ trans('labels.ChooseManufacturers') }}</option>
                                                    @foreach ($result['manufacturer'] as $manufacturer)
                                                        <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                                                    @endforeach
                                                </select><span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseManufacturerText') }}.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Categorias<span style="color:red;">*</span></label>
                                    <div class="col-md-12">
                                        <?php print_r($result['categories']); ?>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseCatgoryText') }}.</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.IsFeature') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="is_feature">
                                            <option value="0">{{ trans('labels.No') }}</option>
                                            <option value="1">{{ trans('labels.Yes') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.IsFeatureProuctsText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="products_status">
                                            <option value="1">{{ trans('labels.Active') }}</option>
                                            <option value="0">{{ trans('labels.Inactive') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.SelectStatus') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductsPrice') }}<span style="color:red;">*</span></label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_price', '', array('class'=>'form-control number-validate', 'id'=>'products_price')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ProductPriceText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group" id="tax-class">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.TaxClass') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control field-validate" name="tax_class_id">
                                            <option selected>{{ trans('labels.SelectTaxClass') }}</option>
                                            @foreach ($result['taxClass'] as $taxClass)
                                                <option value="{{ $taxClass->tax_class_id }}">{{ $taxClass->tax_class_title }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseTaxClassForProductText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Min Order Limit') }}<span style="color:red;">*</span></label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_min_order', '1', array('class'=>'form-control field-validate number-validate', 'id'=>'products_min_order')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Min Order Limit Text') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Max Order Limit') }}<span style="color:red;">*</span></label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_max_stock', '9999', array('class'=>'form-control field-validate number-validate', 'id'=>'products_max_stock')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Max Order Limit Text') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductsWeight') }}</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::text('products_weight', '0', array('class'=>'form-control field-validate number-validate', 'id'=>'products_weight')) !!}
                                        </div>
                                        <div class="col-md-6" style="padding-left: 0;">
                                            <select class="form-control" name="products_weight_unit">
                                                <option value="gm">Gm</option>
                                            </select>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.RequiredTextForWeight') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductsModel') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_model', '', array('class'=>'form-control', 'id'=>'products_model')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ProductsModelText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 ">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }}<span style="color:red;">*</span></label>
                                    <div class="col-md-12">
                                        <div id="imageselected">
                                            {!! Form::button( trans('labels.Add Image'), array('id'=>'newImage','class'=>"btn btn-primary btn-flat field-validate", 'data-bs-toggle'=>"modal", 'data-bs-target'=>"#Modalmanufactured" )) !!}
                                            <br>
                                            <div id="selectedthumbnail" class="selectedthumbnail col-md-5"> </div>
                                            <div class="closimage">
                                                <button type="button" class="close pull-left image-close " id="image-close" style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.UploadProductImageText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Link do Video</label>
                                    <div class="col-md-12">
                                        {!! Form::textarea('products_video_link', '', array('class'=>'form-control', 'id'=>'products_video_link', 'rows'=>2)) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Adicione o link do video no Youtube</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group flash-sale-link">
                                    <label for="name" class="col-md-12 control-label"> Venda Rapida ou Promoção</label>
                                    <div class="col-md-12">
                                        <select class="form-control" onChange="showFlash();" name="isFlash" id="isFlash">
                                            <option value="no">{{ trans('labels.No') }}</option>
                                            <option value="yes">{{ trans('labels.Yes') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Selecione "Sim" se for uma venda rapida ou promoção</span>
                                    </div>
                                </div>

                                <div class="flash-container" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">Preço<span style="color:red;">*</span></label>
                                                <div class="col-md-12">
                                                    <input class="form-control" type="text" name="flash_sale_products_price" id="flash_sale_products_price" value="">
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Preço da promoção</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                                <div class="col-md-12">
                                                    <select class="form-control" name="flash_status">
                                                        <option value="1">{{ trans('labels.Active') }}</option>
                                                        <option value="0">{{ trans('labels.Inactive') }}</option>
                                                    </select>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Escolha activar para mostrar na secção de promoções</span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">Data de Inicio<span style="color:red;">*</span></label>
                                                <div class="col-md-12">
                                                    <input class="form-control datepicker" readonly type="text" name="flash_start_date" id="flash_start_date" readonly value="">
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Hora de Inicio</span>
                                                </div>
                                                <div class="col-md-12 bootstrap-timepicker">
                                                    <input type="text" class="form-control timepicker" name="flash_start_time" readonly id="flash_start_time" value="">
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">Data de Expiração<span style="color:red;">*</span></label>
                                                <div class="col-md-12">
                                                    <input class="form-control datepicker" readonly type="text" readonly name="flash_expires_date" id="flash_expires_date" value="">
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Hora de Expiracção</span>
                                                </div>
                                                <div class="col-md-12 bootstrap-timepicker">
                                                    <input type="text" class="form-control timepicker" readonly name="flash_end_time" id="flash_end_time" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group special-link">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Special') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" onChange="showSpecial();" name="isSpecial" id="isSpecial">
                                            <option value="no">{{ trans('labels.No') }}</option>
                                            <option value="yes">{{ trans('labels.Yes') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.SpecialProductText') }}.</span>
                                    </div>
                                </div>

                                <div class="special-container" style="display: none;">
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.SpecialPrice') }}<span style="color:red;">*</span></label>
                                        <div class="col-md-12">
                                            <input class="form-control" type="text" name="specials_new_products_price" id="special-price" value="">
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.SpecialPriceTxt') }}.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.ExpiryDate') }}<span style="color:red;">*</span></label>
                                        <div class="col-md-12">
                                            <input class="form-control datepicker" readonly readonly type="text" name="expires_date" id="expiry-date" value="">
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.SpecialExpiryDateTxt') }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}<span style="color:red;">*</span></label>
                                        <div class="col-md-12">
                                            <select class="form-control" name="status">
                                                <option value="1">{{ trans('labels.Active') }}</option>
                                                <option value="0">{{ trans('labels.Inactive') }}</option>
                                            </select>
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ActiveSpecialProductText') }}.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        

                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabbable tabs-left">
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            @foreach($result['languages'] as $key=>$languages)
                                            <button class="nav-link @if($key==0) active @endif" id="nav-product_<?=$languages->languages_id?>" data-bs-toggle="tab" data-bs-target="#product_<?=$languages->languages_id?>" type="button" role="tab" aria-controls="nav-product_<?=$languages->languages_id?>" aria-selected="true"><?=$languages->name?><span style="color:red;">*</span></button>
                                            @endforeach
                                        </div>
                                    </nav>

                                    <div class="tab-content" id="nav-tabContent">
                                        @foreach($result['languages'] as $key=>$languages)

                                        <div style="margin-top: 15px;" class="tab-pane fade @if($key==0) show active @endif" id="product_<?=$languages->languages_id?>" role="tabpanel" aria-labelledby="nav-product_<?=$languages->languages_id?>">
                                            <div class="">
                                                <div class="form-group">
                                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductName') }}<span style="color:red;">*</span> ({{ $languages->name }})</label>
                                                    <div class="col-md-12">
                                                        <input type="text" name="products_name_<?=$languages->languages_id?>" class="form-control field-validate">
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.EnterProductNameIn') }} {{ $languages->name }} </span>
                                                    </div>
                                                </div>

                                                <div class="form-group external_link" style="display: none">
                                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.External URL') }} ({{ $languages->name }})</label>
                                                    <div class="col-md-12">
                                                        <input type="text" name="products_url_<?=$languages->languages_id?>" class="form-control products_url">
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.External URL Text') }} {{ $languages->name }} </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }}<span style="color:red;">*</span> ({{ $languages->name }})</label>
                                                    <div class="col-md-12">
                                                        <textarea id="editor<?=$languages->languages_id?>" name="products_description_<?=$languages->languages_id?>" class="form-control" rows="5"></textarea>
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.EnterProductDetailIn') }} {{ $languages->name }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <!-- Modal -->
                     <div class="modal fade" id="Modalmanufactured" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">{{ trans('labels.Choose Image') }}</h4>
                                    <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                </div>
                                <div class="modal-body manufacturer-image-embed">
                                    @if(isset($allimage))
                                        <select class="image-picker show-html " name="image_id" id="select_img">
                                            <option value=""></option>
                                            @foreach($allimage as $key=>$image)
                                                <option  data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary btn-flat float-left">{{ trans('labels.Add Image') }}</a>
                                    <button type="button" class="btn btn-default btn-flat refresh-image"><i class="fa fa-refresh"></i></button>
                                    <button type="button" class="btn btn-primary btn-flat" id="selected" data-bs-dismiss="modal">Concluir</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary btn-flat">
                            <span>Salvar e Continuar</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection

@section('addjs')
    <script type="text/javascript">
        $(function() {
            @foreach($result['languages'] as $languages)
            CKEDITOR.replace('editor{{$languages->languages_id}}');
            @endforeach
            $(".textarea").wysihtml5();
        });

        
    </script>
    <script>
        $("#select_img").imagepicker()
     </script>
@endsection