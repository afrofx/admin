@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditProduct') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/products/display')}}">{{ trans('labels.ListingAllProducts') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.EditProduct') }}</li>
@endsection

@section('addcss')
<style>
.list-group-item {
    padding: 0 !important;
}
</style>
    
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/products/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title"><b>{{ trans('labels.EditProduct') }}</b> </h3>
                    </div>

                    <div class="card-body">
                        @if(session()->has('message.level'))
                            <div class="alert alert-{{ session('message.level') }} alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {!! session('message.content') !!}
                            </div>
                        @endif

                        @if( count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <span class="fa fa-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        {!! Form::hidden('id', $result['product'][0]->products_id, array('class'=>'form-control', 'id'=>'id')) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="col-md-12 control-label">{{ trans('labels.Product Type') }} </label>
                                            <div class="col-md-12">
                                                <select class="form-control field-validate prodcust-type" name="products_type" onChange="prodcust_type();">
                                                    <option selected hidden disabled>{{ trans('labels.Choose Type') }}</option>
                                                    <option value="0" @if($result['product'][0]->products_type==0) selected @endif>{{ trans('labels.Simple') }}</option>
                                                    <option value="1" @if($result['product'][0]->products_type==1) selected @endif>{{ trans('labels.Variable') }}</option>
                                                    <option value="2" @if($result['product'][0]->products_type==2) selected @endif>{{ trans('labels.External') }}</option>
                                                </select>
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Product Type Text') }}.</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="col-md-12 control-label">{{ trans('labels.Manufacturers') }} </label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="manufacturers_id">
                                                    <option selected hidden disabled>{{ trans('labels.Choose Manufacturer') }}</option>
                                                    @foreach ($result['manufacturer'] as $manufacturer)
                                                        <option @if($result['product'][0]->manufacturers_id == $manufacturer->id ) selected @endif value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                    Adicione fabricantes no link Fabricantes ou deixe em branco se não houver nenhum fabricante.    
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Categorias</label>
                                    <div class="col-md-12">
                                    <?php print_r($result['categories']); ?>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.ChooseCatgoryText') }}.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.IsFeature') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="is_feature">
                                            <option value="0" @if($result['product'][0]->is_feature==0) selected @endif>{{ trans('labels.No') }}</option>
                                            <option value="1" @if($result['product'][0]->is_feature==1) selected @endif>{{ trans('labels.Yes') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Por favor, escolha 'Sim' para definir o recurso deste produto.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="products_status">
                                            <option value="1" @if($result['product'][0]->products_status==1) selected @endif >{{ trans('labels.Active') }}</option>
                                            <option value="0" @if($result['product'][0]->products_status==0) selected @endif>{{ trans('labels.Inactive') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Selecione o estado Ativo ou Inativo</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductsPrice') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_price', $result['product'][0]->products_price, array('class'=>'form-control number-validate', 'id'=>'products_price')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.ProductPriceText') }} </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group" id="tax-class">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.TaxClass') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control field-validate" name="tax_class_id">
                                            <option selected> {{ trans('labels.SelectTaxClass') }}</option>
                                            @foreach ($result['taxClass'] as $taxClass)
                                                <option @if($result['product'][0]->products_tax_class_id == $taxClass->tax_class_id ) selected @endif value="{{ $taxClass->tax_class_id }}">{{ $taxClass->tax_class_title }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.ChooseTaxClassForProductText') }} </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Min Order Limit') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_min_order', $result['product'][0]->products_min_order, array('class'=>'form-control field-validate number-validate', 'id'=>'products_min_order')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.Min Order Limit Text') }} </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Max Order Limit') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_max_stock', $result['product'][0]->products_max_stock, array('class'=>'form-control field-validate number-validate', 'id'=>'products_max_stock')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.Max Order Limit Text') }} </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductsWeight') }}</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::text('products_weight', $result['product'][0]->products_weight, array('class'=>'form-control field-validate number-validate', 'id'=>'products_weight')) !!}
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.RequiredTextForWeight') }}</span>
                                        </div>
                                        <div class="col-md-6" style="padding-left: 0;">
                                            <select class="form-control" name="products_weight_unit">
                                                <option value="gm" @if($result['product'][0]->products_weight_unit=='gm') selected @endif>Gm</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductsModel') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('products_model', $result['product'][0]->products_model, array('class'=>'form-control', 'id'=>'products_model')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ProductsModelText') }}</span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }} </label>
                                    <div class="col-md-12">
                                        <!-- Modal -->
                                        <div class="modal fade" id="Modalmanufactured" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title text-primary" id="myModalLabel">Escolher Imagem </h3>
                                                        <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    </div>

                                                    <div class="modal-body manufacturer-image-embed">
                                                        @if(isset($allimage))
                                                        <select class="image-picker show-html " name="image_id" id="select_img">
                                                            <option value=""></option>
                                                            @foreach($allimage as $key=>$image)
                                                            <option data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                            @endforeach
                                                        </select>
                                                        @endif
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary btn-flat pull-left">{{ trans('labels.Add Image') }}</a>
                                                        <button type="button" class="btn btn-default btn-flat refresh-image"><i class="fa fa-refresh"></i></button>
                                                        <button type="button" class="btn btn-primary btn-flat" id="selected" data-bs-dismiss="modal">Done</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="imageselected">
                                            {!! Form::button(trans('labels.Add Image'), array('id'=>'newImage','class'=>"btn btn-primary btn-flat", 'data-bs-toggle'=>"modal", 'data-bs-target'=>"#Modalmanufactured" )) !!}
                                            <br>
                                            <div id="selectedthumbnail" class="selectedthumbnail col-md-5"> </div>
                                            <div class="closimage">
                                                <button type="button" class="close pull-left image-close " id="image-close"
                                                    style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.UploadProductImageText') }}</span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label"></label>
                                    <div class="col-md-12">
                                        {!! Form::hidden('oldImage', $result['product'][0]->products_image , array('id'=>'oldImage', 'class'=>'field-validate ')) !!}
                                        <img src="{{asset($result['product'][0]->path)}}" alt="" width=" 100px">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.slug') }} </label>
                                    <div class="col-md-12">
                                        <input type="hidden" name="old_slug" value="{{$result['product'][0]->products_slug}}">
                                        <input type="text" name="slug" class="form-control field-validate" value="{{$result['product'][0]->products_slug}}">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.slugText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.VideoEmbedCodeLink') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::textarea('products_video_link', $result['product'][0]->products_video_link, array('class'=>'form-control', 'id'=>'products_video_link', 'rows'=>4)) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.VideoEmbedCodeLinkText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.FlashSale') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" onChange="showFlash()" name="isFlash" id="isFlash">
                                            <option value="no" @if($result['flashProduct'][0]->flash_status == 0) selected  @endif>{{ trans('labels.No') }}</option>
                                            <option value="yes" @if($result['flashProduct'][0]->flash_status == 1) selected @endif>{{ trans('labels.Yes') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.FlashSaleText') }}</span>
                                    </div>
                                </div>
                                
                                <div class="flash-container" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">{{ trans('labels.FlashSalePrice') }}</label>
                                                <div class="col-md-12">
                                                    <input class="form-control" type="text" name="flash_sale_products_price" id="flash_sale_products_price" value="{{$result['flashProduct'][0]->flash_sale_products_price}}">
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FlashSalePriceText') }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                                <div class="col-md-12">
                                                    <select class="form-control" name="flash_status">
                                                        <option value="1">{{ trans('labels.Active') }}</option>
                                                        <option value="0">{{ trans('labels.Inactive') }}</option>
                                                    </select>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.ActiveFlashSaleProductText') }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">Data de Inicio</label>
                                                @if($result['flashProduct'][0]->flash_status == 1)
                                                    <div class="col-md-12">
                                                        <input class="form-control datepicker" readonly type="text" name="flash_start_date" id="flash_start_date" value="{{date('d/m/Y', $result['flashProduct'][0]->flash_start_date) }}">
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FlashSaleDateText') }}</span>
                                                    </div>
                                                    <div class="col-md-12 bootstrap-timepicker">
                                                        <input type="text" class="form-control timepicker" readonly name="flash_start_time" id="flash_start_time"value="{{date('h:i:sA',  $result['flashProduct'][0]->flash_start_date ) }}">
                                                    </div>
                                                @else
                                                    <div class="col-md-12">
                                                        <input class="form-control datepicker" readonly type="text" name="flash_start_date" id="flash_start_date" value="">
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FlashSaleDateText') }}</span>
                                                    </div>
                                                    <div class="col-md-12 bootstrap-timepicker">
                                                        <input type="text" class="form-control timepicker" readonly name="flash_start_time" id="flash_start_time" value="">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">Data de Expiração</label>
                                                @if($result['flashProduct'][0]->flash_status == 1)
                                                    <div class="col-md-12">
                                                        <input class="form-control datepicker" readonly type="text" name="flash_expires_date" id="flash_expires_date"value="{{ date('d/m/Y', $result['flashProduct'][0]->flash_expires_date )}}">
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FlashExpireDateText') }}</span>
                                                    </div>
                                                    <div class="col-md-12 bootstrap-timepicker">
                                                        <input type="text" class="form-control timepicker" readonly name="flash_end_time" id="flash_end_time" value="{{ date('h:i:sA', $result['flashProduct'][0]->flash_expires_date ) }}">
                                                    </div>
                                                @else
                                                <div class="col-md-12">
                                                    <input class="form-control datepicker" readonly type="text" name="flash_expires_date" id="flash_expires_date" value="">
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.FlashExpireDateText') }}</span>
                                                </div>
                                                <div class="col-md-12 bootstrap-timepicker">
                                                    <input type="text" class="form-control timepicker" readonly name="flash_end_time" id="flash_end_time" value="">
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group  special-link">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Special') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control" onChange="showSpecial()" name="isSpecial" id="isSpecial">
                                            <option @if($result['product'][0]->products_id != $result['specialProduct'][0]->products_id && $result['specialProduct'][0]->status == 0) selected @endif value="no">{{ trans('labels.No') }}</option>
                                            <option @if($result['product'][0]->products_id == $result['specialProduct'][0]->products_id && $result['specialProduct'][0]->status == 1) selected @endif  value="yes">{{ trans('labels.Yes') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.SpecialProductText') }}</span>
                                    </div>
                                </div>

                                <div class="special-container" style="display: none;">
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.SpecialPrice') }}</label>
                                        <div class="col-md-12">
                                            {!! Form::text('specials_new_products_price', $result['specialProduct'][0]->specials_new_products_price, array('class'=>'form-control', 'id'=>'special-price')) !!}
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.SpecialPriceTxt') }}.</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.ExpiryDate') }}</label>
                                        <div class="col-md-12">
                                            @if(!empty($result['specialProduct'][0]->status) and $result['specialProduct'][0]->status == 1)
                                                {!! Form::text('expires_date', date('d/m/Y', $result['specialProduct'][0]->expires_date), array('class'=>'form-control datepicker', 'id'=>'expiry-date', 'readonly'=>'readonly')) !!}
                                            @else
                                                {!! Form::text('expires_date', '', array('class'=>'form-control datepicker', 'id'=>'expiry-date', 'readonly'=>'readonly')) !!}
                                            @endif
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.SpecialExpiryDateTxt') }} </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                        <div class="col-md-12">
                                            <select class="form-control" name="status">
                                                <option @if($result['specialProduct'][0]->status == 1 ) selected @endif value="1">{{ trans('labels.Active') }} </option>
                                                <option @if($result['specialProduct'][0]->status == 0 ) selected @endif value="0">{{ trans('labels.Inactive') }}</option>
                                            </select>
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ActiveSpecialProductText') }}.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabbable tabs-left">
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            @php $i = 0; @endphp
                                            @foreach($result['languages'] as $key=>$languages)
                                                <button class="nav-link @if($i==0) active @endif" id="nav-product_<?=$languages->languages_id?>" data-bs-toggle="tab" data-bs-target="#product_<?=$languages->languages_id?>" type="button" role="tab" aria-controls="nav-product_<?=$languages->languages_id?>" aria-selected="true"><?=$languages->name?><span style="color:red;">*</span></li>
                                                @php $i++; @endphp
                                            @endforeach
                                        </div>
                                    </nav>

                                    <div class="tab-content" id="nav-tabContent">
                                        @php $j = 0; @endphp
                                        @foreach($result['description'] as $key=>$description_data)
                                            <div style="margin-top: 15px;" class="tab-pane fade @if($j==0) show active @endif" id="product_<?=$description_data['languages_id']?>" id="product_<?=$description_data['languages_id']?>" role="tabpanel" aria-labelledby="nav-product_<?=$description_data['languages_id']?>">
                                                @php $j++; @endphp
                                                <div class="form-group">
                                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ProductName') }} ({{ $description_data['language_name'] }})</label>
                                                    <div class="col-md-12">
                                                        <input type="text" name="products_name_<?=$description_data['languages_id']?>" class="form-control field-validate" value='{{$description_data['products_name']}}'>
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.EnterProductNameIn') }} {{ $description_data['language_name'] }} </span>
                                                    </div>
                                                </div>

                                                <div class="form-group external_link" style="display: none">
                                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.External URL') }} ({{ $description_data['language_name'] }})</label>
                                                    <div class="col-md-12">
                                                        <input type="text" name="products_url_<?=$description_data['languages_id']?>" class="form-control products_url" value='{{$description_data['products_url']}}'>
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.External URL Text') }} ({{ $description_data['language_name'] }}) </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }} ({{ $description_data['language_name'] }})</label>
                                                    <div class="col-md-12">
                                                        <textarea id="editor<?=$description_data['languages_id']?>" name="products_description_<?=$description_data['languages_id']?>" class="form-control" rows="5">{{stripslashes($description_data['products_description'])}}</textarea>
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.EnterProductDetailIn') }} {{ $description_data['language_name'] }}</span>
                                                     </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right btn-flat" id="normal-btn">{{ trans('labels.Save_And_Continue') }} <i class="fa fa-angle-right 2x"></i></button>

                    </div>
                    <div class="card-footer text-center">
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection


@section('addjs')
<script type="text/javascript">
    $(function() {
        @foreach($result['languages'] as $languages)
        CKEDITOR.replace('editor{{$languages->languages_id}}');
        @endforeach
        $(".textarea").wysihtml5();
    });
</script>

<script>
    $("#select_img").imagepicker()
</script>
@endsection