@extends('admin.layout')
@section('title-dash')
    {{ trans('labels.Inventory') }}...
@endsection
@section('title-link')
    <li class="breadcrumb-item"><a
            href="{{ URL::to('admin/products/display') }}">{{ trans('labels.ListingAllProducts') }}</a></li>
    {{-- @if ($result['products'][0]->products_type == 1)
        <li class="breadcrumb-item">
            <a href="{{ URL::to('admin/products/attach/attribute/display/' . $result['products'][0]->products_id) }}">{{ trans('labels.AddOptions') }}</a>
        </li>
    @endif --}}
    <li class="breadcrumb-item active">{{ trans('labels.Inventory') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(['url' => 'admin/products/inventory/addnewstock', 'name' => 'inventoryfrom', 'id' => 'addewinventoryfrom', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
        <div class="row">
            <div class="col-md-6">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <a href="#" class="btn btn-primary pull-left btn-flat">Carregar Stock</a>
                        <a href="#" class="btn btn-success pull-left btn-flat" style="margin-left:10px">Baixar Modelo em Excel(CSV)</a>
                        <small class="text-warning pull-right" style="color:red;">Em Desenvolvimento</small>
                    </div>

                    <div class="card-body">
                        @if (session()->has('message.level'))
                            <div class="alert alert-{{ session('message.level') }} alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! session('message.content') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <input type="hidden" name="products_id" id="products_id" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Referencia do Produto<span style="color:red;">*</span> </label>
                                    <div class="col-md-12">
                                        <select class="form-control product-type" name="ref_id" id="ref_id">
                                            <option selected hidden disabled>Pesquise a Refenrencia</option>
                                            @foreach ($result['inventory'] as $item)
                                                <option value="{{ $item->inventory_ref_id }}">{{ $item->reference_code }} </option>
                                            @endforeach
                                        </select>
                                        <small class="help-block"vstyle="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            Caso a referencia nao exista por favor clique em "Adicionar Por Produto"...
                                        </small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label"> {{ trans('labels.Current Stock') }} : <span id="current_stocks" style="width:100%">0</span> </label>
                                    <div class="col-md-12" hidden>
                                        <input type="hidden" id="current_stocks_val" name="current_stocks_input" value="" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">Tipo de Stock</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="stock_type">
                                            <option value="in">ENTRADA</option>
                                            <option value="out">SAIDA</option>
                                        </select>
                                        <span class="help-block"style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Escolha Entrada ou Saida</span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Enter Stock') }}<span style="color:red;">*</span></label>
                                    <div class="col-md-12">
                                        <input type="text" name="stock" value="" class="form-control stock-validate" placeholder="Quatidade">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Enter Stock Text') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        <a type="submit" class="btn btn-success pull-left btn-flat">
                            Guardar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection

@section('addjs')
    <script>
        $('#ref_id').select2();
        $(document).ready(function() {
            $('#ref_id').change(function() {
                var id = $(this).val();
                $.ajax({
                    url: 'listselectref/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                    }
                });
            });
        });
    </script>
@endsection
