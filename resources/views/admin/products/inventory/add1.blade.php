@extends('admin.layout')
@section('title-dash')
    {{ trans('labels.Inventory') }}...
@endsection
@section('title-link')
    <li class="breadcrumb-item"><a
            href="{{ URL::to('admin/products/display') }}">{{ trans('labels.ListingAllProducts') }}</a></li>
    @if ($result['products'][0]->products_type == 1)
        <li class="breadcrumb-item"><a
                href="{{ URL::to('admin/products/attach/attribute/display/' . $result['products'][0]->products_id) }}">{{ trans('labels.AddOptions') }}</a>
        </li>
    @endif
    <li class="breadcrumb-item active">{{ trans('labels.Inventory') }}</li>
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <a href="#" class="btn btn-primary pull-left btn-flat">Carregar Stock</a>
                        <a href="#" class="btn btn-success pull-left btn-flat" style="margin-left:10px">Baixar Modelo
                            em Excel(CSV)</a>
                        <small class="text-warning pull-right" style="color:red;">Em Desenvolvimento</small>
                    </div>

                    <div class="card-body">
                        @if (session()->has('message.level'))
                            <div class="alert alert-{{ session('message.level') }} alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                {!! session('message.content') !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-3 p-2">
                                <a class="btn btn-app bg-success" href="/admin/products/inventory/addnewstockref">
                                    <i class="fa fa-barcode"></i> Adicionar Por Ref.
                                </a>
                            </div>
                        </div>


                        {!! Form::open(['url' => 'admin/products/inventory/addnewstock', 'name' => 'inventoryfrom', 'id' => 'addewinventoryfrom', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name"
                                        class="col-md-12 control-label">{{ trans('labels.Products') }}<span
                                            style="color:red;">*</span> </label>
                                    <div class="col-md-12">
                                        <select class="form-control product-type" name="products_id" id="product_id">
                                            <option selected hidden disabled>Escolher Produto</option>
                                            @foreach ($result['products'] as $item)
                                                <option value="{{ $item->products_id }}">{{ $item->products_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <span class="help-block"
                                            style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.Product Type Text') }}.
                                        </span>
                                    </div>
                                </div>

                                <div id="attribute" style="display:none"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="form_div" style="display:none">
                                    <div class="callout callout-danger">
                                        <h4><b>{{ trans('labels.Add Stock') }}</b></h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name" class="col-md-12 control-label"> {{ trans('labels.Current Stock') }} : <span id="current_stocks" style="width:100%">0</span> </label>
                                                    <div class="col-md-12" hidden>
                                                        <input type="hidden" id="current_stocks_val" name="current_stocks_input" value="" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name" class="col-md-12 control-label">Tipo de Stock
                                                    </label>
                                                    <div class="col-md-12">
                                                        <select class="form-control" name="stock_type">
                                                            <option value="in">ENTRADA</option>
                                                            <option value="out">SAIDA</option>
                                                        </select>
                                                        <span class="help-block"
                                                            style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Escolha
                                                            Entrada ou Saida</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name"
                                                        class="col-md-12 control-label">{{ trans('labels.Enter Stock') }}<span
                                                            style="color:red;">*</span></label>
                                                    <div class="col-md-12">
                                                        <input type="text" name="stock" value=""
                                                            class="form-control stock-validate">
                                                        <span class="help-block"
                                                            style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Enter Stock Text') }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name"
                                                        class="col-md-12 control-label">Selecionar Referencia</label>
                                                    <div class="col-md-12">
                                                        <select class="form-control" id="ref_code" name="ref_code">
                                                            <option value="" selected disabled>Escolher Referencia</option>
                                                        </select>
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Escolha a referencia caso tenha</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name"
                                                        class="col-md-12 control-label">Referencia</label>
                                                    <div class="col-md-12">
                                                        <input type="text" name="reference_code" id="reference_code" value="" class="form-control">
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Escreva a Referencia caso não tenha</span>
                                                    </div>
                                                </div>
                                            </div>

                                            @if (count($result['products']) > 0)
                                                @if (count($result['attributes']) >= 0 and $result['products'][0]->products_type == 1 or $result['products'][0]->products_type == 0)
                                                    <div class="col-md-12 text-right">
                                                        <button type="submit" id="attribute-btn"
                                                            class="btn btn-primary pull-right btn-flat">{{ trans('labels.Add Stock') }}</button>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="card-footer text-right">
                        @if (count($result['products']) > 0 && $result['products'][0]->products_type == 1)
                            <a href="{{ URL::to('admin/products/attach/attribute/display/' . $result['products'][0]->products_id) }}"
                                class="btn btn-warning pull-left btn-flat">{{ trans('labels.AddOptions') }}</a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div id="form_attr" style="display:none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title"><b>{{ trans('labels.Manage Min/Max Quantity') }}</b></h3>
                                </div>
                                <div class="card-body">
                                    {!! Form::open(['url' => 'admin/products/inventory/addminmax', 'name' => 'addminmax', 'id' => 'addminmax', 'method' => 'post', 'class' => 'form-horizontal form-validate-level', 'enctype' => 'multipart/form-data']) !!}
                                    <input class="form-control check_reference_id" id="inventory_ref_id"
                                        name="inventory_ref_id" type="hidden" value="">
                                    <input class="form-control check_reference_id" id="inventory_pro_id"
                                        name="products_id" type="hidden" value="">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">
                                                    {{ trans('labels.Min Level') }}<span style="color:red;">*</span>
                                                </label>
                                                <div class="col-md-12">
                                                    <input type="text" name="min_level" id="min_level" value=""
                                                        class="form-control number-validate-level">
                                                    <span class="help-block"
                                                        style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                        {{ trans('labels.Min Level Text') }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="col-md-12 control-label">
                                                    {{ trans('labels.Max Level') }}<span style="color:red;">*</span>
                                                </label>
                                                <div class="col-md-12">
                                                    <input type="text" name="max_level" id="max_level" value=""
                                                        class="form-control number-validate-level">
                                                    <span class="help-block"
                                                        style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                        {{ trans('labels.Max Level Text') }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="alert alert-danger alert-dismissible" id="minmax-error"
                                            role="alert" style="display: none">
                                            <button type="button" class="close" data-bs-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ trans('labels.This stock is not asscociated with any attributes. Please choose products attributes first') }}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                                @if (count($result['products']) > 0)
                                    <div class="card-footer text-center">
                                        <button type="submit"
                                            class="btn btn-primary pull-right btn-flat">{{ trans('labels.Submit') }}</button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('addjs')
    <script>
        $('#product_id').select2();
        $(document).ready(function() {
            $('#product_id').change(function() {
                var id = $(this).val();
                $('#ref_code').find('option').not(':first').remove();
                $.ajax({
                    url: 'listselectref/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        var model = $('#ref_code');
                        model.empty();
                        $.each(response, function(index, element) {
                            console.log(element);
                            model.append("<option value='" + element.reference_code + "'>" + element.reference_code + "</option>");
                        });
                    }
                });
            });
        });
        $("#ref_code").change(function() {
            $("#reference_code").val($(this).val());
        });
    </script>
@endsection
