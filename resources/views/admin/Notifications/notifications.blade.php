@extends('admin.layout')
@section('title-dash'){{ trans('labels.SendNotification') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.SendNotification') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/devices/sendNotifications', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.SendNotification') }}</h3>
                </div>
                        
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-info alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    @if(count($result['message'])>0)
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $result['message'] }}
                        </div>
                    @endif


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Devices') }}</label>
                                <div class="col-md-12">
                                    <select class="form-control" name="device_type" id="device_type">
                                        @if($web_setting[66]->value=='1' and $web_setting[67]->value=='1' or $web_setting[66]->value=='1' and $web_setting[67]->value=='0')
                                            <option value="all">{{ trans('labels.All') }}</option>
                                        @endif
                                        @if($web_setting[66]->value=='1')
                                            <option value="1">{{ trans('labels.IOS') }} </option>
                                            <option value="2">{{ trans('labels.Android') }}</option>
                                        @endif
                                        @if($web_setting[67]->value=='1') 
                                            <option value="3">{{ trans('labels.Website') }}</option>
                                        @endif
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.SelectDeviceText') }}</span>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                <div class="col-md-12">
                                    <select class="form-control" name="devices_status" id="">
                                        <option value="1">{{ trans('labels.Active') }}</option>
                                        <option value="0">{{ trans('labels.Inactive') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.SelectDeviceStatusText') }} </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Title') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('title',  '', array('class'=>'form-control field-validate', 'id'=>'title'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TitleText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }}</label>
                                <div class="col-md-12 float-left">
                                    <div class="modal fade" id="Modalmanufactured" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title text-primary" id="myModalLabel">Escolher Imagem</h3>
                                                    <button type="button" class="close" data-bs-dismiss="modal" id ="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    <button type="button" id="AddImage" class="btn btn-primary pull-right btn-flat" >Add Image</button>
                                                </div>
                                                <div class="modal-body manufacturer-image-embed">
                                                    @if(isset($allimage))
                                                        <select class="image-picker show-html field-validate" name="image_id" id="select_img">
                                                            <option  value=""></option>
                                                            @foreach($allimage as $key=>$image)
                                                                <option data-img-src="{{asset($image->path)}}"  class="imagedetail" data-img-alt="{{$key}}" value="{{$image->path}}"> {{$image->path}} </option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary btn-flat" id="selected" data-bs-dismiss="modal">Concluir</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    {!! Form::button('Add Image', array('id'=>'newImage','class'=>"btn btn-primary btn-flat", 'data-bs-toggle'=>"modal", 'data-target'=>"#Modalmanufactured" )) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.notificationImageText') }}</span>
                                    <br>
                                    <div  id="selectedthumbnail" class="selectedthumbnail col-md-5"> </div>
                                    <div class="closimage">
                                        <button type="button" class="close pull-left image-close"  id="image-close" style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2;" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Message') }}</label>
                                <div class="col-md-12">
                                    {!! Form::textarea('message',  '', array('class'=>'form-control field-validate', 'id'=>'message'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.SendNotificationDetailext') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Send') }} </button>
                    <a href="{{ URL::to('admin/dashboard/this_month') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection