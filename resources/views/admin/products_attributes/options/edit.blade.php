@extends('admin.layout')
@section('title-dash'){{ trans('labels.Edit Values') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item"><a href="{{ URL::to("admin/products/attributes/display")}}">{{ trans('labels.ListingOptions') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.Edit Values') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.Edit Values') }}</h3>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    {!! Form::open(array('url' =>'admin/products/attributes/options/values/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                        {!! Form::hidden('products_options_values_id', $result['editoptions'][0]->products_options_values_id , array('class'=>'form-control', 'id'=>'products_options_values_id')) !!}

                        <div class="row">
                            @foreach($result['description'] as $description_data)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Option Value') }} ({{ $description_data['language_name'] }})</label>
                                        <div class="col-md-12">
                                            <input type="text" name="options_values_name_<?=$description_data['languages_id']?>" class="form-control field-validate" value="{{$description_data['name']}}">
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Option Value Text') }} ({{ $description_data['language_name'] }}).</span>
                                            <span class="help-block hidden">{{ trans('labels.Option Value Text') }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    {!! Form::close() !!}
                </div>
                
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{ URL::to("admin/products/attributes/options/values/display/".$result['editoptions'][0]->products_options_id)}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
