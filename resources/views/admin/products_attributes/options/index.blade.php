@extends('admin.layout')
@section('title-dash'){{ trans('labels.Options Value') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item active">{{ trans('labels.Options Value') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/products/attributes/options/values/insert', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title"> {{ trans('labels.Options Value for') }} @foreach($result['options'] as $key=>$option) <strong>{{$option->options_name}}</strong> @if(count($result['options']) != $key+1)<span> / </span> @endif  @endforeach</h3>
                    </div>
                    
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif

                        {!! Form::hidden('products_options_id', $result['options'][0]->products_options_id , array('class'=>'form-control', 'id'=>'products_options_values_id')) !!}

                        
                        <h4>{{ trans('labels.Add Option Value') }}</h4>

                        <div class="row">
                            <div class="col-md-4">
                                @foreach($result['languages'] as $languages)
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Option Value') }} ({{ $languages->name }})</label>
                                        <div class="col-md-12">
                                            <input type="text" name="ValuesName_<?=$languages->languages_id?>" class="form-control field-validate">
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Option Value Text') }} ({{ $languages->name }}).</span>
                                            <span class="help-block hidden">{{ trans('labels.Option Value Text') }}</span>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="">
                                    <a href="{{ URL::to('admin/products/attributes/display')}}" type="button" class="btn btn-default btn-flat pull-left"><i class="fa fa-angle-left"></i> {{ trans('labels.attributes') }}</a>
                                    <button type="submit" class="btn btn-primary btn-flat pull-right">{{ trans('labels.Add Values') }}</button>
                                </div>
                            </div>

                            

                            <div class="col-md-8">
                                <table id="myTable" class="table-striped">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('labels.ID') }}</th>
                                        <th>{{ trans('labels.Values') }}</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($result['content'])>0)
                                            @foreach($result['content'] as $data)
                                                <tr id="tr_parent_{{$data->products_options_values_id}}">
                                                    <td>{{$data->products_options_values_id}}</td>
                                                    <td dir="ltr">
                                                        @foreach($data->data as $language)
                                                            <p class="mb-0">
                                                                <strong>{{$language->name}}:</strong>
                                                                @if(count($language->values)>0)
                                                                    @foreach($language->values as $value)
                                                                        {{$value->options_values_name}}<br>
                                                                    @endforeach
                                                                @else
                                                                    --- <br>
                                                                @endif
                                                            </p>
                                                        @endforeach
                                                    </td>
                                                    <td><a data-toggle="tooltip" data-placement="bottom" title="" href="{{ URL::to('admin/products/attributes/options/values/edit')}}/{{$data->products_options_values_id}}" class="badge badge-secondary" data-original-title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                        <a href="javascript:void(0)" value_id="{{$data->products_options_values_id}}" option_id="{{$result['options'][0]->products_options_id}}" data-toggle="tooltip" data-placement="bottom" title="" class="badge badge-danger delete-value" data-original-title="Delete Value"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                                <div class="col-xs-12 text-right"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

    <div class="modal fade" id="productListModalValue" tabindex="-1" role="dialog" aria-labelledby="productListModalValueLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="productListModalValueLabel">{{ trans('labels.AssociatedProducts') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                  
                </div>
                <div class="modal-body">
                    <p><strong>{{ trans('labels.DeletingValueErrorMessage') }}</strong></p>
                    <ul style="padding:0" id="assciate-products-value">
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Ok') }}</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteValueModal" tabindex="-1" role="dialog" aria-labelledby="deleteValueModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteValueModalLabel">{{ trans('labels.DeleteValue') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/products/attributes/options/values/delete', 'name'=>'deleteValue', 'id'=>'deleteValue', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                    {!! Form::hidden('value_id',  '', array('class'=>'form-control', 'id'=>'value_id')) !!}
                    {!! Form::hidden('delete_language_id', '' , array('class'=>'form-control', 'id'=>'delete_language_id')) !!}
                    <div class="modal-body">
                        <p>{{ trans('labels.DeleteValuePrompt') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="button" class="btn btn-primary btn-flat" id="deleteAttribute">{{ trans('labels.DeleteValue') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</section>
@endsection
