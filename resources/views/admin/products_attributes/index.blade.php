@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllOptions') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item active">{{ trans('labels.Options') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"> {{ trans('labels.ListingAllOptions') }} </h3>
                    <div class="card-tools pull-right">
                        <a href="{{ URL::to('admin/products/attributes/add')}}" type="button" class="btn btn-block btn-primary btn-sm btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <table id="myTable" class="table-striped">
                                <thead>
                                    <tr>
                                        <th>{{ trans('labels.ID') }}</th>
                                        <th>{{ trans('labels.Options') }}</th>
                                        <th width="40%">{{ trans('labels.Values') }}</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($result['attributes'])>0)
                                        @foreach($result['attributes'] as $data)
                                            <tr>
                                                <td>{{$data->products_options_id}}</td>
                                                <td dir="ltr">
                                                    @foreach($data->data as $language)
                                                        <strong>{{$language->name}}:</strong>
                                                        @if(count($language->attributes)>0)
                                                            @foreach($language->attributes as $attribute)
                                                                {{$attribute->options_name}}<br>
                                                            @endforeach
                                                        @else
                                                            --- <br>
                                                        @endif
                                                    @endforeach
                                                    <a href="{{ URL::to('admin/products/attributes/edit')}}/{{$data->products_options_id}}" >{{ trans('labels.Manage Options') }}</a><br>
                                                </td>
                                                <td dir="ltr">
                                                    @foreach($data->data as $language)
                                                        <strong>{{$language->name}}:</strong>
                                                        @if(count($language->values)>0)
                                                            @foreach($language->values as $value)
                                                                {{$value->options_values_name}},
                                                            @endforeach
                                                        @else
                                                            --- <br>
                                                        @endif
                                                        <br>
                                                    @endforeach
                                                    <a href="{{ URL::to('admin/products/attributes/options/values/display')}}/{{$data->products_options_id}}" >{{ trans('labels.Manage Values') }}</a><br>
                                                </td>
                                                <td><a option_id="{{$data->products_options_id}}" class="btn btn-danger btn-flat deleteOption"><i class="fa fa-trash " aria-hidden="true"></i></a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteattributeModal" tabindex="-1" role="dialog" aria-labelledby="deleteAttributeModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteAttributeModalLabel">{{ trans('labels.DeleteOption') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                {!! Form::open(array('url' =>'admin/products/attributes/delete', 'name'=>'deleteAttribute', 'id'=>'deleteAttribute', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                {!! Form::hidden('option_id',  '', array('class'=>'form-control', 'id'=>'option_id')) !!}
                <div class="modal-body">
                    <p>{{ trans('labels.DeleteOptionPrompt') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                    <button type="submit" class="btn btn-primary btn-flat" id="deleteAttribute">{{ trans('labels.DeleteOption') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="productListModal" tabindex="-1" role="dialog" aria-labelledby="productListModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="productListModalLabel"></h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <p><strong>{{ trans('labels.DeletingErrorMessage') }}</strong></p>
                    <ul style="padding:0" id="assciate-products">
                    </ul>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Ok') }}</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
