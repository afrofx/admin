@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddOptions') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item"><a href="{{ URL::to("admin/products/attributes/display")}}">{{ trans('labels.ListingOptions') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.AddOptions') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/products/attributes/insert', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.AddOptions') }}</h3>
                    </div>

                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif

                        <div class="row">
                            <div class="col-md-6">
                                @foreach($result['languages'] as $languages)
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Name') }} ({{ $languages->name }})</label>
                                        <div class="col-md-12">
                                            <input type="text" name="OptionsName_<?=$languages->languages_id?>" class="form-control field-validate">
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Option Name Text') }} ({{ $languages->name }}).</span>
                                            <span class="help-block hidden">{{ trans('labels.Option Name Text') }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        <a href="{{ URL::to('admin/products/attributes/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
