@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddTaxClass') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{url('admin/tax/taxclass/display')}}">{{ trans('labels.ListingTaxClasses') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.AddTaxClass') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/tax/taxclass/add', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.AddTaxClass') }}</h3>
                </div>

                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Title') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('tax_class_title',  '', array('class'=>'form-control field-validate', 'id'=>'tax_class_title'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                        {{ trans('labels.TaxClassTitleText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }}
                                </label>
                                <div class="col-md-12">
                                    {!! Form::textarea('tax_class_description',  '', array('class'=>'form-control', 'id'=>'tax_class_description'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TaxClassDescriptionText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{url('admin/tax/taxclass/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
