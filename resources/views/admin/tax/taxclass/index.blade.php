@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingTaxClasses') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.TaxClasses') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/tax/taxclass/filter')}}">
                            <input type="hidden"  value="{{csrf_token()}}">
                            <div class="input-group search-panel ">
                                <select type="button" class="btn btn-default btn-flat dropdown-toggle form-control" data-bs-toggle="dropdown" name="FilterBy" id="FilterBy"  >
                                    <option value="" selected disabled hidden>{{ trans('labels.Filter By') }}</option>
                                    <option value="Title"  @if(isset($name)) @if  ($name == "Title") {{ 'selected' }} @endif @endif>{{ trans('labels.Title') }}</option>
                                    <option value="Description" @if(isset($name)) @if  ($name == "Description") {{ 'selected' }}@endif @endif>{{ trans('labels.Description') }}</option>
                                </select>
                                <input type="text" class="form-control input-group-form " name="parameter" placeholder="Pesquisar..." id="parameter" @if(isset($param)) value="{{$param}}" @endif >
                                <span class="input-group-append">
                                    <button class="btn btn-primary btn-flat " id="submit" type="submit"><span class="fa fa-search"></span></button>
                                    @if(isset($param,$name))  <a class="btn btn-danger btn-flat " href="{{url('admin/tax/taxclass/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-tools">
                        <a href="{{url('admin/tax/taxclass/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddTaxClass') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@sortablelink('tax_class_id', trans('labels.ID') )</th>
                                    <th>@sortablelink('tax_class_title', trans('labels.Title') )</th>
                                    <th>@sortablelink('tax_class_description', trans('labels.Description') )</th>
                                    <th>@sortablelink('created_at', trans('labels.Date') )</th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($tax_class as $key=>$data)
                                    <tr>
                                        <td>{{ $data->tax_class_id }}</td>
                                        <td>{{ $data->tax_class_title }}</td>
                                        <td width="30%">{{ $data->tax_class_description }}</td>
                                        <td>
                                            <strong>{{ trans('labels.AddedDate') }}: </strong>{{ $data->created_at }}<br>
                                            <strong>{{ trans('labels.LastModified') }}: </strong>{{ $data->updated_at }}
                                        </td>
                                        <td><a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{url('admin/tax/taxclass/edit/'. $data->tax_class_id) }}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteTaxClassId" tax_class_id ="{{ $data->tax_class_id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if($tax_class != null)
                            <div class="col-md-12 text-right">
                                {{$tax_class->links()}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="deleteTaxClassModal" tabindex="-1" role="dialog" aria-labelledby="deleteTaxClassModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteTaxClassModalLabel">{{ trans('labels.DeleteTaxClass') }}</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(array('url' =>'admin/tax/taxclass/delete', 'name'=>'deleteTaxClass', 'id'=>'deleteTaxClass', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'tax_class_id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeleteTaxClassText') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                <button type="submit" class="btn btn-primary btn-flat" id="deleteTaxClass">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
