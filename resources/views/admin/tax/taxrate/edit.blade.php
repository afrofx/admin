@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditTaxRate') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/tax/taxrates/display')}}">{{ trans('labels.TaxRates') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.EditTaxRate') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/tax/taxrates/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.EditTaxRate') }}</b></h3>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    @if(count($result['message'])>0)
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $result['message'] }}
                        </div>
                    @endif

                    <div class="row">
                        {!! Form::hidden('id', $result['taxrate']->tax_rates_id, array('class'=>'form-control', 'id'=>'tax_rate'))!!}

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.TaxClass') }}*  </label>
                                <div class="col-md-12">
                                    <select name="tax_class_id" class="form-control">
                                        @foreach($result['tax_class'] as $tax_class)
                                            <option @if($result['taxrate']->tax_class_id == $tax_class->tax_class_id) selected @endif value="{{ $tax_class->tax_class_id }}"> {{ $tax_class->tax_class_title }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseTaxClass') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Zone') }}* </label>
                                <div class="col-md-12">
                                    <select name="tax_zone_id" class="form-control">
                                        @foreach($result['zones'] as $zones)
                                            <option @if($result['taxrate']->tax_zone_id == $zones->zone_id) selected @endif value="{{ $zones->zone_id }}"> {{ $zones->zone_name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.AddTaxRateText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.AddTaxRatePercentage') }}*</label>
                                <div class="col-md-12">
                                    {!! Form::text('tax_rate',  $result['taxrate']->tax_rate, array('class'=>'form-control number-validate', 'id'=>'tax_rate'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.AddTaxRatePercentageText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.NumericValueError') }}</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }}
                                </label>
                                <div class="col-md-12">
                                    {!! Form::textarea('tax_description',  $result['taxrate']->tax_description, array('class'=>'form-control', 'id'=>'tax_description'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TaxDescription') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{ URL::to('admin/tax/taxrates/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
