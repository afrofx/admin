@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllTaxRates') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.TaxRates') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/tax/taxrates/filter')}}">
                            <input type="hidden"  value="{{csrf_token()}}">
                            <div class="input-group search-panel ">
                                <select type="button" class="btn btn-default btn-flat dropdown-toggle form-control" data-bs-toggle="dropdown" name="FilterBy" id="FilterBy"  >
                                    <option value="" selected disabled hidden>{{ trans('labels.Filter By') }}</option>
                                    <option value="Zone"  @if(isset($name)) @if  ($name == "Zone") {{ 'selected' }} @endif @endif>{{ trans('labels.Zone') }}</option>
                                    <!-- <option value="TaxRates" @if(isset($name)) @if  ($name == "TaxRates") {{ 'selected' }}@endif @endif>{{ trans('labels.TaxRates') }}</option> -->
                                    <option value="TaxClass" @if(isset($name)) @if  ($name == "TaxClass") {{ 'selected' }}@endif @endif>{{ trans('labels.TaxClass') }}</option>
                                </select>
                                <input type="text" class="form-control input-group-form " name="parameter" placeholder="Pesquisar..." id="parameter" @if(isset($param)) value="{{$param}}" @endif >
                                <span class="input-group-append">
                                    <button class="btn btn-primary btn-flat " id="submit" type="submit"><span class="fa fa-search"></span></button>
                                    @if(isset($param,$name))  <a class="btn btn-danger btn-flat " href="{{url('admin/tax/taxrates/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-tools">
                        <a href="{{ URL::to('admin/tax/taxrates/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddTaxRate') }}</a>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@sortablelink('tax_rates_id', trans('labels.ID') )</th>
                                    <th>@sortablelink('zone_name', trans('labels.Zone') )</th>
                                    <th>@sortablelink('tax_rate', trans('labels.TaxRates') )</th>
                                    <th>@sortablelink('tax_class_title', trans('labels.TaxClass') )</th>
                                    <th>@sortablelink('created_at', trans('labels.Date') )</th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($result['tax_rates'] as $key=>$taxRate)
                                    <tr>
                                        <td>{{ $taxRate->tax_rates_id }}</td>
                                        <td>{{ $taxRate->zone_name }}</td>
                                        <td>{{ $taxRate->tax_rate }}</td>
                                        <td>{{ $taxRate->tax_class_title }}</td>
                                        <td>
                                            <strong>{{ trans('labels.AddedDate') }}: </strong>{{ $taxRate->created_at }}<br>
                                            <strong>{{ trans('labels.LastModified') }}: </strong>{{ $taxRate->updated_at }}
                                        </td>
                                        <td><a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{ URL::to('admin/tax/taxrates/edit/'.$taxRate->tax_rates_id) }}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteTaxRateId" tax_rates_id ="{{ $taxRate->tax_rates_id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if($result['tax_rates'] != null)
                            <div class="col-xs-12 text-right">
                                {{$result['tax_rates']->links()}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="deleteTaxRateModal" tabindex="-1" role="dialog" aria-labelledby="deletetaxRateModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteTaxRateModalLabel">{{ trans('labels.DeleteTaxRate') }}</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(array('url' =>'admin/tax/taxrates/delete', 'name'=>'deletetaxRate', 'id'=>'deletetaxRate', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'tax_rates_id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeleteTaxRateText') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                <button type="submit" class="btn btn-primary btn-flat" id="deletetaxRate">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
