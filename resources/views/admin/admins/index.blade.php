@extends('admin.layout')
@section('title-dash'){{ trans('labels.admins') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item active">{{ trans('labels.admins') }}</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<h3 class="card-title">{{ trans('labels.admins') }} </h3>
					<div class="card-tools right">
						<a href="{{ URL::to('admin/addadmins') }}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.addadmins') }}</a>
					</div>
				</div>
				
				<div class="card-body">
					@if (count($errors) > 0)
						@if ($errors->any())
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ $errors->first() }}
							</div>
						@endif
					@endif

					<div class="row">
						<div class="col-xs-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>{{ trans('labels.ID') }}</th>
										<th>{{ trans('labels.Full Name') }}</th>
										<th>{{ trans('labels.Email') }}</th>
										<th>{{ trans('labels.AdminType') }}</th>
										<th>{{ trans('labels.Status') }}</th>
										<th>{{ trans('labels.Action') }}</th>
									</tr>
								</thead>
								<tbody>
									@if (count($result['admins']) > 0)
										@foreach ($result['admins'] as $key => $admin)
											<tr>
												<td>{{ $admin->id }}</td>
												<td>{{ $admin->first_name }} {{ $admin->last_name }} </td>
												<td>{{ $admin->email }}</td>
												<td>
													@if ($admin->user_types_id == 1) <strong class="badge badge-success"> 
													@else <strong class="badge bg-secondary"> @endif {{ $admin->user_types_name }}</strong>
												</td>
												<td>
													@if ($admin->isActive == 1)
														<strong class="badge badge-success">{{ trans('labels.Active') }}</strong>
													@else
														<strong class="badge bg-secondary">{{ trans('labels.InActive') }}</strong>
													@endif
												</td>
												<td>
													<div class="dropdown">
														<a class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
															{{ trans('labels.Action') }}
														</a>
														<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														  <li><a class="dropdown-item" href="editadmin/{{ $admin->id }}">{{ trans('labels.editadmin') }}</a></li>
														  <li><a class="dropdown-item" data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" customers_id="{{ $admin->id }}" id="deleteCustomerFrom">{{ trans('labels.Delete') }}</a></li>
														</ul>
													</div>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
										</tr>
									@endif
								</tbody>
							</table>
							@if (count($result['admins']) > 0)
								<div class="col-md-12 text-right">
									{{ $result['admins']->links() }}
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="deleteCustomerModal" tabindex="-1" role="dialog" aria-labelledby="deleteCustomerModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="deleteCustomerModalLabel">{{ trans('labels.deleteAdmin') }}</h4>
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>

				{!! Form::open(['url' => 'admin/deleteadmin', 'name' => 'deleteAdmin', 'id' => 'deleteAdmin', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
					{!! Form::hidden('action', 'delete', ['class' => 'form-control']) !!}
					{!! Form::hidden('id', '', ['class' => 'form-control', 'id' => 'customers_id']) !!}
					<div class="modal-body">
						<p>{{ trans('labels.Are you sure you want to delete this admin') }}</p>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
						<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Delete') }}</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content notificationContent">

			</div>
		</div>
	</div>

</section>
@endsection
