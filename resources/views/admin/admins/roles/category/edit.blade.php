@extends('admin.layout')
@section('title-dash'){{ trans('labels.Assign Categories') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item"><a href="{{ URL::to('admin/categoriesroles') }}">{{ trans('labels.categoriesroles') }}</a></li>
	<li class="breadcrumb-item active">{{ trans('labels.Assign Categories') }}</li>
@endsection
@section('content')
<section class="content">
	{!! Form::open(['url' => 'admin/updatecategoriesroles', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
		<div class="row">
			<div class="col-md-12">
				<div class="card card-danger card-outline">
					<div class="card-header">
						<h3 class="card-title">{{ trans('labels.Assign Categories') }} </h3>
					</div>
					
					<div class="card-body">
						@if (session()->has('error'))
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ session()->get('error') }}
							</div>
						@endif

						@if (session()->has('success'))
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ session()->get('success') }}
							</div>
						@endif
						
						<input type="hidden" name="categories_role_id" value="{{ $result['data'][0]->categories_role_id }}">

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.ChooseAdmin') }}</label>
									<div class="col-md-12">
										<select class="form-control field-validate" name="admin_id" disabled>
											@foreach ($result['admins'] as $admins)
												@if ($result['data'][0]->admin_id == $admins->myid)
													<option value="{{ $admins->myid }}">
														{{ $admins->first_name }} {{ $admins->last_name }}
													</option>
												@endif
											@endforeach
										</select>
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.ChooseAdminText') }}. </span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								@php
									$cat_array = explode(',', $result['data'][0]->categories_ids);
								@endphp

								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Categories') }}</label>
									<div class="col-md-12">
										<ul class="list-group list-group-root well">
											@foreach ($result['categories'] as $categories)
												<li href="#" class="list-group-item">
													<label style="width:100%">
														<input @if (in_array($categories->id, $cat_array)) checked @endif id="categories_<?= $categories->id ?>" type="checkbox" class=" required_one categories" name="categories[]" value="{{ $categories->id }}"> {{ $categories->name }}
													</label>
												</li>

												@if (!empty($categories->sub_categories))
													<ul class="list-group">
														<li class="list-group-item">
															@foreach ($categories->sub_categories as $sub_category)
																<label>
																	<input type="checkbox" name="categories[]" class="required_one sub_categories sub_categories_<?= $categories->id ?>" parents_id='<?= $categories->id ?>' value="{{ $sub_category->sub_id }}" @if (in_array($sub_category->sub_id, $cat_array)) checked @endif>{{ $sub_category->sub_name }}
																</label>
															@endforeach
														</li>

													</ul>
												@endif
											@endforeach
										</ul>
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.ChooseCatgoryText') }}.</span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="card-footer text-center">
						<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Assign') }}</button>
						<a href="{{ URL::to('admin/categoriesroles') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
					</div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
</section>
@endsection
