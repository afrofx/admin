@extends('admin.layout')
@section('title-dash'){{ trans('labels.CategoriesRoles') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item active">{{ trans('labels.CategoriesRoles') }}</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<h3 class="card-title">{{ trans('labels.CategoriesRoles') }} </h3>
					<div class="card-tools right">
						<a href="{{ URL::to('admin/addcategoriesroles') }}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.Assign Categories') }}</a>
					</div>
				</div>
				
				<div class="card-body">
					@if (count($errors) > 0)
						@if ($errors->any())
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ $errors->first() }}
							</div>
						@endif
					@endif
					
					<div class="row">
						<div class="col-md-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>{{ trans('labels.ID') }}</th>
										<th>{{ trans('labels.admin') }}</th>
										<th width="40%">{{ trans('labels.Categories') }}</th>
										<th>{{ trans('labels.Action') }}</th>
									</tr>
								</thead>

								<tbody>
									@if (count($result['data']) > 0)
										@foreach ($result['data'] as $key => $data)
											<tr>
												<td>{{ $data->categories_role_id }}</td>
												<td>{{ $data->first_name }} {{ $data->last_name }}</td>
												<td width="40%">
													@foreach ($data->description as $descriptions)
														{{ $descriptions->categories_name }},
													@endforeach
												</td>
												<td>
													<a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="editcategoriesroles/{{ $data->categories_role_id }}" class="badge badge-secondary">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
													</a>
													<a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" href="deletecategoriesroles/{{ $data->categories_role_id }}" class="badge badge-danger">
														<i class="fa fa-trash" aria-hidden="true"></i>
													</a>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="6">{{ trans('labels.NoRecordFound') }}</td>
										</tr>
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
