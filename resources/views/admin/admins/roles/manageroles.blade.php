@extends('admin.layout')
@section('title-dash'){{ trans('labels.manageroles') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item active">{{ trans('labels.manageroles') }}</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card">

				<div class="card-header">
					<h3 class="card-title">{{ trans('labels.manageroles') }} </h3>
					<div class="card-tools right">
						<a href="{{ URL::to('admin/addadmintype') }}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.addadmintype') }}</a>
					</div>
				</div>
				
				<div class="card-body">
					@if (count($errors) > 0)
						@if ($errors->any())
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ $errors->first() }}
							</div>
						@endif
					@endif
					
					<div class="row">
						<div class="col-md-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>{{ trans('labels.ID') }}</th>
										<th>{{ trans('labels.types') }}</th>
										<th>{{ trans('labels.Date') }}</th>
										<th>{{ trans('labels.Status') }}</th>
										<th>{{ trans('labels.Manage Role') }}</th>
										<th>{{ trans('labels.Action') }}</th>
									</tr>
								</thead>
								<tbody>
									@if (count($result['adminTypes']) > 0)
										@foreach ($result['adminTypes'] as $key => $adminType)
											<tr>
												<td>{{ $key + 1 }}</td>
												<td>{{ $adminType->user_types_name }}</td>
												<td>
													<strong>{{ trans('labels.AddedDate') }}:
													</strong>{{ date('d-m-Y', $adminType->created_at) }}<br>
													<strong>{{ trans('labels.ModifiedDate') }}: </strong>
													@if (!empty($adminType->updated_at))
														{{ date('d-m-Y', $adminType->updated_at) }}
													@else
														---
													@endif
												</td>
												<td>
													@if ($adminType->isActive == 1)
														<strong class="badge badge-success">{{ trans('labels.Active') }}
														</strong>
													@else
														<strong
															class="badge badge-secondary">{{ trans('labels.InActive') }}
														</strong>
													@endif

												</td>
												<td>
													<a href="addrole/{{ $adminType->user_types_id }}" class="manage-role-popup" user_types_id="{{ $adminType->user_types_id }}"> {{ trans('labels.Manage Role') }}</a>
												</td>
												<td>
													<div class="dropdown">
														<a class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
															{{ trans('labels.Action') }}
														</a>
														<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														  <li><a class="dropdown-item" href="editadmintype/{{ $adminType->user_types_id }}">{{ trans('labels.editadmintype') }}</a></li>
														  <li><a class="dropdown-item" data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteCustomerFrom" customers_id="{{ $adminType->user_types_id }}">{{ trans('labels.Delete') }}</a></li>
														</ul>
													</div> 
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
										</tr>
									@endif
								</tbody>
							</table>
							@if (count($result['adminTypes']) > 0)
								<div class="col-md-12 text-right">
									{{ $result['adminTypes']->links() }}
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="deleteCustomerModal" tabindex="-1" role="dialog"
		aria-labelledby="deleteCustomerModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="deleteCustomerModalLabel">{{ trans('labels.AdminType') }}</h4>
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				{!! Form::open(['url' => 'admin/deleteadmintype', 'name' => 'deleteadmintype', 'id' => 'deleteadmintype', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
					{!! Form::hidden('action', 'delete', ['class' => 'form-control']) !!}
					{!! Form::hidden('user_types_id', '', ['class' => 'form-control', 'id' => 'customers_id']) !!}
					<div class="modal-body">
						<p>{{ trans('labels.Are you sure you want to delete this admin type') }}</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
						<button type="submit" class="btn btn-primary btn-primary btn-flat">{{ trans('labels.Delete') }}</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="modal fade" id="manageRoleModal" tabindex="-1" role="dialog"
		aria-labelledby="manageRoleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" id="manage-role-content">
			</div>
		</div>
	</div>
</section>
@endsection
