@extends('admin.layout')
@section('title-dash'){{ trans('labels.editadmintype') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item"><a href="{{ URL::to('admin/manageroles') }}"> {{ trans('labels.manageroles') }}</a></li>
	<li class="breadcrumb-item active">{{ trans('labels.editadmintype') }}</li>
@endsection
@section('content')
<section class="content">
	{!! Form::open(['url' => 'admin/updatetype', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<h3 class="card-title">{{ trans('labels.editadmintype') }} </h3>
				</div>
				
				<div class="card-body ">
					@if (session()->has('message'))
						<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							{{ session()->get('message') }}
						</div>
					@endif

					@if (session()->has('errorMessage'))
						<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							{{ session()->get('errorMessage') }}
						</div>
					@endif

							
					<div class="row">	
						{!! Form::hidden('user_types_id', $result['user_types'][0]->user_types_id, ['class' => 'form-control', 'id' => 'user_types_id']) !!}
									
						<div class="col-md-6">	
							<div class="form-group">
								<label for="name" class="col-md-12  control-label">{{ trans('labels.AdminTypeName') }} </label>
								<div class="col-md-12">
									{!! Form::text('user_types_name', $result['user_types'][0]->user_types_name, ['class' => 'form-control field-validate', 'id' => 'user_types_name']) !!}
									<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.AdminTypeNameText') }}</span>
									<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
								</div>
							</div>
						</div>
						
						<div class="col-md-6">	
							<div class="form-group">
								<label for="name" class="col-md-12  control-label">{{ trans('labels.Status') }}</label>
								<div class="col-md-12">
									<select class="form-control" name="isActive">
										<option value="1" @if ($result['user_types'][0]->isActive == 1) selected @endif>
											{{ trans('labels.Active') }}
										</option>
										<option value="0" @if ($result['user_types'][0]->isActive == 0) selected @endif>
											{{ trans('labels.Inactive') }}
										</option>
									</select>
									<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.AdminTypeStatusText') }}</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card-footer text-center">
					<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
					<a href="{{ URL::to('admin/manageroles') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</section>
@endsection
