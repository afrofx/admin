@extends('admin.layout')
@section('title-dash'){{ trans('labels.Add Roles') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item"><a href="{{ URL::to('admin/manageroles') }}">{{ trans('labels.manageroles') }}</a></li>
	<li class="breadcrumb-item active">{{ trans('labels.Add Roles') }}</li>
@endsection
@section('content')
<section class="content">
	{!! Form::open(['url' => 'admin/addnewroles', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
		<div class="row">
			<div class="col-md-12">
				<div class="card card-danger card-outline">
					<div class="card-header">
						<h3 class="card-title"><strong>{{ trans('labels.Type') }}:</strong> {{ $result['adminType'][0]->user_types_name }} </h3>
					</div>
					
					<div class="card-body">
						@if (session()->has('message'))
							<div class="alert alert-success" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ session()->get('message') }}
							</div>
						@endif
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									@foreach ($result['data'] as $datas)
										{!! Form::hidden('user_types_id', $result['user_types_id'], ['class' => 'form-control', 'id' => 'user_types_id']) !!}
										<div class="col-md-6">
											<hr>
											<h5>{{ trans('labels.manage ' . $datas['link_name']) }} </h5>
											<hr>
											@foreach ($datas['permissions'] as $data)
												<div class="form-group">
													<label class="col-md-12 control-label" style="">{{ trans('labels.manage ' . $data['name']) }}</label>
													<div class="col-md-12">
														<label class=" control-label">
															<input type="radio" name="{{ $data['name'] }}" value="1" class="flat-red" @if ($data['value'] == 1) checked @endif> &nbsp;{{ trans('labels.Yes') }}
														</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

														<label class=" control-label">
															<input type="radio" name="{{ $data['name'] }}" value="0" class="flat-red" @if ($data['value'] == 0) checked @endif> &nbsp;{{ trans('labels.No') }}
														</label>
													</div>
												</div>
											@endforeach
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					
					<div class="card-footer ">
						<a style="float:right;" href="{{ URL::to('admin/dashboard/this_month') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
						<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
					</div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
</section>
@endsection
