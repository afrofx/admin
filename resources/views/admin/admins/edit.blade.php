@extends('admin.layout')
	@section('title-dash') {{trans('labels.editadmin')}}...@endsection
	@section('title-link') 
		<li class="breadcrumb-item"><a href="{{URL::to('admin/admins')}}">{{ trans('labels.admins') }}</a></li>
		<li class="breadcrumb-item active">{{trans('labels.editadmin')}}</li>
	@endsection
@section('content')
<section class="content">
	{!! Form::open(['url' => 'admin/updateadmin', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
		<div class="row">
			<div class="col-md-12">
				<div class="card card-danger card-outline">
					<div class="card-header">
						<h3 class="card-title">{{ trans('labels.editadmin') }} </h3>
					</div>
					
					<div class="card-body">
						@if (session()->has('message'))
							<div class="alert alert-success" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ session()->get('message') }}
							</div>
						@endif

						@if (session()->has('errorMessage'))
							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ session()->get('errorMessage') }}
							</div>
						@endif

						<div class="row">
							<div class="col-md-6">
								{!! Form::hidden('myid', $result['myid'], ['id' => 'myid']) !!}
							</div>
							
							<div class="col-md-12">
								<h4>{{ trans('labels.Personal Info') }} </h4>
								<hr>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.AdminType') }}</label>
									<div class="col-md-12">
										<select class="form-control" name="adminType">
											@foreach ($result['adminTypes'] as $adminType)
												<option value="{{ $adminType->user_types_id }}" @if ($result['admins'][0]->role_id == $adminType->user_types_id) selected @endif>
													{{ $adminType->user_types_name }}
												</option>
											@endforeach
										</select>
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.AdminTypeText') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.FirstName') }} </label>
									<div class="col-md-12">
										{!! Form::text('first_name', $result['admins'][0]->first_name, ['class' => 'form-control field-validate', 'id' => 'first_name']) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.LastName') }} </label>
									<div class="col-md-12">
										{!! Form::text('last_name', $result['admins'][0]->last_name, ['class' => 'form-control field-validate', 'id' => 'last_name']) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.lastNameText') }}</span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Telephone') }}</label>
									<div class="col-md-12">
										{!! Form::text('phone', $result['admins'][0]->phone, ['class' => 'form-control', 'id' => 'phone']) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
											{{ trans('labels.TelephoneText') }}
										</span>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<h4>{{ trans('labels.Login Info') }}</h4> 
								<hr>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.EmailAddress') }} </label>
									<div class="col-md-12">
										{!! Form::text('email', $result['admins'][0]->email, ['class' => 'form-control email-validate', 'id' => 'email']) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.EmailText') }}</span>
										<span class="help-block hidden"> {{ trans('labels.EmailError') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.EmailAddress') }} </label>
									<div class="col-md-12">
										{!! Form::text('email', $result['admins'][0]->email, ['class' => 'form-control email-validate', 'id' => 'email']) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.EmailText') }}</span>
										<span class="help-block hidden"> {{ trans('labels.EmailError') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name"
										class="col-md-12 control-label">{{ trans('labels.changePassword') }}</label>
									<div class="col-md-12">
										{!! Form::checkbox('changePassword', 'yes', null, ['class' => '', 'id' => 'change-passowrd']) !!}
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Password') }}</label>
									<div class="col-md-12">
										{!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.PasswordText') }}</span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
									<div class="col-md-12">
										<select class="form-control" name="isActive">
											<option value="1" @if ($result['admins'][0]->status == 1) selected @endif>{{ trans('labels.Active') }}</option>
											<option value="0" @if ($result['admins'][0]->status == 0) selected @endif>{{ trans('labels.Inactive') }}</option>
										</select>
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusText') }}</span>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card-footer text-center">
						<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
						<a href="{{ URL::to('admin/admins') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
					</div>

				</div>
			</div>
		</div>
	{!! Form::close() !!}
</section>
@endsection
