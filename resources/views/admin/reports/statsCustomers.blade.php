@extends('admin.layout')
@section('title-dash')
    Relatório de Clientes...
@endsection
@section('title-link')
    <li class="breadcrumb-item active">Relatório de Clientes</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-success card-outline">
				<div class="card-header with-border">
					<h3 class="card-title"><b>Filtrar:</b></h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>

				<div class="card-body no-padding">
					<form name='registration' method="get" action="{{ url('admin/customers-orders-report') }}">
						<input type="hidden" name="type" value="all">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="dateRange">Data de Inicio e Fim</label>
									<input class="form-control reservation dateRange" placeholder="Data de Inicio e Fim" readonly value="{{ app('request')->input('dateRange') }}" name="dateRange" aria-label="Text input with multiple buttons ">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="exampleInputEmail1">Escolher Cliente</label>
									<select type="button" class="btn btn-default select2 form-control" required
										data-toggle="dropdown" name="customers_id" id="customers_id">
										<option value="">Escolher Cliente</option>
										@foreach ($result['customers'] as $customers)
											<option value="{{ $customers->id }}"
												@if (app('request')->input('customers_id')) @if (app('request')->input('customers_id') == $customers->id) {{ 'selected' }} @endif
												@endif>{{ $customers->first_name }}
												{{ $customers->last_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<!-- <div class="col-xs-2">
				<div class="form-group">
				<label for="exampleInputEmail1">{{ trans('labels.currency') }}</label>
				<select type="button" class="btn btn-default select2 form-control" data-toggle="dropdown" name="currency" id="currency">
					@foreach ($result['currency'] as $currency)
<option value="{{ $currency->symbol_left ? $currency->symbol_left : $currency->symbol_right }}"  @if (app('request')->input('currency')) @if (app('request')->input('currency') == $currency->symbol_right || app('request')->input('currency') == $currency->symbol_left) {{ 'selected' }} @endif @endif>{{ $currency->symbol_left ? $currency->symbol_left : $currency->symbol_right }}</option>
@endforeach
				</select>
				</div>
			</div>             -->
							<div class="col-md-2">
								<div class="form-group" style="padding-top: 30px">
									<button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
									@if (app('request')->input('type') and app('request')->input('type') == 'all') 
									<a class="btn btn-danger btn-flat" href="{{ url('admin/customers-orders-report') }}"><i class="fa fa-ban" aria-hidden="true"></i> </a>
									@endif
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<div class="card-title">
						<form method="get" action="{{ url('admin/customers-orders-report') }}">
							<input type="hidden" name="type" value="id">
							<input type="hidden" value="{{ csrf_token() }}">
							<div class="input-group search-panel">
								<input class="form-control" placeholder="ID da Compra" value="{{ app('request')->input('orderid') }}" name="orderid" aria-label="Text input with multiple buttons ">
								<span class="input-group-append">
									<button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
									@if (app('request')->input('type') and app('request')->input('type') == 'id')
										<a class="btn btn-danger btn-flat" href="{{ url('admin/customers-orders-report') }}"><i class="fa fa-ban" aria-hidden="true"></i> </a>
									@endif
								</span>
							</div>
						</form>
					</div>
					<div class="card-tools">
						<form action="{{ URL::to('admin/customer-orders-print') }}" target="_blank">
							<input type="hidden" name="page" value="invioce">
							<input type="hidden" name="customers_id" value="{{ app('request')->input('customers_id') }}">
							<input type="hidden" name="orders_status_id" value="{{ app('request')->input('orders_status_id') }}">
							<input type="hidden" name="dateRange" value="{{ app('request')->input('dateRange') }}">
							<input type="hidden" name="orderid" value="{{ app('request')->input('orderid') }}">
							<button type='submit' class="btn btn-warning pull-right btn-flat"><i class="fa fa-print"></i> Imprimir</button>
						</form>
					</div>
				</div>
				
				<div class="card-body">

					<div class="row">
						<div class="col-md-12">
							<?php $totalSale = 0; ?>
							<div class="pull-right">
								<h4><small>Valor Total:</small>  {{ $result['price'] }} MT</h4>
							</div>
						</div>

						<div class="col-md-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>{{ trans('labels.ID') }}</th>
										<th>{{ trans('labels.CustomerName') }}</th>
										<th>Compra Feita Por</th>
										<th>Valor Total</th>
										<th>{{ trans('labels.DatePurchased') }}</th>
										<th>{{ trans('labels.Status') }} </th>
										<th>{{ trans('labels.Action') }}</th>
									</tr>
								</thead>
								<tbody>
									@if (count($result['reports']['orders']) > 0)
										@foreach ($result['reports']['orders'] as $key => $orderData)
											<tr>
												<td>{{ $orderData->orders_id }}</td>
												<td>{{ $orderData->customers_name }}</td>
												<td>
													@if ($orderData->ordered_source == 1)
														{{ trans('labels.Website') }}
													@else
														{{ trans('labels.Application') }}
													@endif
												</td>
												<td>
													@if ($result['commonContent']['currency']->symbol_left == app('request')->input('currency'))
														{{ app('request')->input('currency') }}
														@endif {{ $orderData->order_price }} @if ($result['commonContent']['currency']->symbol_left != app('request')->input('currency'))
															{{ app('request')->input('currency') }}
														@endif
												</td>
												<td>{{ date('d/m/Y', strtotime($orderData->date_purchased)) }}
												</td>
												<td>
													@if ($orderData->orders_status_id == 1)
														<span class="badge badge-warning">
													@elseif($orderData->orders_status_id == 2)
														<span class="badge badge-success">
													@elseif($orderData->orders_status_id == 3)
														<span class="badge badge-danger">
													@else
														<span class="badge badge-primary">
													@endif
													{{ $orderData->orders_status }}
													</span>
												</td>

												<td>
													<a data-bs-toggle="tooltip" target="_blank" data-placement="bottom" title="{{ trans('labels.View Invoice') }}" href="{{ url('admin/orders/invoiceprint/' . $orderData->orders_id) }}" class="btn btn-primary btn-flat"><i class="fa fa-eye" aria-hidden="true"></i>{{ trans('labels.View Invoice') }}</a>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong>
											</td>
										</tr>
									@endif
								</tbody>
							</table>
						</div>

						<div class="col-md-12" style="background: #eee;">
							@if (count($result['reports']['orders']) > 0)
								@php
									
									if ($result['reports']['orders']->total() > 0) {
										$fromrecord = ($result['reports']['orders']->currentpage() - 1) * $result['reports']['orders']->perpage() + 1;
									} else {
										$fromrecord = 0;
									}
									if ($result['reports']['orders']->total() < $result['reports']['orders']->currentpage() * $result['reports']['orders']->perpage()) {
										$torecord = $result['reports']['orders']->total();
									} else {
										$torecord = $result['reports']['orders']->currentpage() * $result['reports']['orders']->perpage();
									}
									
								@endphp
								<div class="col-xs-12 col-md-6" style="padding:30px 15px; border-radius:5px;">
									<div>Showing {{ $fromrecord }} to {{ $torecord }}
										of {{ $result['reports']['orders']->total() }} entries
									</div>
								</div>
								<div class="col-xs-12 col-md-6 text-right">
									{{ $result['reports']['orders']->appends(\Request::except('type'))->render() }}
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
