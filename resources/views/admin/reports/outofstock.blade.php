@extends('admin.layout')
@section('title-dash')
    {{ trans('labels.Productsoutofstock') }}...
@endsection
@section('title-link')
    <li class="breadcrumb-item active">{{ trans('labels.Productsoutofstock') }}</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<h3 class="card-title"><b>{{ trans('labels.Productsoutofstock') }}</b></h3>
					<div class="card-tools pull-right">
						<form action="{{ URL::to('admin/outofstockprint') }}" target="_blank">
							<input type="hidden" name="page" value="invioce">
							<button type='submit' class="btn btn-default pull-right btn-flat"><i class="fa fa-print"></i>Imprimir</button>
						</form>
					</div>
				</div>
				

				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>ID Produto</th>
										<th>{{ trans('labels.ProductName') }}</th>
										<th>{{ trans('labels.Current Stock') }}</th>
										<th>Operação</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($result['reports'] as $key => $report)
										<tr>
											<td>{{ $report->products_id }}</td>
											<td>{{ $report->products_name }}</td>
											<td>0</td>
											<td>
												<a data-bs-toggle="tooltip" target="_blank" data-placement="bottom" title="{{ trans('labels.View') }}" href="{{ url('admin/inventoryreport?type=all&products_id=' . $report->products_id) }}" class="btn btn-primary btn-flat"><i class="fa fa-eye" aria-hidden="true"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						
						<div class="col-md-12" style="background: #eee;">
							@php
								if ($result['reports']->total() > 0) {
									$fromrecord = ($result['reports']->currentpage() - 1) * $result['reports']->perpage() + 1;
								} else {
									$fromrecord = 0;
								}
								if ($result['reports']->total() < $result['reports']->currentpage() * $result['reports']->perpage()) {
									$torecord = $result['reports']->total();
								} else {
									$torecord = $result['reports']->currentpage() * $result['reports']->perpage();
								}
								
							@endphp
							<div class="col-xs-12 col-md-6" style="padding:30px 15px; border-radius:5px;">
								<div>Showing {{ $fromrecord }} to {{ $torecord }}
									of {{ $result['reports']->total() }} entries
								</div>
							</div>
							<div class="col-xs-12 col-md-6 text-right">
								{{ $result['reports']->appends(\Request::except('type'))->render() }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
@endsection
