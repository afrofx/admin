@extends('admin.layout')
@section('title-dash')
    Relatório de Cupões...
@endsection
@section('title-link')
    <li class="breadcrumb-item active">Relatório de Cupões</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-success card-outline">
				<div class="card-header with-border">
					<h3 class="card-title"><b>Filtrar</b></h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"> <i class="fa fa-minus"></i> </button>
						<button type="button" class="btn btn-tool" data-card-widget="remove"> <i class="fa fa-times"></i> </button>
					</div>
				</div>

				<div class="card-body">
					<form name='registration' method="get" action="{{ url('admin/couponreport') }}">
						<input type="hidden" name="type" value="all">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Data de inicio e Fim</label>
									<input class="form-control reservation dateRange" placeholder="Data de inicio e Fim" readonly value="{{ app('request')->input('dateRange') }}" name="dateRange" aria-label="Text input with multiple buttons ">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Codigo do Cupão</label>
									<input class="form-control" placeholder="Codigo do Cupão" value="{{ app('request')->input('couponcode') }}" name="couponcode" aria-label="Text input with multiple buttons ">
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group" style="padding-top:30px">
									<button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
									@if (app('request')->input('type') and app('request')->input('type') == 'all')
										<a class="btn btn-danger btn-danger" href="{{ url('admin/couponreport') }}"><i class="fa fa-ban" aria-hidden="true"></i> </a>
									@endif
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<div class="card-title">
					</div>
					<div class="card-tools">
						<form action="{{ URL::to('admin/couponreport-print') }}" target="_blank">
							<input type="hidden" name="page" value="invioce">
							<input type="hidden" name="couponcode" value="{{ app('request')->input('couponcode') }}">
							<input type="hidden" name="dateRange" value="{{ app('request')->input('dateRange') }}">
							<button type='submit' class="btn btn-warning pull-right btn-flat"><i class="fa fa-print"></i> Imprimir</button>
						</form>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Codigo do Cupão</th>
										<th>Desconto</th>
										<th>ID Compra</th>
										<th>{{ trans('labels.CustomerName') }}</th>
										<th>Valor Total</th>
										<th>Data da Compra</th>
										<th>{{ trans('labels.Action') }}</th>
									</tr>
								</thead>
								<tbody>
									@if (count($result['reports']['orders']) > 0)
										@foreach ($result['reports']['orders'] as $key => $orderData)
											<tr>
												<td>{{ $orderData->coupon_code }}</td>
												<td>{{ $orderData->coupon_amount }}</td>
												<td>{{ $orderData->orders_id }}</td>
												<td>{{ $orderData->customers_name }}</td>
												<td>
													@if (!empty($result['commonContent']['currency']->symbol_left))
														{{ $result['commonContent']['currency']->symbol_left }}
														@endif {{ $orderData->order_price }} @if (!empty($result['commonContent']['currency']->symbol_right))
															{{ $result['commonContent']['currency']->symbol_right }}
														@endif
												</td>
												<td>
													{{ date('d/m/Y', strtotime($orderData->date_purchased)) }}
												</td>
												<td>
													<a data-bs-toggle="tooltip" target="_blank" data-placement="bottom" title="{{ trans('labels.View Invoice') }}" href="{{ url('admin/orders/invoiceprint/' . $orderData->orders_id) }}" class="btn btn-primary btn-flat"><i class="fa fa-eye" aria-hidden="true"></i></a>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong>
											</td>
										</tr>
									@endif
								</tbody>
							</table>
						</div>

						<div class="col-md-12" style="background: #eee;">
							@php
								if ($result['reports']['orders']->total() > 0) {
									$fromrecord = ($result['reports']['orders']->currentpage() - 1) * $result['reports']['orders']->perpage() + 1;
								} else {
									$fromrecord = 0;
								}
								if ($result['reports']['orders']->total() < $result['reports']['orders']->currentpage() * $result['reports']['orders']->perpage()) {
									$torecord = $result['reports']['orders']->total();
								} else {
									$torecord = $result['reports']['orders']->currentpage() * $result['reports']['orders']->perpage();
								}
								
							@endphp
							<div class="col-xs-12 col-md-6" style="padding:30px 15px; border-radius:5px;">
								<div>Showing {{ $fromrecord }} to {{ $torecord }}
									of {{ $result['reports']['orders']->total() }} entries
								</div>
							</div>
							<div class="col-xs-12 col-md-6 text-right">
								{{ $result['reports']['orders']->appends(\Request::except('type'))->render() }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
