@extends('admin.layout')
@section('title-dash')
    Relatório de Vendas...
@endsection
@section('title-link')
    <li class="breadcrumb-item active">Relatório de Vendas</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<div class="card-title">
						<form method="get" action="{{ url('admin/salesreport') }}">
							<input type="hidden" name="type" value="id">
							<input type="hidden" value="{{ csrf_token() }}">
							<div class="input-group search-panel">
								<input class="form-control reservation dateRange" placeholder="Data de Inicio e Fim" readonly value="{{ app('request')->input('dateRange') }}" name="dateRange" aria-label="Text input with multiple buttons ">
								<span class="input-group-append">
									<button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
									@if (app('request')->input('type') and app('request')->input('type') == 'all') 
									<a class="btn btn-danger btn-flat" href="{{ url('admin/salesreport') }}"><i  class="fa fa-ban" aria-hidden="true"></i> </a>
									@endif
								</span>
							</div>
						</form>
					</div>

					<div class="card-tools">
						<form action="{{ URL::to('admin/customer-orders-print') }}" target="_blank">
							<input type="hidden" name="page" value="invioce">
							<input type="hidden" name="customers_id" value="{{ app('request')->input('customers_id') }}">
							<input type="hidden" name="orders_status_id" value="{{ app('request')->input('orders_status_id') }}">
							<input type="hidden" name="deliveryboys_id" value="{{ app('request')->input('deliveryboys_id') }}">
							<input type="hidden" name="dateRange" value="{{ app('request')->input('dateRange') }}">
							<input type="hidden" name="orderid" value="{{ app('request')->input('orderid') }}">
							<button type='submit' class="btn btn-warning pull-right btn-flat"><i class="fa fa-print"></i> Imprimir</button>
						</form>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<!-- <h2><small>{{ trans('labels.Total Sale Price') }}:</small>
@if ($result['commonContent']['currency']->symbol_left == app('request')->input('currency')) {{ app('request')->input('currency') }} @endif {{ $result['price'] }} @if ($result['commonContent']['currency']->symbol_left != app('request')->input('currency')) {{ app('request')->input('currency') }} @endif
</h2> -->
								<h2><small>Valor total: {{ $result['price'] }} MT</h2>
							</div>
						</div>

						<div class="col-md-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>{{ trans('labels.Date') }}</th>
										<th>Numero de Compras</th>
										<th>Valor Total</th>
									</tr>
								</thead>
								<tbody>
									@if (count($result['reports']['orders']) > 0)
										@foreach ($result['reports']['orders'] as $key => $orderData)
											<tr>
												<td>{{ $orderData->date_purchased }}</td>
												<td>{{ $orderData->total_orders }}</td>
												<td>{{ $orderData->total_price }}</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong>
											</td>
										</tr>
									@endif
								</tbody>
							</table>
						</div>

						<div class="col-md-12" style="background: #eee;">
							<div class="col-xs-12 col-md-6 text-right">
								{{ $result['reports']['orders']->appends(\Request::except('type'))->render() }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

<!-- <div class="col-md-3">
								<div class="form-group" style="width:100%">
								<label for="exampleInputEmail1">{{ trans('labels.currency') }}</label> &nbsp
								<select type="button" class="btn btn-default select2 form-control" data-toggle="dropdown" name="currency" id="currency" style="width:100%;">
									@foreach ($result['currency'] as $currency)
<option value="{{ $currency->symbol_left ? $currency->symbol_left : $currency->symbol_right }}"  @if (app('request')->input('currency')) @if (app('request')->input('currency') == $currency->symbol_right || app('request')->input('currency') == $currency->symbol_left) {{ 'selected' }} @endif @endif>{{ $currency->symbol_left ? $currency->symbol_left : $currency->symbol_right }}</option>
@endforeach
								</select>
								</div>
							</div>  -->
