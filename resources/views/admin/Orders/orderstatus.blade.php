@extends('admin.layout')
@section('title-dash'){{ trans('labels.OrderStatus') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.OrderStatus') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.ListingOrderStatus') }}</b></h3>
                    <div class="card-tools pull-right">
                        <a href="addorderstatus" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddOrderStatus') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{ trans('labels.ID') }}</th>
                                    <th>{{ trans('labels.OrderStatus') }}</th>
                                    <th style="display:none">{{ trans('labels.Default') }}</th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($result['orders_status'] as $OrderStatus)
                                    <tr>
                                        <td>{{ $OrderStatus->orders_status_id }}</td>
                                        <td>{{ $OrderStatus->orders_status_name }}</td>
                                        <!-- <td>@if($OrderStatus->public_flag==1) {{ trans('labels.Yes') }}  @else {{ trans('labels.No') }} @endif</td> -->
                                       
                                        <td>
                                            <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="editorderstatus/{{ $OrderStatus->orders_status_id }}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteOrderStatusId" orders_status_id ="{{ $OrderStatus->orders_status_id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>        
                        </div>
                        <div class="col-md-12 text-right">
                            {{$result['orders_status']->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteOrderStatusModal" tabindex="-1" role="dialog" aria-labelledby="deleteOrderStatusModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteOrderStatusModalLabel">{{ trans('labels.DeleteOrderStatus') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/orders/deleteOrderStatus', 'name'=>'deleteOrderStatus', 'id'=>'deleteOrderStatus', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'orders_status_id')) !!}
                <div class="modal-body">
                    <p>{{ trans('labels.DeleteOrderStatusText') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                    <button type="submit" class="btn btn-primary btn-flat" id="deleteOrderStatus">{{ trans('labels.Delete') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
