@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddOrderStatus') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item "><a href="{{ URL::to('admin/orders/orderstatus')}}">{{ trans('labels.ListingOrderStatus') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.AddOrderStatus') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/orders/addNewOrderStatus', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.AddOrderStatus') }}</b></h3>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-6" hidden>
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Status Type') }}</label>
                                <div class="col-md-12">
                                    <select name="role_id" class="form-control" >
                                        <option value="2"  selected>{{ trans('labels.General') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                      {{ trans('labels.StatusLanguageText') }}</span>
                                </div>
                            </div>
                        </div>

                        @foreach($result['languages'] as $key=>$languages)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.OrdersStatus') }} ({{ $languages->name }})</label>
                                    <div class="col-md-12">
                                        <input type="text" name="OrdersStatus_<?=$languages->languages_id?>" class="form-control field-validate"  >
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.textRequiredFieldMessage') }} ({{ $languages->name }}).</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        
                        <div class="col-md-6" style="display:none;">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Set Default') }}</label>
                                <div class="col-md-12">
                                    <select name="public_flag" class="form-control">
                                        <option value="0"  selected>{{ trans('labels.No') }}</option>
                                        <option value="1" >{{ trans('labels.Yes') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                      {{ trans('labels.StatusLanguageText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="orderstatus" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
