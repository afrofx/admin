@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllOrders') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.ListingAllOrders') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.ListingAllOrders') }}</b></h3>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{ trans('labels.ID') }}</th>
                                    <th>{{ trans('labels.CustomerName') }}</th>
                                    <th>Feita por</th>
                                    <th>Valor Total</th>
                                    <th>{{ trans('labels.DatePurchased') }}</th>
                                    <th>{{ trans('labels.Status') }} </th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($listingOrders['orders'])>0)
                                    @foreach ($listingOrders['orders'] as $key=>$orderData)
                                        <tr>
                                            <td>{{ $orderData->orders_id }}</td>
                                            <td>{{ $orderData->customers_name }}</td>
                                            <td>
                                                @if($orderData->ordered_source == 1)
                                                Website
                                                @else
                                                 Aplicativo
                                                @endif</td>
                                            <td>
                                                
                                            @if(!empty($result['commonContent']['currency']->symbol_left) && $result['commonContent']['currency']->symbol_left == $orderData->currency)  {{ $orderData->currency }}  {{ $orderData->order_price *  $orderData->currency_value }} @else  {{ $orderData->order_price *  $orderData->currency_value }}  {{ $orderData->currency }} @endif</td>
                                            <td>{{ date('d/m/Y', strtotime($orderData->date_purchased)) }}</td>
                                            <td>
                                                @if($orderData->orders_status_id==1)
                                                    <span class="badge badge-warning">
                                                @elseif($orderData->orders_status_id==2)
                                                    <span class="badge badge-success">
                                                @elseif($orderData->orders_status_id==3)
                                                    <span class="badge badge-danger">
                                                @else
                                                    <span class="badge badge-primary">
                                                @endif
                                                {{ $orderData->orders_status }}
                                                    </span>
                                            </td>
                                            
                                            <td>
                                                <a data-bs-toggle="tooltip" data-placement="bottom" title="View Order" href="vieworder/{{ $orderData->orders_id }}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                                <a data-bs-toggle="tooltip" data-placement="bottom" title="Delete Order" id="deleteOrdersId" orders_id ="{{ $orderData->orders_id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                            </td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 text-right">
                            {{$listingOrders['orders']->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- deleteModal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteModalLabel">{{ trans('labels.DeleteOrder') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/orders/deleteOrder', 'name'=>'deleteOrder', 'id'=>'deleteOrder', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                {!! Form::hidden('orders_id',  '', array('class'=>'form-control', 'id'=>'orders_id')) !!}
                <div class="modal-body">
                    <p>{{ trans('labels.DeleteOrderText') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                    <button type="submit" class="btn btn-primary btn-flat" id="deleteOrder">{{ trans('labels.Delete') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
