@extends('admin.layout')
@section('title-dash')  {{ trans('labels.AdminProfile') }}...@endsection
@section('title-link')
<li class="breadcrumb-item active"> {{ trans('labels.AdminProfile') }}</li>
@endsection
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<div class="card-title">
						<h4>Perfil do Usuario</h4>
					</div>
				</div>
				<div class="card-body">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
						  <button class="nav-link active" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="true">Perfil</button>
						  <button class="nav-link" id="nav-password-tab" data-bs-toggle="tab" data-bs-target="#passwordDiv" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Senha</button>
						</div>
					</nav>
		
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="nav-profile-tab">
							@if (count($errors) > 0)
								<div class="alert alert-danger alert-dismissible">
									<button aria-hidden="true" data-bs-dismiss="alert" class="close" type="button">X</button>
									{{ $errors->first() }}
								</div>
							@endif
							
							{!! Form::open(['url' => 'admin/admin/update', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
								<div class="row">
									{!! Form::hidden('myid', auth()->user()->myid, ['class' => 'form-control', 'id' => 'myid']) !!}
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputName" class="col-md-12 control-label">{{ trans('labels.AdminFirstName') }}</label>
											<div class="col-md-12">
												{!! Form::text('first_name', Auth()->user()->first_name, ['class' => 'form-control', 'id' => 'first_name']) !!}
												<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
													{{ trans('labels.AdminFirstNameText') }}
												</span>
											</div>
										</div>
									</div>
		
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputEmail" class="col-md-12 control-label">{{ trans('labels.LastName') }}</label>
											<div class="col-md-12">
												{!! Form::text('last_name', auth()->user()->last_name, ['class' => 'form-control', 'id' => 'last_name']) !!}
												<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
													{{ trans('labels.AdminLastNameText') }}
												</span>
											</div>
										</div>
									</div>
		
		
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputName" class="col-md-12 control-label">{{ trans('labels.Address') }}</label>
											<div class="col-md-12">
												{!! Form::text('address', $result['admin']->entry_street_address, ['class' => 'form-control', 'id' => 'address']) !!}
												<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
													{{ trans('labels.AddressText') }}
												</span>
											</div>
										</div>
									</div>
		
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputExperience" class="col-md-12 control-label">{{ trans('labels.City') }} </label>
											<div class="col-md-12">
												{!! Form::text('city', $result['admin']->entry_city, ['class' => 'form-control', 'id' => 'city']) !!}
												<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
													{{ trans('labels.CityText') }}
												</span>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputSkills" class="col-md-12 control-label">{{ trans('labels.Country') }}</label>
											<div class="col-md-12">
												<select class="form-control" name="country" id="entry_country_id">
													<option value="">{{ trans('labels.SelectCountry') }}</option>
													@foreach ($result['countries'] as $countries)
														<option @if ($result['admin']->entry_country_id == $countries->countries_id) selected @endif value="{{ $countries->countries_id }}">
															{{ $countries->countries_name }}
														</option>
													@endforeach
												</select>
												<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
													{{ trans('labels.CountryText') }}
												</span>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputSkills" class="col-md-12 control-label">{{ trans('labels.State') }}</label>
											<div class="col-md-12">
												<select class="form-control zoneContent" name="state">
													<option value="">{{ trans('labels.SelectZone') }}</option>
													@foreach ($result['zones'] as $zones)
														<option @if ($result['admin']->entry_state == $zones->zone_id) selected @endif value="{{ $zones->zone_id }}">
															{{ $zones->zone_name }}
														</option>
													@endforeach
												</select>
												<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
													{{ trans('labels.SelectZoneText') }}
												</span>
											</div>
										</div>
									</div>
		
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputExperience" class="col-md-12 control-label">{{ trans('labels.ZipCode') }}</label>
											<div class="col-md-12">
												{!! Form::text('zip', $result['admin']->entry_postcode, ['class' => 'form-control', 'id' => 'zip']) !!}
											</div>
										</div>
									</div>
		
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputExperience" class="col-md-12 control-label">{{ trans('labels.Phone') }}</label>
											<div class="col-md-12">
												{!! Form::text('phone', auth()->user()->phone, ['class' => 'form-control', 'id' => 'phone']) !!}
												<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> 
													{{ trans('labels.PhoneText') }}
												</span>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<button type="submit"
													class="btn btn-success btn-flat">{{ trans('labels.Submit') }}</button>
											</div>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
		
		
						<div class="tab-pane fade" id="passwordDiv" role="tabpanel" aria-labelledby="nav-password-tab">
							{!! Form::open(['url' => 'admin/admin/updatepassword', 'onSubmit' => 'return validatePasswordForm()', 'id' => 'updateAdminPassword', 'name' => 'updateAdminPassword', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
								<div class="row">
									<div class="col-md-6">
										<div class="form-group form-group-email">
											<label for="email" class="col-md-12 control-label">{{ trans('labels.Email') }}</label>
											<div class="col-md-12">
												<input type="text" class="form-control" id="email"
													value="{{ $result['admin']->email }}" name="email" placeholder="email">
												<span class="help-block"
													style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.AdminPasswordRestriction') }}</span>
												<span style="display: none" class="help-block"></span>
											</div>
										</div>
									</div>
		
									<div class="col-md-6">
										<div class="form-group">
											<label for="password"
												class="col-md-12 control-label">{{ trans('labels.NewPassword') }}</label>
											<div class="col-md-12">
												<input type="password" class="form-control" id="password" name="password"
													placeholder="New Password">
												<span class="help-block"
													style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.AdminPasswordRestriction') }}</span>
												<span style="display: none" class="help-block"></span>
											</div>
										</div>
									</div>
		
									<div class="col-md-6">
										<div class="form-group">
											<label for="re-password"
												class="col-md-12 control-label">{{ trans('labels.Re-EnterPassword') }}</label>
											<div class="col-md-12">
												<input type="password" class="form-control" id="re_password" name="re_password"
													placeholder="Re-Enter Password">
												<span class="help-block"
													style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.AdminPasswordRestriction') }}</span>
												<span style="display: none" class="help-block"></span>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<button type="submit" class="btn btn-success btn-flat">
													{{ trans('labels.Submit') }}
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
