@extends('admin.layout')
@section('title-dash'){{ trans('labels.Update Project') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.Update Project') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/managements/updatercontent', 'id' =>'updater-form', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">

                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.Update Project / Bug Fixer') }}</h3>
                    </div>
                        
                    <div class="card-body">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.NOTE') }}</label>
                                    <p class="col-md-12">
                                        {{ trans('labels.Text of updater and bug zip file') }}
                                        </br>
                                        {{ trans('labels.For source code Updator. Please update admin code first then other zip files.') }}
                                        </br>
                                        <b style="color: red">Please take backup of your code as well as database before performing any update.
                                        Author would not be responsible for any loss or change in your own code customization.</b>
                                    </p>
                                </div>
                            </div>

                            
                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Choose Zip') }}</label>
                                    <div class="col-md-12">
                                        <input type="file" name="zip_file" id="file" class="form-control field-validate">
                                        <span class="help-block hidden">{{ trans('labels.Choose Zip Text') }}</span>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Purchase Code') }}</label>
                                    <div class="col-md-12">
                                        <input type="text" name="purchase_code"  class="form-control field-validate">
                                        <span class="help-block">{{ trans('labels.Purchase Code Text') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-flat" id="password-confirm-btn" >{{ trans('labels.Submit') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>

<div class="modal fade" id="checkpassword" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p style="text-align:center">{{ trans('labels.Update source code confirm password text') }}</p>
                <div class="form-group" id="imageIcone">
                    <label for="name" class="col-sm-3 col-md-4 control-label">{{ trans('labels.Password') }}</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                </div>
                <div class="alert alert-danger" id="passowrd-error" style="display: none">
                    {{ trans('labels.Please enter your valid passowrd.') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-flat" id="check-password">{{ trans('labels.Confirm') }}</button>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
