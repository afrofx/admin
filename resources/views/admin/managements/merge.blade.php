@extends('admin.layout')
@section('title-dash')Actualizar o Sistema...@endsection
@section('title-link') 
<li class="breadcrumb-item active">Actualizar o Sistema</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/managements/mergecontent', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Actualizar o Sistema</h3>
                    </div>
                    
                    <div class="card-body">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $execution_time = ini_get('max_execution_time');
                                $upload_size = ini_get('upload_max_filesize'); 
                                $upload_size = str_replace('M','',$upload_size);

                                $post_size = ini_get('post_max_size');  
                                $post_size = str_replace('M','',$post_size);     
                                ?>
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">NOTA</label>
                                    <p class="col-md-12">Your maximum file upload size, post max size and execution time do not match to the given requirements. Please fix it. Otherwise you will face problem while merging project.
                                    </br>
                                    <strong>Max Execution Time:</strong> 180 </br>
                                    <strong>Upload File Size:</strong> 128M</br>
                                    <strong>Post Max Size:</strong> 128M </p>
                                </div>
                                
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">Escolher Ficheiro Zip</label>
                                    <div class="col-md-12">
                                        <input type="file" name="zip_file" id="file" class="form-control field-validate">
                                        <span class="help-block">Por favor carregue o 'web.zip' ou 'app.zip'</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">Licensa</label>
                                    <div class="col-md-12">
                                        <input type="text" name="purchase_code"  class="form-control field-validate">
                                        <span class="help-block">Escreva a licensa</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
