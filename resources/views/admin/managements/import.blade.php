@extends('admin.layout')
@section('title-dash')Importar Dados...@endsection
@section('title-link') 
<li class="breadcrumb-item active">Importar Dados</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/managements/importdata', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">

                    <div class="card-header">
                        <h3 class="card-title">Importar Dados</h3>
                    </div>
                    
                    <div class="card-body">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">NOTA</label>
                                    <p class="col-md-12">{{ trans('labels.Text for import zip file') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">Escolher Ficheiro em ZIP</label>
                                    <div class="col-md-12">
                                        <input type="file" name="zip_file" id="file" class="form-control field-validate">
                                        <span class="help-block hidden">Carregue o "web.zip" ou o "app.zip"</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">Licensa</label>
                                    <div class="col-md-12">
                                        <input type="text" name="purchase_code"  class="form-control field-validate">
                                        <span class="help-block">Escreva a licensa</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
