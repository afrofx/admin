@extends('admin.layout')
@section('title-dash') {{ trans('labels.Back Up / Restore') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.Back Up / Restore') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/managements/take_backup', 'id' =>'updater-form', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">

                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.Back Up / Restore') }}</h3>
                    </div>
                    
                    <div class="card-body">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="control-label">Licensa</label>
                                    <div class="col-md-6">
                                        <input type="text" name="purchase_code"  class="form-control field-validate">
                                        <span class="help-block">Por favor escreva a licensa de compra</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-left">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-flat" id="password-confirm-btn" >{{ trans('labels.Take Back Up') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>

<div class="modal fade" id="checkpassword" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p style="text-align:center">{{ trans('labels.Update source code confirm password text') }}</p>
                <div class="form-group" id="imageIcone">
                    <label for="name" class="col-sm-3 col-md-4 control-label">{{ trans('labels.Password') }}</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                </div>
                <div class="alert alert-danger" id="passowrd-error" style="display: none">
                    {{ trans('labels.Please enter your valid passowrd.') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-flat" id="check-password">{{ trans('labels.Confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
