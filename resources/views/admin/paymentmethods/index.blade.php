@extends('admin.layout')
@section('title-dash'){{ trans('labels.PaymentMethods') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.PaymentMethods') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h4 class="card-title"><b>Metodos de Pagamento</b></h4>
                </div>

                <div class="card-body">
                    @if ($errors)
                        @if($errors->any())
                            <div @if ($errors->first()=='Default can not Deleted!!') class="alert alert-danger alert-dismissible" @else class="alert alert-success alert-dismissible" @endif role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{trans('labels.ID')}}</th>
                                        <th>{{trans('labels.Active')}}</th>
                                        <th>{{ trans('labels.PaymentMethods')}}</th>
                                        <th>{{trans('labels.Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result['methods'] as $method)
                                        @if($method->payment_methods_id != 9 and $method->payment_methods_id != 10 and $method->payment_methods_id != 11)
                                            <tr>
                                                <td>
                                                    {{$method->payment_methods_id}}
                                                </td>
                                                <td>
                                                    <label>
                                                        <input type="checkbox" @if($method->status==1) checked @endif name="payment_methods_id" value="{{$method->payment_methods_id}}"  class="default_pay_method" >
                                                    </label>
                                                </td>
                                                <td>{{$method->name}}</td>
                                                <td>
                                                    <a href="{{url('admin/paymentmethods/display')}}/{{$method->payment_methods_id}}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
