@extends('admin.layout')
@section('title-dash'){{ trans('labels.Edit') }} {{$result['methods']['method_detail'][1]['name']}}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/paymentmethods/index')}}">{{ trans('labels.PaymentSetting') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.Edit') }} {{$result['methods']['method_detail'][1]['name']}}</li>
@endsection
@section('content')
<section class="content">
    <form enctype="multipart/form-data" class='form-validate' action="{{ URL::to('admin/paymentmethods/update')}}" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.Edit') }} {{$result['methods']['method_detail'][1]['name']}}</b></h3>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{$errors->first()}}
                        </div>
                        @endif
                    @endif

                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$id}}">
                    
                    <div class="row">
                        @if($id != 9)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="shippingEnvironment"vclass="col-md-12 control-label"vstyle="">{{ trans('labels.Enviroment') }}</label>
                                    <div class="col-md-12">
                                        <label class=" control-label">
                                            <input type="radio" name="environment" value="0" class="flat-red" @if($result['methods']['payment_methods']['environment']==0) checked @endif > &nbsp;{{ trans('labels.Sanbox') }}
                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label class=" control-label">
                                            <input type="radio" name="environment" value="1" class="flat-red" @if($result['methods']['payment_methods']['environment']==1) checked @endif > &nbsp;{{ trans('labels.Live') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @foreach($result['methods']['method_keys'] as $res)
                        @if($res->keyname != 'paymentcurrency')
                        @if($res->keyname == 'paystackcallbackurl')
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.'.$res->keyname) }}</label>
                                <div class="col-md-12">                                                                     
                                    <input type="text" name="<?php echo 'field_' .$res->key; ?>" id="braintree_merchant_id" value="{{url('/')}}{{$res->value}}" class="form-control field-validate" readonly>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.'.$res->keyname) }}</label>
                                <div class="col-md-12">
                                    @if($res->key == 'short_code' or $res->key == 'iban' or $res->key == 'swift')
                                        <input type="text" name="<?php echo 'field_' .$res->key; ?>" id="braintree_merchant_id" value="{{$res->value}}" class="form-control">
                                    @else
                                        <input type="text" name="<?php echo 'field_' .$res->key; ?>" id="braintree_merchant_id" value="{{$res->value}}" class="form-control field-validate">
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        @endif
                        @endforeach
                        <div class="col-md-12">
                            <hr>
                            <h4>Tradução</h4>
                            <hr>
                        </div>
                        @foreach($result['methods']['method_detail'] as $detail)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Name') }} ({{ $detail['language_name'] }}) </label>
                                        <div class="col-md-12">
                                        <input type="text" name="name_<?=$detail['languages_id']?>" class="form-control field-validate" value="{{$detail['name']}}">
                                    </div>
                                </div>
                            </div>
                            @if($id == 9)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Descriptions') }} ({{ $detail['language_name'] }}) </label>
                                    <div class="col-md-12">
                                        <textarea name="descriptions_<?=$detail['languages_id']?>"  class="form-control field-validate" id="" rows="2">{{$detail['descritions']}}</textarea>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary payment-checkcard btn-flat">{{ trans('labels.Submit') }} </button>
                    <a href="{{ URL::to('admin/paymentmethods/index')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>
@endsection