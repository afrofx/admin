@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditNews') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/news/display')}}">{{ trans('labels.ListingAllNews') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.EditNews') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/news/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.EditNews') }} </b></h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="fa fa-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Error') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    {!! Form::hidden('id',  $result['news'][0]->news_id, array('class'=>'form-control', 'id'=>'id')) !!}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Category') }}</label>
                                <div class="col-md-12">
                                    <select class="form-control field-validate" id="category_id" name="category_id">
                                        <option value="">Escolher Categoria</option>
                                        @foreach ($result['categories'] as $categories)
                                            <option @if(!empty($result['editCategory'][0]->categories_id)) @if($result['editCategory'][0]->categories_id == $categories->id ) selected @endif @endif value="{{ $categories->id }}">{{ $categories->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseNewsCategory') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.slug') }} </label>
                                <div class="col-md-12">
                                    <input type="hidden" name="old_slug" value="{{$result['news'][0]->news_slug}}">
                                    <input type="text" name="slug" class="form-control field-validate" value="{{$result['news'][0]->news_slug}}">
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.slugText') }}</span>
                                </div>
                            </div>
                        </div>
                        @foreach($result['description'] as $description_data)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.TitleNews') }} ({{ $description_data['language_name'] }})</label>
                                    <div class="col-md-12">
                                        <input type="text" name="news_name_<?=$description_data['languages_id']?>" class="form-control field-validate" value="{{$description_data['news_name']}}">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TitleNews') }} ({{ $description_data['language_name'] }})</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }} ({{ $description_data['language_name'] }})</label>
                                    <div class="col-md-12">
                                        <textarea id="editor<?=$description_data['languages_id']?>" name="news_description_<?=$description_data['languages_id']?>" class="form-control"  rows="10">{{ stripslashes($description_data['news_description']) }}</textarea>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Description') }} ({{ $description_data['language_name'] }})</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }}</label>
                                <div class="col-md-12">
                                    <!-- Modal -->
                                    <div class="modal fade" id="Modalmanufactured" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                    <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body manufacturer-image-embed">
                                                    @if(isset($allimage))
                                                    <select class="image-picker show-html " name="image_id" id="select_img">
                                                        <option value=""></option>
                                                        @foreach($allimage as $key=>$image)
                                                        <option data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                        @endforeach
                                                    </select>
                                                    @endif
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary pull-left  btn-flat" >{{ trans('labels.Add Image') }}</a>
                                                    <button type="button" class="btn btn-default refresh-image btn-flat"><i class="fa fa-refresh"></i></button>
                                                    <button type="button" class="btn btn-primary btn-flat" id="selected" data-bs-dismiss="modal">Concluir</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        {!! Form::button(trans('labels.Add Image'), array('id'=>'newImage','class'=>"btn btn-primary btn-flat", 'data-bs-toggle'=>"modal", 'data-target'=>"#Modalmanufactured" )) !!}
                                        <br>
                                        <div id="selectedthumbnail" class="selectedthumbnail col-md-5"> </div>
                                        <div class="closimage">
                                            <button type="button" class="close pull-left image-close " id="image-close"
                                            style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.UploadSubCategoryImage') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label"></label>
                                <div class="col-md-12">
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.OldImage') }}</span>
                                    {!! Form::hidden('oldImage',  $result['news'][0]->news_image , array('id'=>'oldImage')) !!}
                                    <img src="{{asset($result['news'][0]->path)}}" alt="" width=" 100px">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.IsFeature') }}</label>
                                <div class="col-md-12">
                                    <select class="form-control" name="is_feature">
                                        <option value="1" @if($result['news'][0]->is_feature==1) selected @endif >{{ trans('labels.Yes') }}</option>
                                        <option value="0" @if($result['news'][0]->is_feature==0) selected @endif>{{ trans('labels.No') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.IsFeatureText') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                <div class="col-md-12">
                                    <select class="form-control" name="news_status">
                                        <option value="1" @if($result['news'][0]->news_status==1) selected @endif>{{ trans('labels.Active') }}</option>
                                        <option value="0" @if($result['news'][0]->news_status==0) selected @endif>{{ trans('labels.Inactive') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Active status will be displayed on user side.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{ URL::to('admin/news/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection


@section('addjs')
    <script type="text/javascript">
        $(function() {
            @foreach($result['languages'] as $languages)
            CKEDITOR.replace('editor{{$languages->languages_id}}');
            @endforeach
            $(".textarea").wysihtml5();
        });

        
    </script>
    <script>
        $("#select_img").imagepicker()
     </script>
@endsection