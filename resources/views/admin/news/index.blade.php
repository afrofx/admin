@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllNews') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.News') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/news/filter')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="input-group  search-panel ">
                                <select type="button" class="btn btn-default dropdown-toggle form-control input-group-form " data-toggle="dropdown" name="FilterBy" id="FilterBy" >
                                    <option value="" selected disabled hidden>{{trans('labels.Filter By')}}</option>
                                    <option value="Name"  @if(isset($name)) @if  ($name == "Name") {{ 'selected' }} @endif @endif>{{trans('labels.Name')}}</option>
                                </select>
                                <input type="text" class="form-control input-group" name="parameter" placeholder="Pesquisar..." id="parameter"  @if(isset($param)) value="{{$param}}" @endif >
                                <span class="input-group-append">
                                    <button class="btn btn-primary " id="submit" type="submit"><span class="fa fa-search"></span></button>
                                    @if(isset($param,$name))  <a class="btn btn-danger btn-flat" href="{{url('admin/news/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-tools">
                        <a href="{{ URL::to('admin/news/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if ($errors != null)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@sortablelink('news_id', trans('labels.ID') )</th>
                                    <th>{{ trans('labels.Image') }}</th>
                                    <th>@sortablelink('news_name', trans('labels.Name') )</th>
                                    <th>@sortablelink('is_feature', trans('labels.Feature') )</th>
                                    <th>@sortablelink('created_at', trans('labels.AddedLastModifiedDate') )</th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($news != null)
                                    @php $newslist = $news->unique('news_id') @endphp
                                    @foreach ($newslist as  $key=>$new)
                                        <tr>
                                            <td>{{ $new->news_id }}</td>
                                            <td><img src="{{asset($new->path)}}" alt="" width=" 100px" height="100px"></td>
                                            <td>
                                                <strong>{{ $new->news_name }}</strong>
                                            </td>
                                            <td>
                                                @if($new->is_feature==1)
                                                    <span class="badge badge-success">{{ trans('labels.Yes') }}</span>
                                                @elseif($new->is_feature==0)
                                                    <span class="badge badge-danger">{{ trans('labels.No') }}</span>
                                                @endif

                                            </td>
                                            <td>
                                                <strong>{{ trans('labels.AddedDate') }}: </strong> {{ $new->created_at}}<br>
                                                <strong>{{ trans('labels.ModifiedDate') }}: </strong>{{ $new->updated_at }}
                                            </td>

                                            <td>
                                                <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{ URL::to('admin/news/edit/'.$new->news_id)}}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                                <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteNewsId" news_id="{{ $new->news_id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">{{ trans('labels.NoRecordFound') }}.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                        @if($news != null)
                        <div class="col-md-12 text-right">
                            {{$news->links()}}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- deleteNewsModal -->
<div class="modal fade" id="deleteNewsModal" tabindex="-1" role="dialog" aria-labelledby="deleteNewsModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteNewsModalLabel">{{ trans('labels.DeleteNews') }}</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(array('url' =>'admin/news/delete', 'name'=>'deleteNews', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeleteNewsText') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                <button type="submit" class="btn btn-primary btn-flat" id="deleteNews">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
