<aside class="main-sidebar sidebar-light-danger elevation-4">

	<a href="{{ URL::to('admin/dashboard/this_month') }}" class="brand-link bg-danger">
		{{-- <img src="dist/img/casaideal.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
        <span class="brand-text font-weight-light"><b>C A S A  I D E A L</b></span>
	</a>

    <!-- sidebar: style can be found in sidebar.less -->
    <div class="sidebar">
		<nav class="mt-2">
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"data-accordion="false">

				<li class="nav-item {{ Request::is('admin/dashboard') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/dashboard/this_month') ? 'active' : '' }}" href="{{ URL::to('admin/dashboard/this_month') }}"><i class="fa fa-dashboard"></i> <p>{{ trans('labels.header_dashboard') }}</p></a>
				</li>

				<?php if( $result['commonContent']['roles'] != null and $result['commonContent']['roles']->view_media == 1){ ?>
				<li class="nav-item {{ Request::is('admin/media/add') ? 'menu-open' : '' }} {{ Request::is('admin/media/display') ? 'menu-open' : '' }} {{ Request::is('admin/addimages') ? 'menu-open' : '' }} {{ Request::is('admin/uploadimage/*') ? 'menu-open' : '' }} ">
					<a class="nav-link {{ Request::is('admin/media/add') ? 'active' : '' }} {{ Request::is('admin/media/display') ? 'active' : '' }} {{ Request::is('admin/addimages') ? 'active' : '' }} {{ Request::is('admin/uploadimage/*') ? 'active' : '' }}" href="#"><i class="fa fa-picture-o"></i> <p>{{ trans('labels.media') }}<i class="right fa fa-angle-left"></i></p></a>
					
					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/media/add') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/media/add') ? 'active' : '' }}" href="{{ url('admin/media/add') }}"><i class="fa fa-circle-o" aria-hidden="true"></i> <p>{{ trans('labels.media') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/media/display') ? 'menu-open' : '' }} {{ Request::is('admin/addimages') ? 'menu-open' : '' }} {{ Request::is('admin/uploadimage/*') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/media/display') ? 'active' : '' }} {{ Request::is('admin/addimages') ? 'active' : '' }} {{ Request::is('admin/uploadimage/*') ? 'active' : '' }}" href="{{ url('admin/media/display') }}"><i class="fa fa-circle-o" aria-hidden="true"></i> <p>{{ trans('labels.Media Setings') }}</p></a>
						</li>
					</ul>
				</li>
				<?php } ?>

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->coupons_view ==1){ ?>
					<li class="nav-item {{ Request::is('admin/coupons/display') ? 'menu-open' : '' }} {{ Request::is('admin/editcoupons/*') ? 'menu-open' : '' }}">
						<a class="nav-link {{ Request::is('admin/coupons/display') ? 'active' : '' }} {{ Request::is('admin/editcoupons/*') ? 'active' : '' }}" href="{{ URL::to('admin/coupons/display') }}"><i class="fa fa-tablet" aria-hidden="true"></i> <p>{{ trans('labels.link_coupons') }}</p></a>
					</li>
				<?php } ?>

				{{-- @if( $result['commonContent']['roles'] != null and $result['commonContent']['roles']->language_view == 1)
				<li class="nav-item {{ Request::is('admin/languages/display') ? 'menu-open' : '' }} {{ Request::is('admin/languages/add') ? 'menu-open' : '' }} {{ Request::is('admin/languages/edit/*') ? 'menu-open' : '' }} ">
					<a class="nav-link {{ Request::is('admin/languages/display') ? 'active' : '' }} {{ Request::is('admin/languages/add') ? 'active' : '' }} {{ Request::is('admin/languages/edit/*') ? 'active' : '' }} " href="{{ URL::to('admin/languages/display') }}"><i class="fa fa-language" aria-hidden="true"></i> <p>{{ trans('labels.languages') }}</p></a>
				</li>
				@endif --}}

				{{-- @if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->general_setting_view == 1)
				<li class="nav-item {{ Request::is('admin/currencies/display') ? 'menu-open' : '' }} {{ Request::is('admin/currencies/add') ? 'menu-open' : '' }} {{ Request::is('admin/currencies/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/currencies/filter') ? 'menu-open' : '' }}">
					<a class="nav-link  {{ Request::is('admin/currencies/display') ? 'active' : '' }} {{ Request::is('admin/currencies/add') ? 'active' : '' }} {{ Request::is('admin/currencies/edit/*') ? 'active' : '' }} {{ Request::is('admin/currencies/filter') ? 'active' : '' }}" href="{{ URL::to('admin/currencies/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.currency') }}</p></a>
				</li>
				@endif --}}

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->customers_view == 1){ ?>
				<li class="nav-item {{ Request::is('admin/customers/display') ? 'menu-open' : '' }}  {{ Request::is('admin/customers/add') ? 'menu-open' : '' }}  {{ Request::is('admin/customers/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/customers/address/display/*') ? 'menu-open' : '' }} {{ Request::is('admin/customers/filter') ? 'menu-open' : '' }} ">
					<a class="nav-link  {{ Request::is('admin/customers/display') ? 'active' : '' }}  {{ Request::is('admin/customers/add') ? 'active' : '' }}  {{ Request::is('admin/customers/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/customers/address/display/*') ? 'active' : '' }} {{ Request::is('admin/customers/filter') ? 'active' : '' }} " href="{{ URL::to('admin/customers/display') }}"><i class="fa fa-users" aria-hidden="true"></i> <p>{{ trans('labels.link_customers') }}</p></a>
				</li>
				<?php } ?>   

				<?php  if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->products_view == 1 or $result['commonContent']['roles']!= null and $result['commonContent']['roles']->categories_view == 1 ){ ?>
				<li class="nav-item {{ Request::is('admin/reviews/display') ? 'menu-open' : '' }} {{ Request::is('admin/manufacturers/display') ? 'menu-open' : '' }} {{ Request::is('admin/manufacturers/add') ? 'menu-open' : '' }} {{ Request::is('admin/manufacturers/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/units') ? 'menu-open' : '' }} {{ Request::is('admin/addunit') ? 'menu-open' : '' }} {{ Request::is('admin/editunit/*') ? 'menu-open' : '' }} {{ Request::is('admin/products/display') ? 'menu-open' : '' }} {{ Request::is('admin/products/add') ? 'menu-open' : '' }} {{ Request::is('admin/products/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/editattributes/*') ? 'menu-open' : '' }} {{ Request::is('admin/products/attributes/display') ? 'menu-open' : '' }}  {{ Request::is('admin/products/attributes/add') ? 'menu-open' : '' }} {{ Request::is('admin/products/attributes/add/*') ? 'menu-open' : '' }} {{ Request::is('admin/addinventory/*') ? 'menu-open' : '' }} {{ Request::is('admin/addproductimages/*') ? 'menu-open' : '' }} {{ Request::is('admin/categories/display') ? 'menu-open' : '' }} {{ Request::is('admin/categories/add') ? 'menu-open' : '' }} {{ Request::is('admin/categories/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/categories/filter') ? 'menu-open' : '' }} {{ Request::is('admin/products/inventory/display') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/reviews/display') ? 'active' : '' }} {{ Request::is('admin/manufacturers/display') ? 'active' : '' }} {{ Request::is('admin/manufacturers/add') ? 'active' : '' }} {{ Request::is('admin/manufacturers/edit/*') ? 'active' : '' }} {{ Request::is('admin/units') ? 'active' : '' }} {{ Request::is('admin/addunit') ? 'active' : '' }} {{ Request::is('admin/editunit/*') ? 'active' : '' }} {{ Request::is('admin/products/display') ? 'active' : '' }} {{ Request::is('admin/products/add') ? 'active' : '' }} {{ Request::is('admin/products/edit/*') ? 'active' : '' }} {{ Request::is('admin/editattributes/*') ? 'active' : '' }} {{ Request::is('admin/products/attributes/display') ? 'active' : '' }}  {{ Request::is('admin/products/attributes/add') ? 'active' : '' }} {{ Request::is('admin/products/attributes/add/*') ? 'active' : '' }} {{ Request::is('admin/addinventory/*') ? 'active' : '' }} {{ Request::is('admin/addproductimages/*') ? 'active' : '' }} {{ Request::is('admin/categories/display') ? 'active' : '' }} {{ Request::is('admin/categories/add') ? 'menu-open' : '' }} {{ Request::is('admin/categories/edit/*') ? 'active' : '' }} {{ Request::is('admin/categories/filter') ? 'active' : '' }} {{ Request::is('admin/products/inventory/display') ? 'active' : '' }}" href="#"><i class="fa fa-database"></i> <p>{{ trans('labels.Catalog') }}<i class="right fa fa-angle-left"></i></p></a>
					
					<ul class="nav nav-treeview">
						@if ($result['commonContent']['roles'] != null and $result['commonContent']['roles']->manufacturer_view == 1)
							<li class="nav-item {{ Request::is('admin/manufacturers/display') ? 'menu-open' : '' }} {{ Request::is('admin/manufacturers/add') ? 'menu-open' : '' }} {{ Request::is('admin/manufacturers/edit/*') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/manufacturers/display') ? 'active' : '' }} {{ Request::is('admin/manufacturers/add') ? 'active' : '' }} {{ Request::is('admin/manufacturers/edit/*') ? 'active' : '' }}" href="{{ URL::to('admin/manufacturers/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_manufacturer') }}</p></a>
							</li>
						@endif

						@if ($result['commonContent']['roles'] != null and $result['commonContent']['roles']->categories_view == 1)
							<li class="nav-item {{ Request::is('admin/categories/display') ? 'menu-open' : '' }} {{ Request::is('admin/categories/add') ? 'menu-open' : '' }} {{ Request::is('admin/categories/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/categories/filter') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/categories/display') ? 'active' : '' }} {{ Request::is('admin/categories/add') ? 'active' : '' }} {{ Request::is('admin/categories/edit/*') ? 'active' : '' }} {{ Request::is('admin/categories/filter') ? 'active' : '' }}" href="{{ URL::to('admin/categories/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_main_categories') }}</p></a>
							</li>
						@endif

						@if ($result['commonContent']['roles'] != null and $result['commonContent']['roles']->products_view == 1)
							<li class="nav-item {{ Request::is('admin/products/attributes/display') ? 'menu-open' : '' }}  {{ Request::is('admin/products/attributes/add') ? 'menu-open' : '' }}  {{ Request::is('admin/products/attributes/*') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/products/attributes/display') ? 'active' : '' }}  {{ Request::is('admin/products/attributes/add') ? 'active' : '' }}  {{ Request::is('admin/products/attributes/*') ? 'active' : '' }}" href="{{ URL::to('admin/products/attributes/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.products_attributes') }}</p></a>
							</li>

							<li class="nav-item {{ Request::is('admin/units') ? 'menu-open' : '' }} {{ Request::is('admin/addunit') ? 'menu-open' : '' }} {{ Request::is('admin/editunit/*') ? 'menu-open' : '' }} ">
								<a class="nav-link {{ Request::is('admin/units') ? 'active' : '' }} {{ Request::is('admin/addunit') ? 'active' : '' }} {{ Request::is('admin/editunit/*') ? 'active' : '' }} " href="{{ URL::to('admin/units') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_units') }}</p></a>
							</li>

							<li class="nav-item {{ Request::is('admin/products/display') ? 'menu-open' : '' }} {{ Request::is('admin/products/add') ? 'menu-open' : '' }} {{ Request::is('admin/products/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/products/attributes/add/*') ? 'menu-open' : '' }} {{ Request::is('admin/addinventory/*') ? 'menu-open' : '' }} {{ Request::is('admin/addproductimages/*') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/products/display') ? 'active' : '' }} {{ Request::is('admin/products/add') ? 'active' : '' }} {{ Request::is('admin/products/edit/*') ? 'active' : '' }} {{ Request::is('admin/products/attributes/add/*') ? 'active' : '' }} {{ Request::is('admin/addinventory/*') ? 'active' : '' }} {{ Request::is('admin/addproductimages/*') ? 'active' : '' }}" href="{{ URL::to('admin/products/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_all_products') }}</p></a>
							</li>

							@if ($result['commonContent']['setting']['Inventory'] == '1')
								<li class="nav-item {{ Request::is('admin/products/inventory/display') ? 'menu-open' : '' }}">
									<a class="nav-link  {{ Request::is('admin/products/inventory/display') ? 'active' : '' }}" href="{{ URL::to('admin/products/inventory/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.inventory') }}</p></a>
								</li>
							@endif
						@endif
						<?php $status_check = DB::table('reviews')->where('reviews_read',0)->first(); ?>

						<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->reviews_view == 1){ ?>
						<li class="nav-item {{ Request::is('admin/reviews/display') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/reviews/display') ? 'active' : '' }}" href="{{ URL::to('admin/reviews/display') }}">
								<i class="fa fa-circle-o" aria-hidden="true"></i> <p>{{ trans('labels.reviews') }}</p>
								@if ($result['commonContent']['new_reviews'] > 0)
									<p class="badge badge-success right">{{ $result['commonContent']['new_reviews'] }} {{ trans('labels.new') }}</p>
								@endif
							</a>
						</li>
						<?php } ?>
					</ul>
				</li>
				<?php } ?>

				

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->reports_view == 1){ ?>
				<li class="nav-item {{ Request::is('admin/maxstock') ? 'menu-open' : '' }} {{ Request::is('admin/minstock') ? 'menu-open' : '' }} {{ Request::is('admin/inventoryreport') ? 'menu-open' : '' }} {{ Request::is('admin/salesreport') ? 'menu-open' : '' }} {{ Request::is('admin/couponreport') ? 'menu-open' : '' }} {{ Request::is('admin/customers-orders-report') ? 'menu-open' : '' }} {{ Request::is('admin/outofstock') ? 'menu-open' : '' }} {{ Request::is('admin/statsproductspurchased') ? 'menu-open' : '' }} {{ Request::is('admin/statsproductsliked') ? 'menu-open' : '' }} {{ Request::is('admin/lowinstock') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/maxstock') ? 'active' : '' }} {{ Request::is('admin/minstock') ? 'active' : '' }} {{ Request::is('admin/inventoryreport') ? 'active' : '' }} {{ Request::is('admin/salesreport') ? 'active' : '' }} {{ Request::is('admin/couponreport') ? 'active' : '' }} {{ Request::is('admin/customers-orders-report') ? 'active' : '' }} {{ Request::is('admin/outofstock') ? 'active' : '' }} {{ Request::is('admin/statsproductspurchased') ? 'active' : '' }} {{ Request::is('admin/statsproductsliked') ? 'active' : '' }} {{ Request::is('admin/lowinstock') ? 'active' : '' }}" href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> <p>{{ trans('labels.link_reports') }}<i class="fa fa-angle-left right"></i></p></a>
					
					<ul class="nav nav-treeview">
						<!-- <li class="{{ Request::is('admin/lowinstock') ? 'menu-open' : '' }} "><a href="{{ URL::to('admin/lowinstock') }}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_products_low_stock') }}</a></li> -->
						@if ($result['commonContent']['setting']['Inventory'] == '1')
						
							<li class="nav-item {{ Request::is('admin/inventoryreport') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/inventoryreport') ? 'active' : '' }} " href="{{ URL::to('admin/inventoryreport') }}"><i class="fa fa-circle-o"></i> <p>Stock</p></a>
							</li>

							<li class="nav-item {{ Request::is('admin/outofstock') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/outofstock') ? 'active' : '' }}" href="{{ URL::to('admin/outofstock') }}"><i class="fa fa-circle-o"></i> <p>Fora de Stock</p></a>
							</li>


							<li class="nav-item {{ Request::is('admin/minstock') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/minstock') ? 'active' : '' }}" href="{{ URL::to('admin/minstock') }}"><i class="fa fa-circle-o"></i> <p>Stock Minimo</p></a>
							</li>

							<li class="nav-item {{ Request::is('admin/maxstock') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/minstock') ? 'active' : '' }}" href="{{ URL::to('admin/maxstock') }}"><i class="fa fa-circle-o"></i> <p>Stock Maximo</p></a>
							</li>
						@endif

						<li class="nav-item {{ Request::is('admin/customers-orders-report') ? 'menu-open' : '' }}">
							<a class=" nav-link {{ Request::is('admin/customers-orders-report') ? 'active' : '' }}" href="{{ URL::to('admin/customers-orders-report') }}"><i class="fa fa-circle-o"></i> <p>Pedidos de Clientes</p></a>
						</li>

						<!-- <li class="{{ Request::is('admin/statsproductsliked') ? 'menu-open' : '' }}"><a href="{{ URL::to('admin/statsproductsliked') }}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_products_liked') }}</a></li> -->

						<li class="nav-item {{ Request::is('admin/couponreport') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/couponreport') ? 'active' : '' }}" href="{{ URL::to('admin/couponreport') }}"><i class="fa fa-circle-o"></i> <p>Cupões</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/salesreport') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/salesreport') ? 'active' : '' }}" href="{{ URL::to('admin/salesreport') }}"><i class="fa fa-circle-o"></i> <p>Vendas</p></a>
						</li>
					</ul>
				</li>
				<?php } ?>

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->orders_view == 1){ ?>
					<li class="nav-item {{ Request::is('admin/orderstatus') ? 'menu-open' : '' }} {{ Request::is('admin/addorderstatus') ? 'menu-open' : '' }} {{ Request::is('admin/editorderstatus/*') ? 'menu-open' : '' }} {{ Request::is('admin/orders/display') ? 'menu-open' : '' }} {{ Request::is('admin/orders/vieworder/*') ? 'menu-open' : '' }}">
						<a class="nav-link {{ Request::is('admin/orderstatus') ? 'active' : '' }} {{ Request::is('admin/addorderstatus') ? 'active' : '' }} {{ Request::is('admin/editorderstatus/*') ? 'active' : '' }} {{ Request::is('admin/orders/display') ? 'active' : '' }} {{ Request::is('admin/orders/vieworder/*') ? 'active' : '' }}" href="#"><i class="fa fa-list-ul" aria-hidden="true"></i> <p>{{ trans('labels.link_orders') }}<i class="fa fa-angle-left right"></i></p></a>
						
						<ul class="nav nav-treeview">
							<li class="nav-item {{ Request::is('admin/orderstatus') ? 'menu-open' : '' }} {{ Request::is('admin/addorderstatus') ? 'menu-open' : '' }} {{ Request::is('admin/editorderstatus/*') ? 'menu-open' : '' }} ">
								<a class="nav-link {{ Request::is('admin/orderstatus') ? 'active' : '' }} {{ Request::is('admin/addorderstatus') ? 'active' : '' }} {{ Request::is('admin/editorderstatus/*') ? 'active' : '' }} " href="{{ URL::to('admin/orderstatus') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_order_status') }}</p></a>
							</li>
	
							<li class="nav-item {{ Request::is('admin/orders/display') ? 'menu-open' : '' }} {{ Request::is('admin/orders/vieworder/*') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/orders/display') ? 'active' : '' }} {{ Request::is('admin/orders/vieworder/*') ? 'active' : '' }}" href="{{ URL::to('admin/orders/display') }}"><i class="fa fa-circle-o" aria-hidden="true"></i> <p>{{ trans('labels.link_orders') }}</p></a>
							</li>
						</ul>
					</li>
					<?php } ?>
				
				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->tax_location_view == 1){ ?>
				<li class="nav-item {{ Request::is('admin/countries/display') ? 'menu-open' : '' }} {{ Request::is('admin/countries/add') ? 'menu-open' : '' }} {{ Request::is('admin/countries/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/zones/display') ? 'menu-open' : '' }} {{ Request::is('admin/zones/add') ? 'menu-open' : '' }} {{ Request::is('admin/zones/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxclass/display') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxclass/add') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxclass/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxrates/display') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxrates/add') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxrates/edit/*') ? 'menu-open' : '' }}">
					<a class="nav-link  {{ Request::is('admin/countries/display') ? 'active' : '' }} {{ Request::is('admin/countries/add') ? 'active' : '' }} {{ Request::is('admin/countries/edit/*') ? 'active' : '' }} {{ Request::is('admin/zones/display') ? 'active' : '' }} {{ Request::is('admin/zones/add') ? 'active' : '' }} {{ Request::is('admin/zones/edit/*') ? 'active' : '' }} {{ Request::is('admin/tax/taxclass/display') ? 'active' : '' }} {{ Request::is('admin/tax/taxclass/add') ? 'active' : '' }} {{ Request::is('admin/tax/taxclass/edit/*') ? 'active' : '' }} {{ Request::is('admin/tax/taxrates/display') ? 'active' : '' }} {{ Request::is('admin/tax/taxrates/add') ? 'active' : '' }} {{ Request::is('admin/tax/taxrates/edit/*') ? 'active' : '' }}" href="#"><i class="fa fa-money" aria-hidden="true"></i> <p>Impostos/Taxas<i class="fa fa-angle-left right"></i></p></a>

					<ul class="nav nav-treeview">
						{{-- <li class="nav-item {{ Request::is('admin/countries/display') ? 'menu-open' : '' }} {{ Request::is('admin/countries/add') ? 'menu-open' : '' }} {{ Request::is('admin/countries/edit/*') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/countries/display') ? 'active' : '' }} {{ Request::is('admin/countries/add') ? 'active' : '' }} {{ Request::is('admin/countries/edit/*') ? 'active' : '' }} " href="{{ URL::to('admin/countries/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_countries') }}</p></a>
						</li> --}}

						<li class="nav-item {{ Request::is('admin/zones/display') ? 'menu-open' : '' }} {{ Request::is('admin/zones/add') ? 'menu-open' : '' }} {{ Request::is('admin/zones/edit/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/zones/display') ? 'active' : '' }} {{ Request::is('admin/zones/add') ? 'active' : '' }} {{ Request::is('admin/zones/edit/*') ? 'active' : '' }}" href="{{ URL::to('admin/zones/display') }}"><i class="fa fa-circle-o"></i> <p>Provincias</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/tax/taxclass/display') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxclass/add') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxclass/edit/*') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/tax/taxclass/display') ? 'active' : '' }} {{ Request::is('admin/tax/taxclass/add') ? 'active' : '' }} {{ Request::is('admin/tax/taxclass/edit/*') ? 'active' : '' }} " href="{{ URL::to('admin/tax/taxclass/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_tax_class') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/tax/taxrates/display') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxrates/add') ? 'menu-open' : '' }} {{ Request::is('admin/tax/taxrates/edit/*') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/tax/taxrates/display') ? 'active' : '' }} {{ Request::is('admin/tax/taxrates/add') ? 'active' : '' }} {{ Request::is('admin/tax/taxrates/edit/*') ? 'active' : '' }} " href="{{ URL::to('admin/tax/taxrates/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_tax_rates') }}</p></a>
						</li>
					</ul>
				</li>
				<?php } ?>

				

				<?php if($result['commonContent']['roles'] != null and $result['commonContent']['roles']->shipping_methods_view == 1){ ?>
				<li class="nav-item {{ Request::is('admin/shippingmethods/display') ? 'menu-open' : '' }} {{ Request::is('admin/shippingmethods/upsShipping/display') ? 'menu-open' : '' }} {{ Request::is('admin/shippingmethods/flateRate/display') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/shippingmethods/display') ? 'active' : '' }} {{ Request::is('admin/shippingmethods/upsShipping/display') ? 'active' : '' }} {{ Request::is('admin/shippingmethods/flateRate/display') ? 'active' : '' }}" href="{{ URL::to('admin/shippingmethods/display') }}"><i class="fa fa-truck" aria-hidden="true"></i> <p>{{ trans('labels.link_shipping_methods') }}</p></a>
				</li>
				<?php } ?>

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->payment_methods_view == 1){ ?>
				<li class="nav-item {{ Request::is('admin/paymentmethods/index') ? 'menu-open' : '' }} {{ Request::is('admin/paymentmethods/display/*') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/paymentmethods/index') ? 'active' : '' }} {{ Request::is('admin/paymentmethods/display/*') ? 'active' : '' }}" href="{{ URL::to('admin/paymentmethods/index') }}"><i class="fa fa-credit-card" aria-hidden="true"></i> <p>{{ trans('labels.link_payment_methods') }}</p></a>
				</li>
				<?php } ?>

				{{-- @if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->news_view == 1)
				<li class="nav-item {{ Request::is('admin/newscategories/display') ? 'menu-open' : '' }} {{ Request::is('admin/newscategories/add') ? 'menu-open' : '' }} {{ Request::is('admin/newscategories/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/news/display') ? 'menu-open' : '' }}  {{ Request::is('admin/news/add') ? 'menu-open' : '' }}  {{ Request::is('admin/news/edit/*') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/newscategories/display') ? 'active' : '' }} {{ Request::is('admin/newscategories/add') ? 'active' : '' }} {{ Request::is('admin/newscategories/edit/*') ? 'active' : '' }} {{ Request::is('admin/news/display') ? 'active' : '' }}  {{ Request::is('admin/news/add') ? 'active' : '' }}  {{ Request::is('admin/news/edit/*') ? 'active' : '' }}" href="#"><i class="fa fa-database" aria-hidden="true"></i> <p>{{ trans('labels.Blog') }}<i class="fa fa-angle-left right"></i></p></a>

					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/newscategories/display') ? 'menu-open' : '' }} {{ Request::is('admin/newscategories/add') ? 'menu-open' : '' }} {{ Request::is('admin/newscategories/edit/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/newscategories/display') ? 'active' : '' }} {{ Request::is('admin/newscategories/add') ? 'active' : '' }} {{ Request::is('admin/newscategories/edit/*') ? 'active' : '' }}" href="{{ URL::to('admin/newscategories/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_news_categories') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/news/display') ? 'menu-open' : '' }}  {{ Request::is('admin/news/add') ? 'menu-open' : '' }}  {{ Request::is('admin/news/edit/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/news/display') ? 'active' : '' }}  {{ Request::is('admin/news/add') ? 'active' : '' }}  {{ Request::is('admin/news/edit/*') ? 'active' : '' }}" href="{{ URL::to('admin/news/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_sub_news') }}</p></a>
						</li>
					</ul>
				</li>
				@endif --}}

				@if ($result['commonContent']['roles'] != null and $result['commonContent']['roles']->notifications_view == 1)
					<li class="nav-item {{ Request::is('admin/pushnotification') ? 'menu-open' : '' }} {{ Request::is('admin/devices/display') ? 'menu-open' : '' }} {{ Request::is('admin/devices/viewdevices/*') ? 'menu-open' : '' }} {{ Request::is('admin/devices/notifications') ? 'menu-open' : '' }}">
						<a class="nav-link {{ Request::is('admin/pushnotification') ? 'active' : '' }} {{ Request::is('admin/devices/display') ? 'active' : '' }} {{ Request::is('admin/devices/viewdevices/*') ? 'active' : '' }} {{ Request::is('admin/devices/notifications') ? 'active' : '' }}" href="{{ URL::to('admin/devices/display') }} "><i class="fa fa-bell-o" aria-hidden="true"></i> <p>{{ trans('labels.link_notifications') }}<i class="fa fa-angle-left right"></i></p> </a>

						<ul class="nav nav-treeview">
							<li class="nav-item {{ Request::is('admin/pushnotification') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/pushnotification') ? 'active' : '' }}" href="{{ URL::to('admin/pushnotification') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_setting') }}</p></a>
							</li>

							<li class="nav-item {{ Request::is('admin/devices/display') ? 'menu-open' : '' }} {{ Request::is('admin/devices/viewdevices/*') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/devices/display') ? 'active' : '' }} {{ Request::is('admin/devices/viewdevices/*') ? 'active' : '' }}" href="{{ URL::to('admin/devices/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_devices') }}</p></a>
							</li>

							<li class="nav-item {{ Request::is('admin/devices/notifications') ? 'menu-open' : '' }} ">
								<a class="nav-link {{ Request::is('admin/devices/notifications') ? 'active' : '' }}" href="{{ URL::to('admin/devices/notifications') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_send_notifications') }}</p></a>
							</li>
						</ul>
					</li>
				@endif

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->general_setting_view == 1){ ?>
				<li class="nav-item {{ Request::is('admin/loginsetting') ? 'menu-open' : '' }} {{ Request::is('admin/firebase') ? 'menu-open' : '' }} {{ Request::is('admin/facebooksettings') ? 'menu-open' : '' }} {{ Request::is('admin/setting') ? 'menu-open' : '' }} {{ Request::is('admin/googlesettings') ? 'menu-open' : '' }}  {{ Request::is('admin/alertsetting') ? 'menu-open' : '' }}  ">
					<a class="nav-link  {{ Request::is('admin/loginsetting') ? 'active' : '' }} {{ Request::is('admin/firebase') ? 'active' : '' }} {{ Request::is('admin/facebooksettings') ? 'active' : '' }} {{ Request::is('admin/setting') ? 'active' : '' }} {{ Request::is('admin/googlesettings') ? 'active' : '' }}  {{ Request::is('admin/alertsetting') ? 'active' : '' }}  " href="#"><i class="fa fa-gears" aria-hidden="true"></i> <p>{{ trans('labels.link_general_settings') }}<i class="fa fa-angle-left right"></i></p></a>
					
					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/setting') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/setting') ? 'active' : '' }}" href="{{ URL::to('admin/setting') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_store_setting') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/facebooksettings') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/setting') ? 'active' : '' }}" href="{{ URL::to('admin/facebooksettings') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_facebook') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/googlesettings') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/setting') ? 'active' : '' }}" href="{{ URL::to('admin/googlesettings') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_google') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/alertsetting') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/setting') ? 'active' : '' }}" href="{{ URL::to('admin/alertsetting') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.alertSetting') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/firebase') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/setting') ? 'active' : '' }}" href="{{ URL::to('admin/firebase') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.Firebase') }}</p></a>
						</li>
						<!-- <li class="{{ Request::is('admin/loginsetting') ? 'menu-open' : '' }}"><a href="{{ URL::to('admin/loginsetting') }}"><i class="fa fa-circle-o"></i> {{ trans('labels.Login Setting') }}</a></li> -->
					</ul>
				</li>
				<?php } ?>
				<?php $route =  DB::table('settings')->where('name','is_web_purchased')->where('value', 1)->first();?>

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->website_setting_view == 1 and $route != null){ ?>
				<li class="nav-item {{ Request::is('admin/webPagesSettings/12') ? 'menu-open' : '' }} {{ Request::is('admin/instafeed') ? 'menu-open' : '' }} {{ Request::is('admin/menus') ? 'menu-open' : '' }} {{ Request::is('admin/mailchimp') ? 'menu-open' : '' }} {{ Request::is('admin/topoffer/display') ? 'menu-open' : '' }} {{ Request::is('admin/webPagesSettings/*') ? 'menu-open' : '' }} {{ Request::is('admin/homebanners') ? 'menu-open' : '' }} {{ Request::is('admin/sliders') ? 'menu-open' : '' }} {{ Request::is('admin/addsliderimage') ? 'menu-open' : '' }} {{ Request::is('admin/editslide/*') ? 'menu-open' : '' }} {{ Request::is('admin/webpages') ? 'menu-open' : '' }}  {{ Request::is('admin/addwebpage') ? 'menu-open' : '' }}  {{ Request::is('admin/editwebpage/*') ? 'menu-open' : '' }} {{ Request::is('admin/websettings') ? 'menu-open' : '' }} {{ Request::is('admin/webthemes') ? 'menu-open' : '' }} {{ Request::is('admin/customstyle') ? 'menu-open' : '' }} {{ Request::is('admin/constantbanners') ? 'menu-open' : '' }} {{ Request::is('admin/addconstantbanner') ? 'menu-open' : '' }} {{ Request::is('admin/editconstantbanner/*') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/webPagesSettings/12') ? 'active' : '' }} {{ Request::is('admin/instafeed') ? 'active' : '' }} {{ Request::is('admin/menus') ? 'active' : '' }} {{ Request::is('admin/mailchimp') ? 'active' : '' }} {{ Request::is('admin/topoffer/display') ? 'active' : '' }} {{ Request::is('admin/webPagesSettings/*') ? 'active' : '' }} {{ Request::is('admin/homebanners') ? 'active' : '' }} {{ Request::is('admin/sliders') ? 'active' : '' }} {{ Request::is('admin/addsliderimage') ? 'active' : '' }} {{ Request::is('admin/editslide/*') ? 'active' : '' }} {{ Request::is('admin/webpages') ? 'active' : '' }}  {{ Request::is('admin/addwebpage') ? 'active' : '' }}  {{ Request::is('admin/editwebpage/*') ? 'active' : '' }} {{ Request::is('admin/websettings') ? 'active' : '' }} {{ Request::is('admin/webthemes') ? 'active' : '' }} {{ Request::is('admin/customstyle') ? 'active' : '' }} {{ Request::is('admin/constantbanners') ? 'active' : '' }} {{ Request::is('admin/addconstantbanner') ? 'active' : '' }} {{ Request::is('admin/editconstantbanner/*') ? 'active' : '' }}" href="#"><i class="fa fa-gears" aria-hidden="true"></i> <p>{{ trans('labels.link_site_settings') }}<i class="fa fa-angle-left right"></i></p></a>
					
					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/topoffer/display') ? 'menu-open' : '' }} {{ Request::is('admin/webPagesSettings/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/topoffer/display') ? 'active' : '' }} {{ Request::is('admin/webPagesSettings/*') ? 'active' : '' }}" href="#"> <i class="fa fa-picture-o"></i> <p>{{ trans('labels.Theme Setting') }}<i class="fa fa-angle-left right"></i></p></a>

							<ul class="nav nav-treeview">
								<li class="nav-item {{ Request::is('admin/webPagesSettings/1') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/1') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/1"><i class="fa fa-picture-o" aria-hidden="true"></i> <p> {{ trans('labels.Home Page Builder') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/7') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/7') ? 'active' : '' }}" href="{{ url('admin/webPagesSettings') }}/7"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Colors Settings') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/10') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/10') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/10"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Banner Transition Settings') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/11') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/11') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/11"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Product Card Style') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/12') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/12') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/12"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Categorywidget') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/13') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/13') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/13"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Categories Section') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/topoffer/display') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/topoffer/display') ? 'active' : '' }} " href="{{ url('admin/topoffer/display') }}"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Top Offer') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/8') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/8') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/8"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.login') }}</p> </a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/9') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/9') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/9"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.News') }}</p></a> 
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/5') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/5') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/5"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Shop Page Settings') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/2') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/2') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/2"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Cart Page Settings') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/6') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/6') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/6"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Contact Page Settings') }}</p></a>
								</li>

								<li class="nav-item {{ Request::is('admin/webPagesSettings/4') ? 'menu-open' : '' }} ">
									<a class="nav-link {{ Request::is('admin/webPagesSettings/4') ? 'active' : '' }} " href="{{ url('admin/webPagesSettings') }}/4"><i class="fa fa-picture-o" aria-hidden="true"></i> <p>{{ trans('labels.Product Page Settings') }}</p></a>
								</li>
							</ul>
						</li>

						<li class="nav-item {{ Request::is('admin/sliders') ? 'menu-open' : '' }} {{ Request::is('admin/addsliderimage') ? 'menu-open' : '' }} {{ Request::is('admin/editslide/*') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/sliders') ? 'active' : '' }} {{ Request::is('admin/addsliderimage') ? 'active' : '' }} {{ Request::is('admin/editslide/*') ? 'active' : '' }} " href="{{ URL::to('admin/sliders') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_Sliders') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/homebanners') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/homebanners') ? 'active' : '' }} " href="{{ URL::to('admin/homebanners') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.Parallax Banners') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/constantbanners') ? 'menu-open' : '' }} {{ Request::is('admin/constantbanners') ? 'menu-open' : '' }} {{ Request::is('admin/constantbanners/*') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/constantbanners') ? 'active' : '' }} {{ Request::is('admin/constantbanners') ? 'active' : '' }} {{ Request::is('admin/constantbanners/*') ? 'active' : '' }} " href="{{ URL::to('admin/constantbanners') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_Banners') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/menus') ? 'menu-open' : '' }}  {{ Request::is('admin/addmenus') ? 'menu-open' : '' }}  {{ Request::is('admin/editmenus/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/menus') ? 'active' : '' }}  {{ Request::is('admin/addmenus') ? 'active' : '' }}  {{ Request::is('admin/editmenus/*') ? 'active' : '' }}" href="{{ URL::to('admin/menus') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.menus') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/webpages') ? 'menu-open' : '' }}  {{ Request::is('admin/addwebpage') ? 'menu-open' : '' }}  {{ Request::is('admin/editwebpage/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/webpages') ? 'active' : '' }}  {{ Request::is('admin/addwebpage') ? 'active' : '' }}  {{ Request::is('admin/editwebpage/*') ? 'active' : '' }}" href="{{ URL::to('admin/webpages') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.content_pages') }}</p></a>
						</li>

						<!-- <li class="{{ Request::is('admin/webthemes') ? 'menu-open' : '' }} "><a href="{{ URL::to('admin/webthemes') }}"><i class="fa fa-circle-o"></i> {{ trans('labels.website_themes') }}</a></li> -->

						<li class="nav-item {{ Request::is('admin/seo') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/seo') ? 'active' : '' }}" href="{{ URL::to('admin/seo') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.seo content') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/customstyle') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/customstyle') ? 'active' : '' }} " href="{{ URL::to('admin/customstyle') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.custom_style_js') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/newsletter') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/newsletter') ? 'active' : '' }}" href="{{ URL::to('admin/newsletter') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.mailchimp') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/instafeed') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/instafeed') ? 'active' : '' }}" href="{{ URL::to('admin/instafeed') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.instagramfeed') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/websettings') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/websettings') ? 'active' : '' }}" href="{{ URL::to('admin/websettings') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_setting') }}</p></a>
						</li>
					</ul>
				</li>
				<?php } ?>

				<?php $route =  DB::table('settings')->where('name','is_app_purchased')->where('value', 1)->first(); ?>

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->application_setting_view == 1 and $route != null){ ?>
				<li class="nav-item {{ Request::is('admin/banners') ? 'menu-open' : '' }} {{ Request::is('admin/addbanner') ? 'menu-open' : '' }} {{ Request::is('admin/editbanner/*') ? 'menu-open' : '' }} {{ Request::is('admin/pages') ? 'menu-open' : '' }}  {{ Request::is('admin/addpage') ? 'menu-open' : '' }}  {{ Request::is('admin/editpage/*') ? 'menu-open' : '' }}  {{ Request::is('admin/appSettings') ? 'menu-open' : '' }} {{ Request::is('admin/admobSettings') ? 'menu-open' : '' }} {{ Request::is('admin/applabel') ? 'menu-open' : '' }} {{ Request::is('admin/addappkey') ? 'menu-open' : '' }} {{ Request::is('admin/applicationapi') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/banners') ? 'active' : '' }} {{ Request::is('admin/addbanner') ? 'active' : '' }} {{ Request::is('admin/editbanner/*') ? 'active' : '' }} {{ Request::is('admin/pages') ? 'active' : '' }}  {{ Request::is('admin/addpage') ? 'active' : '' }}  {{ Request::is('admin/editpage/*') ? 'active' : '' }}  {{ Request::is('admin/appSettings') ? 'active' : '' }} {{ Request::is('admin/admobSettings') ? 'active' : '' }} {{ Request::is('admin/applabel') ? 'active' : '' }} {{ Request::is('admin/addappkey') ? 'active' : '' }} {{ Request::is('admin/applicationapi') ? 'active' : '' }}" href="#"><i class="fa fa-gears" aria-hidden="true"></i> <p>Configurações da App<i class="fa fa-angle-left right"></i></p></a>
					
					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/banners') ? 'menu-open' : '' }} {{ Request::is('admin/addbanner') ? 'menu-open' : '' }} {{ Request::is('admin/editbanner/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/banners') ? 'active' : '' }} {{ Request::is('admin/addbanner') ? 'active' : '' }} {{ Request::is('admin/editbanner/*') ? 'active' : '' }}" href="{{ URL::to('admin/banners') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_Banners') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/pages') ? 'menu-open' : '' }}  {{ Request::is('admin/addpage') ? 'menu-open' : '' }}  {{ Request::is('admin/editpage/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/pages') ? 'active' : '' }}  {{ Request::is('admin/addpage') ? 'active' : '' }}  {{ Request::is('admin/editpage/*') ? 'active' : '' }}" href="{{ URL::to('admin/pages') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.content_pages') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/admobSettings') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/admobSettings') ? 'active' : '' }}" href="{{ URL::to('admin/admobSettings') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_admob') }}</p></a>
						</li>

						<li class="nav-item android-hide {{ Request::is('admin/applabel') ? 'menu-open' : '' }} {{ Request::is('admin/addappkey') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/applabel') ? 'active' : '' }} {{ Request::is('admin/addappkey') ? 'active' : '' }}" href="{{ URL::to('admin/applabel') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.labels') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/applicationapi') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/applicationapi') ? 'active' : '' }}" href="{{ URL::to('admin/applicationapi') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.applicationApi') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/appsettings') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/appsettings') ? 'active' : '' }}" href="{{ URL::to('admin/appsettings') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_setting') }}</p></a>
						</li>
					</ul>
				</li>
				<?php }  ?>

				<?php  $route =  DB::table('settings')->where('name','is_deliverboy_purchased')->where('value', 1)->first(); ?>

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->deliveryboy_view == 1 and $route != null){?>
				<li class="nav-item {{ Request::is('admin/deliveryboys/status/display') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/status/add') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/status/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/deliveryboys/withdraw/display') ? 'menu-open' : '' }} {{ Request::is('admin/deliveryboys/floatingcash/display') ? 'menu-open' : '' }} {{ Request::is('admin/deliveryboys/ratings') ? 'menu-open' : '' }} {{ Request::is('admin/deliveryboys/display') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/add') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/deliveryboys/filter') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/deliveryboys/status/display') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/status/add') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/status/edit/*') ? 'active' : '' }} {{ Request::is('admin/deliveryboys/withdraw/display') ? 'active' : '' }} {{ Request::is('admin/deliveryboys/floatingcash/display') ? 'active' : '' }} {{ Request::is('admin/deliveryboys/ratings') ? 'active' : '' }} {{ Request::is('admin/deliveryboys/display') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/add') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/edit/*') ? 'active' : '' }} {{ Request::is('admin/deliveryboys/filter') ? 'active' : '' }}" href="#"><i class="fa fa-database" aria-hidden="true"></i> <p>{{ trans('labels.link_manage_deliveryboy') }}<i class="fa fa-angle-left right"></i></p></a>
					
					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/deliveryboys/display') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/add') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/edit/*') ? 'menu-open' : '' }} {{ Request::is('admin/deliveryboys/filter') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/deliveryboys/display') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/add') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/edit/*') ? 'active' : '' }} {{ Request::is('admin/deliveryboys/filter') ? 'active' : '' }}" href="{{ URL::to('admin/deliveryboys/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_deliveryboy') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/deliveryboys/floatingcash/display') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/deliveryboys/floatingcash/display') ? 'active' : '' }}" href="{{ URL::to('admin/deliveryboys/floatingcash/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.Floating Cash') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/deliveryboys/withdraw/display') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/deliveryboys/withdraw/display') ? 'active' : '' }}" href="{{ URL::to('admin/deliveryboys/withdraw/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.Withdraw') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/deliveryboys/pages') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/addpage') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/editpage/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/deliveryboys/pages') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/addpage') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/editpage/*') ? 'active' : '' }}" href="{{ URL::to('admin/deliveryboys/pages') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.content_pages') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/deliveryboys/status/display') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/status/add') ? 'menu-open' : '' }}  {{ Request::is('admin/deliveryboys/status/edit/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/deliveryboys/status/display') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/status/add') ? 'active' : '' }}  {{ Request::is('admin/deliveryboys/status/edit/*') ? 'active' : '' }}" href="{{ URL::to('admin/deliveryboys/status/display') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.Status') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/deliveryboys/setting') ? 'menu-open' : '' }} ">
							<a class="nav-link {{ Request::is('admin/deliveryboys/setting') ? 'active' : '' }} " href="{{ URL::to('admin/deliveryboys/setting') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.Setting') }}</p></a>
						</li>
					</ul>
				</li>


				<?php } ?>

				<?php if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->manage_admins_view == 1){ ?>
				<li class="nav-item {{ Request::is('admin/admins') ? 'menu-open' : '' }} {{ Request::is('admin/addadmins') ? 'menu-open' : '' }} {{ Request::is('admin/editadmin/*') ? 'menu-open' : '' }} {{ Request::is('admin/manageroles') ? 'menu-open' : '' }} {{ Request::is('admin/addadminType') ? 'menu-open' : '' }} {{ Request::is('admin/editadminType/*') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/admins') ? 'active' : '' }} {{ Request::is('admin/addadmins') ? 'active' : '' }} {{ Request::is('admin/editadmin/*') ? 'active' : '' }} {{ Request::is('admin/manageroles') ? 'active' : '' }} {{ Request::is('admin/addadminType') ? 'active' : '' }} {{ Request::is('admin/editadminType/*') ? 'active' : '' }}" href="#"><i class="fa fa-users" aria-hidden="true"></i> <p>{{ trans('labels.Manage Admins') }}<i class="fa fa-angle-left right"></i></p></a>

					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/admins') ? 'menu-open' : '' }} {{ Request::is('admin/addadmins') ? 'menu-open' : '' }} {{ Request::is('admin/editadmin/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/admins') ? 'active' : '' }} {{ Request::is('admin/addadmins') ? 'active' : '' }} {{ Request::is('admin/editadmin/*') ? 'active' : '' }}" href="{{ URL::to('admin/admins') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_admins') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/manageroles') ? 'menu-open' : '' }} {{ Request::is('admin/addadminType') ? 'menu-open' : '' }} {{ Request::is('admin/editadminType/*') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/manageroles') ? 'active' : '' }} {{ Request::is('admin/addadminType') ? 'active' : '' }} {{ Request::is('admin/editadminType/*') ? 'active' : '' }}" href="{{ URL::to('admin/manageroles') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_manage_roles') }}</p></a>
						</li>
					</ul>
				</li>
				<?php } ?>

				{{-- @if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->edit_management == 1)
				<!--------create middlewares -------->
				<li class="nav-item {{ Request::is('admin/managements/merge') ? 'menu-open' : '' }} {{ Request::is('admin/managements/updater') ? 'menu-open' : '' }}">
					<a class="nav-link  {{ Request::is('admin/managements/merge') ? 'active' : '' }} {{ Request::is('admin/managements/updater') ? 'active' : '' }}" href="#"><i class="fa fa-gears" aria-hidden="true"></i> <p>{{ trans('labels.Code Manager') }}<i class="fa fa-angle-left right"></i></p></a>

					<ul class="nav nav-treeview">
						@if ($result['commonContent']['setting']['is_deliverboy_purchased'] == '0')
							<li class="nav-item {{ Request::is('admin/managements/merge') ? 'menu-open' : '' }}">
								<a class="nav-link {{ Request::is('admin/managements/merge') ? 'active' : '' }}" href="{{ URL::to('admin/managements/merge') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_merge') }}</p></a>
							</li>
						@endif
						<li class="nav-item {{ Request::is('admin/managements/updater') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/managements/updater') ? 'active' : '' }}" href="{{ URL::to('admin/managements/updater') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.link_updater') }}</p></a>
						</li>
					</ul>
				</li>
				@endif

				@if($result['commonContent']['roles']!= null and $result['commonContent']['roles']->edit_management == 1)
				<!--------create middlewares -------->
				<li class="nav-item {{ Request::is('admin/managements/import') ? 'menu-open' : '' }} {{ Request::is('admin/managements/backup') ? 'menu-open' : '' }}">
					<a class="nav-link {{ Request::is('admin/managements/import') ? 'active' : '' }} {{ Request::is('admin/managements/backup') ? 'active' : '' }}" href="#"><i class="fa fa-gears" aria-hidden="true"></i> <p>{{ trans('labels.Backup / Restore') }}<i class="fa fa-angle-left right"></i></p></a>

					<ul class="nav nav-treeview">
						<li class="nav-item {{ Request::is('admin/managements/backup') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/managements/backup') ? 'active' : '' }}" href="{{ URL::to('admin/managements/backup') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.backup') }}</p></a>
						</li>

						<li class="nav-item {{ Request::is('admin/managements/import') ? 'menu-open' : '' }}">
							<a class="nav-link {{ Request::is('admin/managements/import') ? 'active' : '' }}" href="{{ URL::to('admin/managements/import') }}"><i class="fa fa-circle-o"></i> <p>{{ trans('labels.restore') }}</p></a>
						</li>
					</ul>
				</li>
				@endif --}}

				<!-- cache clear -->
				{{-- <li class="nav-item {{ Request::is('admin/dashboard') ? 'menu-open' : '' }}">
					<a href="javascript:void(0)" class="nav-link clear-cache  {{ Request::is('admin/dashboard') ? 'active' : '' }}"><i class="fa fa-eraser" aria-hidden="true"></i> <p>{{ trans('labels.Clear Cache') }}</p></a>
				</li> --}}
			</ul>
		</nav>
    </div>
    <!-- /.sidebar -->
</aside>
