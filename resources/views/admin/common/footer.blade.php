<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Versão</b> 1.1.22
    </div>
    <strong>Copyright © <?php echo date('Y'); ?>.</strong> Todos Direitos Reservados.
</footer>
