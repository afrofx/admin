<nav class="main-header navbar navbar-expand navbar-dark bg-danger">

    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fa fa-bars"></i></a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-bs-toggle="dropdown" href="#">
                <i class="fa fa-list-ul"></i>
                <span class="badge badge-success navbar-badge">{{ count($unseenOrders) }}</span>
            </a>

            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">{{ trans('labels.you_have') }}{{ count($unseenOrders) }}{{ trans('labels.new_orders') }}</span>
                <div class="dropdown-divider"></div>
				@foreach ($unseenOrders as $unseenOrder)
				<a href="{{ URL::to('admin/orders/vieworder') }}/{{ $unseenOrder->orders_id }}" class="dropdown-item">
					{{ $unseenOrder->customers_name }} <small><i class="fa fa-clock-o"></i>
					{{ date('d/m/Y', strtotime($unseenOrder->date_purchased)) }}</small>
					<span class="float-right text-muted text-sm">Ordered Products ({{ $unseenOrder->total_products }})</span>
				</a>
				<div class="dropdown-divider"></div>
                @endforeach
        	</div>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link" data-bs-toggle="dropdown" href="#">
				<i class="fa fa-users"></i>
				<span class="badge badge-warning">{{ count($newCustomers) }}</span> 
			</a>

            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">{{ count($newCustomers) }} {{ trans('labels.new_users') }}</span>
				<div class="dropdown-divider"></div>
				@foreach ($newCustomers as $newCustomer)
				<a class="dropdown-item" href="{{ URL::to('admin/customers/edit') }}/{{ $newCustomer->id }}">
					{{-- {{ date('d/m/Y', $newCustomer->created_at) }} --}}
					{{ $newCustomer->first_name }} {{ $newCustomer->last_name }}
					<span class="float-right text-muted text-sm"><i class="fa fa-clock-o"></i></span>
				</a>
				@endforeach
			</div>
        </li>

        @if ($result['commonContent']['setting']['Inventory'] == '1')
        <li class="nav-item dropdown">
			<a class="nav-link" data-bs-toggle="dropdown" href="#">
				<i class="fa fa-th"></i>
				<span class="badge badge-warning">{{ count($lowInQunatity) }}</span>
			</a>
			<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
				<span class="dropdown-item dropdown-header">{{ count($lowInQunatity) }} {{ trans('labels.products_are_in_low_quantity') }}</span>
				<div class="dropdown-divider"></div>
				@foreach ($lowInQunatity as $lowInQunatity)
				<a class="dropdown-item" href="{{ URL::to('admin/inventoryreport?type=all&dateRange=&products_id=') }}{{ $lowInQunatity->products_id }}">
					<div class="media">
						<img src="{{ asset('') . $lowInQunatity->image }}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
						<div class="media-body">
							<h3 class="dropdown-item-title">
								{{ $lowInQunatity->products_name }}
							</h3>
						</div>
					</div>
				</a>
				@endforeach
			</div>
		</li>
        @endif
		
        {{-- <li class="nav-item dropdown">
            <a href="javascript:void(0)" class="clear-cache small-box-footer btn" data-bs-toggle="tooltip"
                data-placement="bottom" title="" data-original-title="{{ trans('labels.Clear Cache') }}">
                <i class="fa fa-eraser" aria-hidden="true"></i>
            </a>
        </li> --}}
        <!-- User Account: style can be found in dropdown.less -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-bs-toggle="dropdown" href="#">
                <span class="hidden-xs">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }} </span>
            </a>

            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <span class="dropdown-item dropdown-header">
                    <p>{{ auth()->user()->first_name }} {{ auth()->user()->last_name }} <small>{{ trans('labels.administrator') }}</small></p>
                </span>
                <!-- Menu Footer-->
                <div class="user-footer">
                    <div class="pull-left">
                        <a href="{{ URL::to('admin/admin/profile') }}"
                            class="btn btn-default btn-flat">{{ trans('labels.profile_link') }}</a>
                    </div>
                    <div class="pull-right">
                        <a href="{{ URL::to('admin/logout') }}"
                            class="btn btn-default btn-flat">{{ trans('labels.sign_out') }}</a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</nav>
