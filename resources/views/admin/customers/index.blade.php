@extends('admin.layout')
@section('title-dash'){{trans('labels.ListingAllCustomers')}}...@endsection
@section('title-link')
    <li class="breadcrumb-item active">{{trans('labels.Customers')}}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title form-inline" id="contact-form">
                        <form name='registration' id="registration" class="registration" method="get" action="{{url('admin/customers/filter')}}">
                            <input type="hidden" value="{{csrf_token()}}">
                            <div class="input-group-form search-panel ">
                                <select type="button" class="btn btn-default dropdown-toggle form-control" data-bs-toggle="dropdown" name="FilterBy" id="FilterBy">
                                    <option value="" selected disabled hidden>Filter By</option>
                                    <option value="Name" @if(isset($filter)) @if ($filter=="Name" ) {{ 'selected' }} @endif @endif>{{ trans('labels.Name') }}</option>
                                    <option value="E-mail" @if(isset($filter)) @if ($filter=="E-mail" ) {{ 'selected' }}@endif @endif>{{ trans('labels.Email') }}</option>
                                    <option value="Phone" @if(isset($filter)) @if ($filter=="Phone" ) {{ 'selected' }}@endif @endif>{{ trans('labels.Phone') }}</option>
                                    <option value="Address" @if(isset($filter)) @if ($filter=="Address" ) {{ 'selected' }}@endif @endif>{{ trans('labels.Address') }}</option>
                                    <!-- <option value="Suburb" @if(isset($filter)) @if ($filter=="Suburb" ) {{ 'selected' }}@endif @endif>{{ trans('labels.Suburb') }}</option> -->
                                    <option value="Postcode" @if(isset($filter)) @if ($filter=="Postcode" ) {{ 'selected' }}@endif @endif>{{ trans('labels.Postcode') }}</option>
                                    <option value="City" @if(isset($filter)) @if ($filter=="City" ) {{ 'selected' }}@endif @endif>{{ trans('labels.City') }}</option>
                                    <option value="State" @if(isset($filter)) @if ($filter=="State" ) {{ 'selected' }}@endif @endif>{{ trans('labels.State') }}</option>
                                    <option value="Country" @if(isset($filter)) @if ($filter=="Country" ) {{ 'selected' }}@endif @endif>{{ trans('labels.Country') }}</option>
                                </select>
                                <input type="text" class="form-control input-group-form " name="parameter" placeholder="Search term..." id="parameter" @if(isset($parameter)) value="{{$parameter}}" @endif>
                                <button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
                                @if(isset($parameter,$filter)) <a class="btn btn-danger  btn-flat" href="{{url('admin/customers/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                            </div>
                        </form>
                    </div>
                    <div class="form-inline" id="contact-form12"></div>
                    <div class="card-tools right">
                        <a href="{{ url('admin/customers/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>@sortablelink('id', trans('labels.ID') )</th>
                                        <th style="display: none">{{ trans('labels.Picture') }}</th>
                                        <th>@sortablelink('first_name', trans('labels.Full Name')) </th>
                                        <th>@sortablelink('email', trans('labels.Email')) </th>
                                        <th>{{ trans('labels.Additional info') }} </th>
                                        <th>@sortablelink('entry_street_address', trans('labels.Address'))</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($customers['result']))
                                    @foreach ($customers['result'] as $key=>$listingCustomers)
                                    <tr>
                                        <td>{{ $listingCustomers->id }}</td>
                                        <td>{{ $listingCustomers->first_name }} {{ $listingCustomers->last_name }}</td>
                                        <td style="text-transform: lowercase">{{ $listingCustomers->email }}</td>
                                        <td>                                               
                                            <strong>{{ trans('labels.Phone') }}: </strong> {{ $listingCustomers->phone }}                                               
                                        </td>
                                        <td>
                                            @if(!empty($listingCustomers->entry_street_address))
                                            {{ $listingCustomers->entry_street_address }},
                                            @endif
                                            @if(!empty($listingCustomers->entry_city))
                                            {{ $listingCustomers->entry_city }},
                                            @endif
                                            @if(!empty($listingCustomers->entry_state))
                                            {{ $listingCustomers->zone_name }},
                                            @endif
                                            @if(!empty($listingCustomers->entry_postcode))
                                            {{ $listingCustomers->entry_postcode }}
                                            @endif
                                            @if(!empty($listingCustomers->countries_name))
                                            {{ $listingCustomers->countries_name }}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                    {{ trans('labels.Action') }}
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                  <li><a class="dropdown-item" href="{{url('admin/customers/edit') }}/{{$listingCustomers->id}}">{{ trans('labels.EditCustomers') }}</a></li>
                                                  <li><a class="dropdown-item" href="{{url('admin/customers/address/display/'.$listingCustomers->id) }}">{{ trans('labels.EditAddress') }}</a></li>
                                                  <li><a class="dropdown-item" data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteCustomerFrom" users_id="{{ $listingCustomers->id }}">{{ trans('labels.Delete') }}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="4">{{ trans('labels.NoRecordFound') }}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            @if (count($customers['result']) > 0)
                            <div class="col-md-12 text-right">
                                {{--'vendor.pagination.default'--}}
                                {{--{{$customers['result']->links()}}--}}
                                {!! $customers['result']->appends(\Request::except('page'))->render() !!}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteCustomerModal" tabindex="-1" role="dialog" aria-labelledby="deleteCustomerModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteCustomerModalLabel">{{ trans('labels.Delete') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/customers/delete', 'name'=>'deleteCustomer', 'id'=>'deleteCustomer', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                {!! Form::hidden('action', 'delete', array('class'=>'form-control')) !!}
                {!! Form::hidden('users_id', '', array('class'=>'form-control', 'id'=>'users_id')) !!}
                <div class="modal-body">
                    <p>{{ trans('labels.DeleteCustomerText') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Delete') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content notificationContent">
            </div>
        </div>
    </div>

</section>
@endsection
