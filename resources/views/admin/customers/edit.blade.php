@extends('admin.layout')
@section('title-dash') {{ trans('labels.EditCurrentCustomers') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item"><a href="{{ URL::to('admin/customers/display')}}">{{ trans('labels.ListingAllCustomers') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.EditCustomers') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/customers/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header card-danger card-outline">
                        <h3 class="card-title">{{ trans('labels.EditCustomers') }} </h3>
                    </div>
                    
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif
                        
                        {!! Form::hidden('customers_id', $data['customers']->id, array('class'=>'form-control', 'id'=>'id')) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.FirstName') }}* </label>
                                    <div class="col-md-12">
                                        {!! Form::text('first_name', $data['customers']->first_name, array('class'=>'form-control field-validate', 'id'=>'first_name')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.LastName') }}*</label>
                                    <div class="col-md-12">
                                        {!! Form::text('last_name', $data['customers']->last_name , array('class'=>'form-control field-validate', 'id'=>'last_name')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.lastNameText') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Telephone') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('phone',  $data['customers']->phone, array('class'=>'form-control phone-validate', 'id'=>'phone')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TelephoneText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.changePassword') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::checkbox('changePassword', 'yes', null, ['class' => '', 'id'=>'change-passowrd']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group password" style="display: none">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Password') }}*</label>
                                    <div class="col-md-12">
                                        {!! Form::password('password', array('class'=>'form-control ', 'id'=>'password')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.PasswordText') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="status">
                                            <option @if($data['customers']->status == 1) selected @endif value="1">{{ trans('labels.Active') }}</option>
                                            <option @if($data['customers']->status == 0) selected @endif value="0">{{ trans('labels.Inactive') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }} </button>
                        <a href="{{ URL::to('admin/customers/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
