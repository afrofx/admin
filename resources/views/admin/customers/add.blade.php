@extends('admin.layout')
@section('title-dash') {{ trans('labels.AddNEWCustomer') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item"><a href="{{ URL::to('admin/customers/display')}}">{{ trans('labels.ListingAllCustomers') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.AddCustomer') }}</li>
@endsection
@section('content')
<section class="content">
	{!! Form::open(array('url' =>'admin/customers/add', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
		<div class="row">
			<div class="col-md-12">
				<div class="card card-danger card-outline">
					<div class="card-header">
						<h3 class="card-title">{{ trans('labels.AddCustomer') }} </h3>
					</div>
					
					<div class="card-body">
						@if (session('update'))
							<div class="alert alert-success alert-dismissable custom-success-card" style="margin: 15px;">
								<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>
								<strong> {{ session('update') }} </strong>
							</div>
						@endif

						@if (count($errors) > 0)
							@if($errors->any())
								<div class="alert alert-danger alert-dismissible" role="alert">
									<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									{{$errors->first()}}
								</div>
							@endif
						@endif
						
								
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.FirstName') }} </label>
									<div class="col-md-12">
										{!! Form::text('customers_firstname',  '', array('class'=>'form-control field-validate', 'id'=>'customers_firstname')) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.LastName') }} </label>
									<div class="col-md-12">
										{!! Form::text('customers_lastname',  '', array('class'=>'form-control field-validate', 'id'=>'customers_lastname')) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.lastNameText') }}</span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Telephone') }}</label>
									<div class="col-md-12">
										{!! Form::text('customers_telephone',  '', array('class'=>'form-control phone-validate', 'id'=>'customers_telephone')) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TelephoneText') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
							<!-- <div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Fax') }}</label>
									<div class="col-md-12">
									{!! Form::text('customers_fax',  '', array('class'=>'form-control', 'id'=>'customers_fax')) !!}
									<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FaxText') }}</span>
									</div>
								</div> -->
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.EmailAddress') }} </label>
									<div class="col-md-12">
										{!! Form::text('email',  '', array('class'=>'form-control email-validate', 'id'=>'email')) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.EmailText') }}</span>
										<span class="help-block hidden"> {{ trans('labels.EmailError') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Password') }}</label>
									<div class="col-md-12">
										{!! Form::password('password', array('class'=>'form-control field-validate', 'id'=>'password')) !!}
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.PasswordText') }}</span>
										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }} </label>
									<div class="col-md-12">
										<select class="form-control" name="isActive">
											<option value="1">{{ trans('labels.Active') }}</option>
											<option value="0">{{ trans('labels.Inactive') }}</option>
										</select>
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusText') }}</span>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card-footer text-center">
						<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
						<a href="{{ URL::to('admin/customers/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
					</div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
</section>
@endsection
