@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllCoupons') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item active">Cupões</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title form-inline" id="contact-form">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/coupons/filter')}}">
                            <input type="hidden"  value="{{csrf_token()}}">
                            <div class="input-group-form search-panel ">
                                <select type="button" class="btn btn-default btn-flat dropdown-toggle form-control" data-toggle="dropdown" name="FilterBy" id="FilterBy"  >
                                    <option value="" selected disabled hidden>Filter By</option>
                                    <option value="Code"  @if(isset($name)) @if  ($name == "Code") {{ 'selected' }} @endif @endif>{{ trans('labels.Code') }}</option>
                                </select>
                                <input type="text" class="form-control input-group-form " name="parameter" placeholder="Search term..." id="parameter" @if(isset($param)) value="{{$param}}" @endif >
                                <button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
                                @if(isset($param,$name))  <a class="btn btn-danger btn-flat " href="{{url('admin/coupons/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                            </div>
                        </form>
                        <div class="form-inline" id="contact-form12"></div>
                    </div>
                    
                    <div class="card-tools right">
                        <a href="{{ URL::to('admin/coupons/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>@sortablelink('coupans_id', trans('labels.ID') )</th>
                                        <th>@sortablelink('code', trans('labels.Code') )</th>
                                        <th>@sortablelink('discount_type', trans('labels.CouponType') )</th>
                                        <th>@sortablelink('amount', trans('labels.CouponAmount') )</th>
                                        <th>@sortablelink('description', trans('labels.Description') )</th>
                                        <th>@sortablelink('expiry_date', trans('labels.ExpiryDate') )</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($coupons !== null)
                                        @foreach ($coupons as $key=>$coupan)
                                            <tr>
                                                <td>{{ $coupan->coupans_id }}</td>
                                                <td>{{ $coupan->code }}</td>
                                                <td>{{ str_replace('_', ' ', $coupan->discount_type) }} </td>
                                                <td>
                                                    @if($coupan->discount_type=='fixed_product' or $coupan->discount_type=='fixed_cart')
                                                        @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $coupan->amount }} 
                                                        @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif 
                                                    @else 
                                                        {{ $coupan->amount }}%
                                                    @endif
                                                </td>
                                                <td>{{ $coupan->description }} </td>
                                                <td>{{ date('d/m/Y',strtotime($coupan->expiry_date)) }} </td>

                                                <td>
                                                    <a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{ url('admin/coupons/edit')}}/{{$coupan->coupans_id}}" class="badge badge-secondary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteCoupans_id" coupans_id ="{{ $coupan->coupans_id }}" class="badge badge-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8"><strong>{{ trans('labels.NoRecordFound') }}</strong></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <div class="col-md-12 text-right">
                                {!! $coupons->appends(\Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteCoupanModal" tabindex="-1" role="dialog" aria-labelledby="deleteCoupanModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteCoupanModalLabel">{{ trans('labels.DeleteCoupon') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                </div>
                {!! Form::open(array('url' =>'admin/coupons/delete', 'name'=>'deleteCoupan', 'id'=>'deleteCoupan', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                    {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'coupans_id')) !!}
                    <div class="modal-body">
                        <p>{{ trans('labels.DeleteCouponText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-flat" id="deleteCoupanBtn">{{ trans('labels.Delete') }} </button>
                        <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
