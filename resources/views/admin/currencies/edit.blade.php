@extends('admin.layout')
@section('title-dash') {{ trans('labels.Edit Currency') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item"><a href="{{ URL::to('admin/currencies/display')}}">{{ trans('labels.Currencies') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.Edit Currency') }}</li>
@endsection
@section('content')


<section class="content">
    {!! Form::open(array('url' =>'admin/currencies/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.Edit Currency') }} </h3>
                    </div>
                    
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                            @endif
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session()->get('error') }}
                            </div>
                        @endif
                        
                        {!! Form::hidden('id', $result['currency']->id , array('class'=>'form-control', 'id'=>'id')) !!}
                        <input type="hidden" name="warning" value="{{$result['warning']}}" />

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.title') }} </label>
                                    <div class="col-md-12 col-md-12">
                                        {!! Form::text('title', $result['currency']->title, array('class'=>'form-control field-validate', 'id'=>'title'))!!}
                                        <span class="help-block"style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.title') }}
                                        </span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Country') }} </label>
                                    <div class="col-md-12 col-md-12">
                                        <select class="form-control field-validate" name="code">
                                            @foreach($currencies as $currency)
                                                <option @if($result['currency']->code == $currency->code) selected @endif value="{{$currency->code}}">{{ $currency->currency_name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.Choose Country') }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.symbol') }} </label>
                                    <div class="col-md-12 col-md-12">
                                        @if(!empty($result['currency']->symbol_left))
                                            {!! Form::text('symbol', $result['currency']->symbol_left, array('class'=>'form-control field-validate','id'=>'symbol'))!!}
                                        @else
                                            {!! Form::text('symbol', $result['currency']->symbol_right, array('class'=>'form-control field-validate','id'=>'symbol'))!!}
                                        @endif
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.symbol text') }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Position') }}</label>
                                    <div class="col-md-12 col-md-12">
                                        <select class="form-control" name="position">
                                            <option value="left" @if(!empty($result['currency']->symbol_left)) selected @endif>{{ trans('labels.Left') }}</option>
                                            <option value="right" @if(!empty($result['currency']->symbol_right)) selected @endif>{{ trans('labels.Right') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.Choose position of the currency') }}
                                        </span>
                                    </div>  
                                </div> 
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" style="display: none">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.decimal_point') }}</label>
                                    <div class="col-md-12 col-md-12">
                                        {!! Form::text('decimal_point',  $result['currency']->decimal_point, array('class'=>'form-control', 'id'=>'decimal_point'))!!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.decimal_point') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" style="display: none">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.thousands_point') }}</label>
                                    <div class="col-md-12 col-md-12">
                                        {!! Form::text('thousands_point',  $result['currency']->thousands_point, array('class'=>'form-control', 'id'=>'thousands_point'))!!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.thousands_point') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.decimal_places') }}</label>
                                    <div class="col-md-12 col-md-12">
                                        {!! Form::text('decimal_places',  $result['currency']->decimal_places, array('class'=>'form-control field-validate', 'id'=>'decimal_places'))!!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.decimal_places') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.value') }}</label>
                                    <div class="col-md-12 col-md-12">
                                        @if($result['currency']->is_default==1)
                                            {!! Form::text('value',  $result['currency']->value, array('class'=>'form-control field-validate', 'id'=>'value', 'readonly'=>'readonly'))!!}
                                        @else
                                            {!! Form::text('value',  $result['currency']->value, array('class'=>'form-control field-validate', 'id'=>'value'))!!}
                                        @endif
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                            {{ trans('labels.value') }}
                                        </span>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                    
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        <a href="{{ URL::to('admin/currencies/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
