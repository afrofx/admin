@extends('admin.layout')
@section('title-dash') {{ trans('labels.ListingAllLanguages') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item active">{{ trans('labels.languages') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title form-inline" id="contact-form">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/languages/filter')}}">
                            <input type="hidden"  value="{{csrf_token()}}">
                            <div class="input-group-form search-panel ">
                                <select type="button" class="btn btn-default dropdown-toggle form-control" data-bs-toggle="dropdown" name="FilterBy" id="FilterBy"  >
                                    <option value="" selected disabled hidden>{{trans('labels.Filter By')}}</option>
                                    <option value="Language"  @if(isset($filter)) @if  ($filter == "Name") {{ 'selected' }} @endif @endif>{{trans('labels.Language')}}</option>
                                    <option value="Code" @if(isset($filter)) @if  ($filter == "E-mail") {{ 'selected' }}@endif @endif>{{trans('labels.Code')}}</option>
                                </select>
                                <input type="text" class="form-control input-group-form " name="parameter" placeholder="Search term..." id="parameter" @if(isset($parameter)) value="{{$parameter}}" @endif >
                                <button class="btn btn-primary  btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
                                @if(isset($parameter,$filter))  <a class="btn btn-danger" href="{{url('admin/languages/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                            </div>
                        </form>
                        <div class="form-inline" id="contact-form12"></div>
                    </div>
                    <div class="card-tools right">
                        <a href="{{ URL::to('admin/languages/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if ($errors)
                                @if($errors->any())
                                    <div @if ($errors->first()=='Default can not Deleted!!') class="alert alert-danger alert-dismissible" @else class="alert alert-success alert-dismissible" @endif role="alert">
                                        <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{$errors->first()}}
                                    </div>
                                @endif
                            @endif
                        </div>
                        
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>@sortablelink('languages_id', trans('labels.ID') )</th>
                                        <th>@sortablelink('languages_id',  trans('labels.Default') )</th>
                                        <th>@sortablelink('name',  trans('labels.Language') )</th>
                                        <th>{{ trans('labels.Icon') }}</th>
                                        <th>@sortablelink('code',  trans('labels.Code') )</th>
                                        <th>@sortablelink('sort_order',  trans('labels.Sort') )</th>
                                        <th>{{trans('labels.Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($result['languages'])
                                    @foreach ($result['languages'] as $key=>$languages)
                                        <tr>
                                            <td>
                                                {{ $languages->languages_id}}
                                            </td>
                                                <td>
                                                <label>
                                                    <input type="radio" name="languages_id" value="{{ $languages->languages_id}}"  class="default_language" @if($languages->is_default==1) checked @endif >
                                                </label>
                                            </td>
                                            <td>{{ $languages->name }}</td>
                                            <td><img src="{{asset($languages->path)}}" width="25px" alt=""></td>
                                            <td>{{ $languages->code }}</td>
                                            <td>{{ $languages->sort_order }}</td>
                                            <td>
                                                <a data-bs-toggle="tooltip" data-placement="bottom" title=" {{ $languages->name }}" href="{{ URL::to('admin/languages/edit/'.$languages->languages_id)}}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                @if($languages->is_default==0)
                                                    <a data-bs-toggle="tooltip" data-placement="bottom" title=" {{ $languages->name }}" id="deleteLanguageId" languages_id ="{{ $languages->languages_id }}" class="btn btn-sm  btn-flat btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">{{ trans('labels.Nolanguageexist') }}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            @if($result['languages'] != null)
                                <div class="col-md-12 text-right">
                                    {{$result['languages']->links()}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteLanguagesModal" tabindex="-1" role="dialog" aria-labelledby="deleteLanguagesModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteLanguagesModalLabel">{{ trans('labels.DeleteLanguages') }}</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                {!! Form::open(array('url' =>'admin/languages/delete', 'name'=>'deletelanguages', 'id'=>'deletelanguages', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'languages_id')) !!}
                <div class="modal-body">
                    <p>{{ trans('labels.confrimLanguageDelete') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat"  data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                    <button type="submit" class="btn btn-primary btn-flat" id="deletelanguages">{{ trans('labels.Delete') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
