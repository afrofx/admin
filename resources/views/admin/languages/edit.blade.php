@extends('admin.layout')
@section('title-dash') {{ trans('labels.EditCurrentLanguage') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item "><a href="{{ URL::to('admin/languages/display')}}">{{ trans('labels.languages') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.EditLanguage') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/languages/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.EditLanguage') }}</h3>
                    </div>
                    
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif

                        {!! Form::hidden('id',  $result['languages'][0]->languages_id, array('class'=>'form-control', 'id'=>'languages_id'))!!}
                        {!! Form::hidden('oldImage', $result['languages'][0]->path , array('id'=>'oldImage')) !!}
                            
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Name') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {!! Form::text('name',  $result['languages'][0]->name, array('class'=>'form-control field-validate', 'id'=>'name'))!!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.exampleLanguageName') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Code') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {!! Form::text('code',  $result['languages'][0]->code, array('class'=>'form-control field-validate', 'id'=>'code'))!!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.exampleLanguageCode') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Direction') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control field-validate" id="direction" name="directions">
                                            <option value="rtl" @if($result['languages'][0]->direction=="rtl") selected @endif>{{ trans('labels.RightToLeft') }}</option>
                                            <option value="ltr" @if($result['languages'][0]->direction=="ltr") selected @endif>{{ trans('labels.LeftToRight') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.LanguageDirection') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                @if($result['languages'][0]->is_default != 1)
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control field-validate" id="direction" name="status">
                                            <option value="1" @if($result['languages'][0]->status==1) selected @endif>{{ trans('labels.Active') }}</option>
                                            <option value="0" @if($result['languages'][0]->status==0) selected @endif>{{ trans('labels.Disable') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.LanguageDirection') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Icon') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {{--{!! Form::file('newImage', array('id'=>'newImage')) !!}--}}
                                        <!-- Modal -->
                                        <div class="modal fade embed-images" id="Modalmanufactured" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body manufacturer-image-embed">
                                                        @if(isset($allimage))
                                                            <select class="image-picker show-html " name="image_id" id="select_img">
                                                                <option  value=""></option>
                                                                @foreach($allimage as $key=>$image)
                                                                    <option data-img-src="{{asset($image->path)}}"  class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                                @endforeach
                                                            </select>
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary pull-left" >{{ trans('labels.Add Icon') }}</a>
                                                        <button type="button" class="btn btn-default refresh-image"><i class="fa fa-refresh"></i></button>
                                                        <button type="button" class="btn btn-success" id="selectedICONE" data-bs-dismiss="modal">{{ trans('labels.Done') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="imageselected">
                                            {!! Form::button(trans('labels.Add Icon'), array('id'=>'newIcon','class'=>"btn btn-primary btn-flat", 'data-bs-toggle'=>"modal", 'data-bs-target'=>"#Modalmanufactured" )) !!}
                                            <br>
                                            <div id="selectedthumbnailIcon" class="selectedthumbnail col-md-5"> </div>
                                            <div class="closimage">
                                                <button type="button" class="close pull-left image-close " id="image-Icone" style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label"></label>
                                    <div class="col-sm-12 col-md-12">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.OldImage') }}</span>
                                        <br>
                                        {!! Form::hidden('oldImage', $result['languages'][0]->image, array('id'=>'oldImage')) !!}
                                        @if(($result['languages'][0]->path!== null))
                                            <img width="80px" src="{{asset($result['languages'][0]->path)}}" class="">
                                        @else
                                            <img width="80px" src="{{asset($result['languages'][0]->path) }}" class="">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        <a href="{{ URL::to('admin/languages/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
