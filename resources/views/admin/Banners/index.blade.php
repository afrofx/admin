@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllBanners') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item active">{{ trans('labels.Banners') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title form-inline" id="contact-form">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/banners/filter')}}">
                            <input type="hidden"  value="{{csrf_token()}}">
                            {{--<div class="input-group-btn search-panel ">--}}
                            <div class="input-group-form search-panel ">
                                <select type="button" class="btn btn-default btn-sm dropdown-toggle form-control" data-toggle="dropdown" name="FilterBy" id="FilterBy"  >
                                    <option value="" selected disabled hidden>Filtrar por</option>
                                    <option value="Title"  @if(isset($name)) @if  ($name == "Title") {{ 'selected' }} @endif @endif>Title</option>
                                </select>
                                
                                <input type="text" class="form-control input-group-form " name="parameter" placeholder="Procurar..." id="parameter" @if(isset($param)) value="{{$param}}" @endif >
                                
                                <button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
                                @if(isset($param,$name))  
                                <a class="btn btn-danger btn-flat" href="{{url('admin/banners')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>
                                @endif
                            </div>
                        </form>

                        <div class="form-inline" id="contact-form12"></div>
                    </div>

                    <div class="card-tools right">
                        <a href="{{url('admin/banners/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNewBanner') }}</a>
                    </div>
                </div>
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@sortablelink('banners_id', trans('labels.ID') )</th>
                                    <th>@sortablelink('banners_title', trans('labels.Title') )</th>
                                    <th>{{ trans('labels.Image') }}</th>
                                    <th>@sortablelink('created_at', trans('labels.AddedModifiedDate') )</th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(count($result['banners'])>0)
                                        @php $resultitems['banners']  = $result['banners']->unique('banners_id');@endphp
                                        @foreach ($resultitems['banners'] as $key=>$banners)
                                            <tr>
                                                <td>{{ $banners->banners_id }}</td>
                                                <td>{{ $banners->banners_title }}</td>
                                                <td><img src="{{asset($banners->path)}}" alt="" width=" 100px"></td>
                                                <td>
                                                    <strong>{{ trans('labels.AddedDate') }}: </strong> {{ date('d M, Y', strtotime($banners->created_at)) }}<br>
                                                    <strong>{{ trans('labels.ModifiedDate') }}: </strong>@if(!empty($banners->updated_at)) {{ date('d M, Y', strtotime($banners->updated_at)) }}  @endif<br>
                                                    <strong>{{ trans('labels.ExpiryDate') }}: </strong>@if(!empty($banners->expires_date)) {{ date('d M, Y', strtotime($banners->expires_date)) }}  @endif
                                                </td>
                                                <td>
                                                    <a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{url('admin/banners/edit')}}/{{ $banners->banners_id }}" class="badge badge-secondary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteBannerId" banners_id ="{{ $banners->banners_id }}" class="badge badge-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <div class="col-md-12 text-right">
                                {!! $result['banners']->appends(\Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
    <div class="modal fade" id="deleteBannerModal" tabindex="-1" role="dialog" aria-labelledby="deleteBannerModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteBannerModalLabel">{{ trans('labels.DeleteBanner') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/banners/delete', 'name'=>'deleteBanner', 'id'=>'deleteBanner', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                    {!! Form::hidden('banners_id',  '', array('class'=>'form-control', 'id'=>'banners_id')) !!}
                    <div class="modal-body">
                        <p>{{ trans('labels.DeleteBannerText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="deleteBanner">{{ trans('labels.Delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
