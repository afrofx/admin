@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddBanner') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/banners')}}">Lista de Banners</a></li>
<li class="breadcrumb-item active">{{ trans('labels.AddBanner') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/banners/insert', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">

                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.AddBanner') }}</h3>
                    </div>
                    
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Language') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="languages_id">
                                            @foreach($result['languages'] as $language)
                                                <option value="{{$language->languages_id}}">{{ $language->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.ChooseLanguageText') }}</span>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Title') }} </label>
                                    <div class="col-md-12">
                                        {!! Form::text('banners_title', '', array('class'=>'form-control field-validate','id'=>'banners_title')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.BannerTitletext') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="imageIcone">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }}</label>
                                    <div class="col-md-12">
                                        <div class="modal fade embed-images" id="ModalmanufacturedICone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                        <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body manufacturer-image-embed">
                                                        @if(isset($allimage))
                                                            <select class="image-picker show-html " name="image_id" id="select_img">
                                                                <option value=""></option>
                                                                @foreach($allimage as $key=>$image)
                                                                    <option  data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                                @endforeach
                                                            </select>
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary pull-left btn-flat" >{{ trans('labels.Add Image') }}</a>
                                                        <button type="button" class="btn btn-default refresh-image btn-flat"><i class="fa fa-refresh"></i></button>
                                                        <button type="button" class="btn btn-success btn-flat" id="selectedICONE" data-bs-dismiss="modal">{{ trans('labels.Done') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="imageselected">
                                            {!! Form::button(trans('labels.Add Image'), array('id'=>'newIcon','class'=>"btn btn-primary btn-flat field-validate", 'data-bs-toggle'=>"modal", 'data-bs-target'=>"#ModalmanufacturedICone" )) !!}
                                            <br>
                                            <div id="selectedthumbnailIcon" class="selectedthumbnail col-md-5"> </div>
                                            <div class="closimage">
                                                <button type="button" class="close pull-left image-close " id="image-Icone" style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ImageText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Type') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="type" id="bannerType" onchange="bannerstypes()">
                                            <option value="category" hidden selected disabled>Escolher Categoria</option>
                                            <option value="product">{{ trans('labels.Product') }}</option>
                                            <option value="top seller">{{ trans('labels.TopSeller') }}</option>
                                            <option value="deals">{{ trans('labels.Deals') }}</option>
                                            <option value="most liked">{{ trans('labels.MostLiked') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;"> {{ trans('labels.AddBannerText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                {!! Form::hidden('banners_url', '', array('class'=>'form-control','id'=>'banners_url')) !!}
                            </div>

                            <div class="col-md-6">
                                <div class="form-group categoryContent">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Categories') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="categories_id" id="categories_id">
                                            @foreach($result['categories'] as $category)
                                                <option value="{{ $category->id}}">{{ $category->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.CategoriesbannerText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group productContent" style="display: none">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Products') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="products_id" id="products_id">
                                            @foreach($result['products'] as $products_data)
                                                <option value="{{ $products_data->products_id }}">{{ $products_data->products_name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ProductsBannerText') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.ExpiryDate') }}</label>
                                    <div class="col-md-12">
                                        <input readonly class="form-control datepicker field-validate" type="text" name="expires_date" value="">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ExpiryDateBanner') }}</span>
                                    </div>
                                </div>
                            </div>
                                        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="status">
                                            <option value="1">{{ trans('labels.Active') }}</option>
                                            <option value="0">{{ trans('labels.InActive') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusBannerText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        <a href="{{ URL::to('admin/banners')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection


@section('addjs')
    <script>
        $("#select_img").imagepicker()
     </script>
@endsection