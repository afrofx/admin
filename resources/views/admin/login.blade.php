@extends('admin.layoutLlogin')
@section('content')
    <style>
        .wrapper {
            display: none !important;
        }
    </style>
    <div class="login-box">
		<div class="card card-danger card-outline">
			<div class="card-header text-center">
				@if (empty($web_setting[15]->value))
					@if ($web_setting[66]->value == '1' and $web_setting[67]->value == '0')
						<img src="{{ asset('images/admin_logo/logo-android-blue-v1.png') }}" class="ionic-hide">
						<img src="{{ asset('images/admin_logo/logo-ionic-blue-v1.png') }}" class="android-hide">
					@elseif($web_setting[66]->value == '1' and $web_setting[67]->value == '1' or $web_setting[66]->value == '0' and $web_setting[67]->value == '1')
						<img src="{{ asset('images/admin_logo/logo-laravel-blue-v1.png') }}" class="website-hide">
					@endif
				@else
					<img style="width: 60%" src="{{ asset('images/admin_logo/logo-laravel-blue-v1.png') }}">
				@endif
			</div>
			

			<div class="card-body">
				@if (count($errors) > 0)
					@foreach ($errors->all() as $error)
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">{{ trans('labels.Error') }}:</span>
							{{ $error }}
						</div>
					@endforeach
				@endif

				@if (Session::has('loginError'))
					<div class="alert alert-danger" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">{{ trans('labels.Error') }}:</span>
						{!! session('loginError') !!}
					</div>
				@endif

				{!! Form::open(['url' => 'admin/checkLogin', 'method' => 'post', 'class' => 'form-validate']) !!}
					<div class="input-group mb-3 has-feedback">
						<input type="email" name="email" class="form-control email-validate" id="email" placeholder="Email">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fa fa-envelope"></span>
							</div>
						</div>
					</div>

					<div class="input-group mb-3">
						<input type="password" name='password' class="form-control field-validate" id="email" placeholder="Senha" value="">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fa fa-lock"></span>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-4">
							{!! Form::submit('Autenticar', ['id' => 'login', 'class' => 'btn btn-danger btn-block btn-flat']) !!}
						</div>
					</div>
				{!! Form::close() !!}

			</div>
		</div>
    </div>
