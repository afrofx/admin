@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingNewsCategories') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.NewsCategories') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/newscategories/filter')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="input-group search-panel ">
                                <select type="button" class="btn btn-default dropdown-toggle form-control input-group-form " data-bs-toggle="dropdown" name="FilterBy" id="FilterBy" >
                                    <option value="" selected disabled hidden>{{trans('labels.Filter By')}}</option>
                                    <option value="Name"  @if(isset($name)) @if  ($name == "Name") {{ 'selected' }} @endif @endif>{{trans('labels.Name')}}</option>
                                </select>
                                <span class="input-group-append">
                                <input type="text" class="form-control input-group" name="parameter" placeholder="Pesquisar..." id="parameter"  @if(isset($param)) value="{{$param}}" @endif >
                                <button class="btn btn-primary btn-flat" id="submit" type="submit"><span class="fa fa-search"></span></button>
                                @if(isset($param,$name))  <a class="btn btn-danger btn-flat" href="{{url('admin/newscategories/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-tools">
                        <a href="{{url('admin/newscategories/add')}}" type="button" class="btn btn-block btn-primary r btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@sortablelink('categories_id', trans('labels.ID') )</th>
                                    <th>@sortablelink('categories_name', trans('labels.Name') )</th>
                                    <th>{{ trans('labels.Image') }}</th>
                                    <th>@sortablelink('created_at', trans('labels.AddedLastModifiedDate') )</th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($listingCategories != null)
                                    @php $listingCategoriesList = $listingCategories->unique('id') @endphp
                                    @foreach ($listingCategoriesList as $key=>$categories)
                                        <tr>
                                            <td>{{ $categories->id }}</td>
                                            <td>{{ $categories->name }}</td>
                                            <td><img src="{{asset($categories->path)}}" alt="" width=" 100px"></td>
                                            <td><strong>{{ trans('labels.AddedDate') }}: </strong> {{ $categories->date_added }}<br>
                                                <strong>{{ trans('labels.ModifiedDate') }}: </strong>{{ $categories->last_modified }}  </td>
                                            <td>
                                                <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{url('admin/newscategories/edit')}}/{{ $categories->id }}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deleteNewsCategroyId" category_id ="{{ $categories->id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5"> {{ trans('labels.NoRecordFound') }}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 text-right">
                            {!! $listingCategories->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="deleteNewsCategoryModal" tabindex="-1" role="dialog" aria-labelledby="deleteNewsCategoryModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteNewsCategoryModalLabel">{{ trans('labels.DeleteNewsCategory') }}</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(array('url' =>'admin/newscategories/delete', 'name'=>'deleteNewsCategory', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeleteNewsCategoryText') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-defaultr btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                <button type="submit" class="btn btn-primaryr btn-flat" id="deleteNewsCategory">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
