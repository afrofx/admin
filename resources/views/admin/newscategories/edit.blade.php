@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditNewsCategories') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/newscategories')}}">{{ trans('labels.Categories') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.EditNewscurrentCategories') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/newscategories/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.EditNewsCategories') }} </b></h3>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        {!! Form::hidden('id',  $result['editCategory'][0]->id , array('class'=>'form-control', 'id'=>'id')) !!}
                        {!! Form::hidden('oldImage',  $result['editCategory'][0]->image , array('id'=>'oldImage')) !!}
                        {!! Form::hidden('oldIcon',  $result['editCategory'][0]->icon , array('id'=>'oldIcon')) !!}

                        @foreach($result['description'] as $description_data)
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Name') }} ({{ $description_data['language_name'] }})</label>
                                <div class="col-md-12">
                                    <input type="text" name="category_name_<?=$description_data['languages_id']?>" class="form-control field-validate" value="{{$description_data['name']}}">
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.CategoryName') }} ({{ $description_data['language_name'] }}).</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.slug') }} </label>
                                <div class="col-md-12">
                                    <input type="hidden" name="old_slug" value="{{$result['editCategory'][0]->slug}}">
                                    <input type="text" name="slug" class="form-control field-validate" value="{{$result['editCategory'][0]->slug}}">
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.slugText') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="imageIcone">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }}</label>
                                <div class="col-md-12">
                                    <div class="modal fade embed-images" id="ModalmanufacturedICone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                </div>
                                                <div class="modal-body manufacturer-image-embed">
                                                    @if(isset($allimage))
                                                    <select class="image-picker show-html " name="image_id" id="select_img">
                                                        <option value=""></option>
                                                        @foreach($allimage as $key=>$image)
                                                            <option data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                        @endforeach
                                                    </select>
                                                    @endif
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary pull-left btn-flat" >{{ trans('labels.Add Image') }}</a>
                                                    <button type="button" class="btn btn-default refresh-image btn-flat"><i class="fa fa-refresh"></i></button>
                                                    <button type="button" class="btn btn-success btn-flat" id="selectedICONE" data-bs-dismiss="modal">{{ trans('labels.Done') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="imageselected">
                                        {!! Form::button(trans('labels.Add Image'), array('id'=>'newIcon','class'=>"btn btn-primary", 'data-bs-toggle'=>"modal", 'data-target'=>"#ModalmanufacturedICone" )) !!}
                                        <br>
                                        <div id="selectedthumbnailIcon" class="selectedthumbnail col-md-5"> </div>
                                        <div class="closimage">
                                            <button type="button" class="close pull-left image-close " id="image-Icone"
                                            style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ImageText') }}</span>

                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label"></label>
                                <div class="col-md-12">
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.OldImage') }}</span>
                                    <img src="{{asset('').$result['editCategory'][0]->path}}" alt="" width=" 100px">
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }} </label>
                                <div class="col-md-12">
                                    <select class="form-control" name="categories_status">
                                        <option value="1" @if($result['editCategory'][0]->categories_status==1) selected @endif>{{ trans('labels.Active') }}</option>
                                        <option value="0" @if($result['editCategory'][0]->categories_status==0) selected @endif>{{ trans('labels.Inactive') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.GeneralStatusText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{ URL::to('admin/newscategories/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection


@section('addjs')
    <script type="text/javascript">
        $(function() {
            @foreach($result['languages'] as $languages)
            CKEDITOR.replace('editor{{$languages->languages_id}}');
            @endforeach
            $(".textarea").wysihtml5();
        });

        
    </script>
    <script>
        $("#select_img").imagepicker()
     </script>
@endsection
