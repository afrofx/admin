@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditZone') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{url('admin/zones/display')}}">{{ trans('labels.ListingAllZones') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.EditZone') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/zones/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.EditZone') }}</b></h3>
                </div>
                <div class="card-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session()->get('message') }}
                        </div>
                    @endif

                    <div class="row">
                        {!! Form::hidden('zone_id', $result['zones']->zone_id, array('class'=>'form-control', 'id'=>'zone_name'))!!}

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Country') }}</label>
                                <div class="col-md-12">
                                    <select name="zone_country_id" class='form-control field-validate'>
                                        @foreach( $result['countries'] as $countries_data)
                                            <option @if( $countries_data->countries_id == $result['zones']->zone_country_id) selected @endif value="{{ $countries_data->countries_id }}"> {{ $countries_data->countries_name }} </option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseZoneCountry') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.ZoneName') }} </label>
                                <div class="col-md-12">
                                    {!! Form::text('zone_name', $result['zones']->zone_name, array('class'=>'form-control field-validate', 'id'=>'zone_name'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ZoneNameText') }}</span>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.ZoneCode') }}
                                </label>
                                <div class="col-md-12">
                                    {!! Form::text('zone_code', $result['zones']->zone_code, array('class'=>'form-control field-validate', 'id'=>'zone_code'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ZoneCodeText') }}</span>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{url('admin/zones/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection
