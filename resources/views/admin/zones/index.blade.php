@extends('admin.layout')
@section('title-dash')Paginas...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.Pages') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <form  name='registration' id="registration" class="registration" method="get" action="{{url('admin/zones/filter')}}">
                            <input type="hidden"  value="{{csrf_token()}}">
                            <div class="input-group search-panel ">
                                <select type="button" class="btn btn-default btn-flat dropdown-toggle form-control" data-bs-toggle="dropdown" name="FilterBy" id="FilterBy"  >
                                    <option value="" selected disabled hidden>{{trans('labels.Filter By')}}</option>
                                    <option value="Zone"  @if(isset($name)) @if  ($name == "Zone") {{ 'selected' }} @endif @endif>{{trans('labels.Zone')}}</option>
                                    <option value="Code" @if(isset($name)) @if  ($name == "Code") {{ 'selected' }}@endif @endif>{{trans('labels.Code')}}</option>
                                    <option value="Country" @if(isset($name)) @if  ($name == "Country") {{ 'selected' }}@endif @endif>{{trans('labels.Country')}}</option>
                                </select>
                                <input type="text" class="form-control input-group-form " name="parameter" placeholder="Pesquisar..." id="parameter" @if(isset($param)) value="{{$param}}" @endif >
                                <span class="input-group-append">
                                    <button class="btn btn-primary btn-flat " id="submit" type="submit"><span class="fa fa-search"></span></button>
                                    @if(isset($param,$name))  <a class="btn btn-danger btn-flat " href="{{url('admin/zones/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-tools">
                        <a href="{{url('admin/zones/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddZone') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@sortablelink('zone_id', trans('labels.ID') )</th>
                                    <th>@sortablelink('zone_name', trans('labels.Zone') )</th>
                                    <th>@sortablelink('zone_code', trans('labels.Code') )</th>
                                    <th>@sortablelink('countries_name', trans('labels.Country') )</th>
                                    <th>{{ trans('labels.Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($zones as $key=>$zone)
                                    <tr>
                                        <td>{{ $zone->zone_id }}</td>
                                        <td>{{ $zone->zone_name }}</td>
                                        <td>{{ $zone->zone_code }}</td>
                                        <td>{{ $zone->countries_name }}</td>
                                        <td><a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="{{url('admin/zones/edit/'.$zone->zone_id)}}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a  data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Delete') }}" id="deletezoneId" zone_id ="{{ $zone->zone_id }}" class="btn btn-danger btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                        @if($zones != null)
                            <div class="col-xs-12 text-right">
                                {{$zones->links()}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="deleteZoneModal" tabindex="-1" role="dialog" aria-labelledby="deleteZoneModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">                
                <h4 class="modal-title" id="deleteZoneModalLabel">{{ trans('labels.DeleteZone') }}</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(array('url' =>'admin/zones/delete', 'name'=>'deleteZone', 'id'=>'deleteZone', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'zone_id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeleteZoneText') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-bs-dismiss="modal">{{ trans('labels.Cancel') }}</button>
                <button type="submit" class="btn btn-primary btn-flat" id="deleteZone">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
