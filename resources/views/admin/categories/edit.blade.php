@extends('admin.layout')
@section('title-dash'){{ trans('labels.EditCategories') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item"><a href="{{ URL::to('admin/categories/display')}}">{{ trans('labels.Categories') }}</a></li>
	<li class="breadcrumb-item active">{{ trans('labels.EditCategories') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/categories/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title">{{ trans('labels.EditCategories') }} </h3>
                    </div>
                    
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif

                        {!! Form::hidden('id', $result['editSubCategory'][0]->id , array('class'=>'form-control', 'id'=>'id')) !!}
                        {!! Form::hidden('oldImage', $result['editSubCategory'][0]->image , array('id'=>'oldImage')) !!}
                        {!! Form::hidden('oldIcon', $result['editSubCategory'][0]->icon , array('id'=>'oldIcon')) !!}
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if($result['editSubCategory'][0]->id >0 )
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Category') }}</label>
                                        <div class="col-md-12">
                                            <select class="form-control" name="parent_id">
                                                {{print_r($result['categories'])}}
                                            </select>
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseMainCategory') }}</span>
                                        </div>
                                    </div>
                                @else
                                    <select hidden name="parent_id">
                                        <option value="0">Deixa como subcategoria</option>1
                                    </select>
                                @endif
                            </div>

                            
                            @foreach($result['description'] as $description_data)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Name') }} ({{ $description_data['language_name'] }})</label>
                                        <div class="col-md-12">
                                            <input type="text" name="category_name_<?=$description_data['languages_id']?>" class="form-control field-validate" value="{{$description_data['name']}}">
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.CategoryName') }} ({{ $description_data['language_name'] }}).</span>
                                            <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }} ({{ $description_data['language_name'] }})</label>
                                        <div class="col-md-12">
                                            <textarea id="editor_<?=$description_data['languages_id']?>" name="description_<?=$description_data['languages_id']?>" class="form-control"  rows="10" cols="80">{{$description_data['descriptions']}}</textarea>
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Description') }} ({{ $description_data['language_name'] }})</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.slug') }} </label>
                                    <div class="col-md-12">
                                        <input type="hidden" name="old_slug" value="{{$result['editSubCategory'][0]->slug}}">
                                        <input type="text" name="slug" class="form-control field-validate" value="{{$result['editSubCategory'][0]->slug}}">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.slugText') }}</span>
                                        <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Image') }}</label>
                                    <div class="col-md-12">
                                        <div class="modal fade" id="Modalmanufactured" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                        <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body manufacturer-image-embed">
                                                        @if(isset($allimage))
                                                        <select class="image-picker show-html " name="image_id" id="select_img">
                                                            <option value=""></option>
                                                            @foreach($allimage as $key=>$image)
                                                                <option data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                            @endforeach
                                                        </select>
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                    <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary btn-flat pull-left" >{{ trans('labels.Add Image') }}</a>
                                                    <button type="button" class="btn btn-default btn-flat refresh-image"><i class="fa fa-refresh"></i></button>
                                                    <button type="button" class="btn btn-primary btn-flat" id="selected" data-bs-dismiss="modal">Done</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {!! Form::button(trans('labels.Add Image'), array('id'=>'newImage','class'=>"btn btn-primary btn-flat ", 'data-toggle'=>"modal", 'data-target'=>"#Modalmanufactured" )) !!}
                                        <br>
                                        <div id="selectedthumbnail" class="selectedthumbnail col-md-5"> </div>
                                        <div class="closimage">
                                            <button type="button" class="close pull-left image-close" id="image-close" style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.UploadSubCategoryImage') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label"></label>
                                    <div class="col-md-12">
                                    <span class="help-block " style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.OldImage') }}</span>
                                    <br>
                                    <img src="{{asset($result['editSubCategory'][0]->imgpath)}}" alt="" width=" 100px">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Icon') }}</label>
                                    <div class="col-md-12">
                                        <div class="modal fade" id="ModalmanufacturedICone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title text-primary" id="myModalLabel">{{ trans('labels.Choose Image') }} </h3>
                                                        <button type="button" class="close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body manufacturer-image-embed">
                                                        @if(isset($allimage))
                                                        <select class="image-picker show-html " name="image_icone" id="select_img">
                                                            <option value=""></option>
                                                            @foreach($allimage as $key=>$image)
                                                                <option data-img-src="{{asset($image->path)}}" class="imagedetail" data-img-alt="{{$key}}" value="{{$image->id}}"> {{$image->id}} </option>
                                                            @endforeach
                                                        </select>
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{url('admin/media/add')}}" target="_blank" class="btn btn-primary btn-flat pull-left" >{{ trans('labels.Add Image') }}</a>
                                                        <button type="button" class="btn btn-default btn-flat refresh-image"><i class="fa fa-refresh"></i></button>
                                                        <button type="button" class="btn btn-primary btn-flat" id="selectedICONE" data-bs-dismiss="modal">Done</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="imageselected">
                                            {!! Form::button(trans('labels.Add Icon'), array('id'=>'newIcon','class'=>"btn btn-primary btn-flat ", 'data-toggle'=>"modal", 'data-target'=>"#ModalmanufacturedICone" )) !!}
                                            <br>
                                            <div id="selectedthumbnailIcon" class="selectedthumbnail col-md-5"> </div>
                                            <div class="closimage">
                                                <button type="button" class="close pull-left image-close " id="image-Icone" style="display: none; position: absolute;left: 105px; top: 54px; background-color: black; color: white; opacity: 2.2; " aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.UploadSubCategoryIcon') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label"></label>
                                    <div class="col-md-12">
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.OldImage') }}</span>
                                        <br>
                                        <img src="{{asset($result['editSubCategory'][0]->iconpath)}}" alt="" width=" 100px">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }} </label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="categories_status">
                                            <option value="1" @if($result['editSubCategory'][0]->categories_status=='1') selected @endif>{{ trans('labels.Active') }}</option>
                                            <option value="0" @if($result['editSubCategory'][0]->categories_status=='0') selected @endif>{{ trans('labels.Inactive') }}</option>
                                        </select>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                        {{ trans('labels.GeneralStatusText') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                        <a href="{{ URL::to('admin/categories/display')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
