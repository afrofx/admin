@extends('admin.layout')
@section('title-dash'){{ trans('labels.ListingAllMainCategories') }}...@endsection
@section('title-link')
	<li class="breadcrumb-item active">{{ trans('labels.MainCategories') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <b>Lista de Categorias</b>
                    </div>

                    <div class="card-tools right">
                        <a href="{{ URL::to('admin/categories/add')}}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNewCategory') }}</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <table id="myTable" class="table-striped">
                                <thead>
                                <tr>
                                    <th>{{trans('labels.ID')}}</th>
                                    <th>{{trans('labels.Name')}}</th>
                                    <th>{{trans('labels.Image')}}</th>
                                    <th>{{trans('labels.Icon')}}</th>
                                    <th>{{trans('labels.AddedLastModifiedDate')}}</th>
                                    <th>{{trans('labels.Status')}}</th>
                                    <th>{{trans('labels.Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(count($categories)>0)
                                        @php $categoriesunique = $categories->unique('id'); @endphp
                                        @foreach ($categories as $key=>$category)
                                            <tr>
                                                <td>@if($category->id == -1) 0 @else {{ $category->id }} @endif</td>
                                                <td>
                                                    @if($category->parent_name)
                                                        {{$category->parent_name}} /
                                                    @endif
                                                    {{ $category->name }}</td>
                                                <td><img src="{{asset($category->imgpath)}}" alt="" width=" 100px"></td>
                                                <td><img src="{{asset($category->iconpath)}}" alt="" width=" 100px"></td>
                                                <td>
                                                    <strong>{{ trans('labels.AddedDate') }}: </strong> {{ $category->date_added }}<br>
                                                    <strong>{{ trans('labels.ModifiedDate') }}: </strong>{{ $category->last_modified }}
                                                </td>
                                                <td>
                                                    @if($category->categories_status==1)
                                                        <span class="badge badge-success"> {{ trans('labels.Active') }} </span>
                                                    @elseif($category->categories_status==0)
                                                        <span class="badge badge-danger"> {{ trans('labels.InActive') }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <a data-bs-toggle="tooltip" data-placement="bottom" title="Edit" href="{{url('admin/categories/edit/'. $category->id) }}" class="btn btn-secondary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    @if($category->id >0 )<a id="delete" category_id="{{$category->id}}" href="#" class="btn btn-danger btn-flat" ><i class="fa fa-trash" aria-hidden="true"></i></a>@endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">{{ trans('labels.NoRecordFound') }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            @if($categories != null)
                                <div class="col-md-12 text-right">
                                        {{$categories->links()}}
                                </div>
                            @endif
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteModalLabel">{{ trans('labels.Delete') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {!! Form::open(array('url' =>'admin/categories/delete', 'name'=>'deleteBanner', 'id'=>'deleteBanner', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                    {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                    {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'category_id')) !!}
                    <div class="modal-body">
                        <p>{{ trans('labels.DeleteText') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default  btn-flat" data-bs-dismiss="modal">{{ trans('labels.Close') }}</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="deleteBanner">{{ trans('labels.Delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
