@extends('admin.layout')
@section('title-dash') {{ trans('labels.ImageDetail') }}...@endsection
@section('title-link') 
    <li class="breadcrumb-item "><a href="{{ URL::to('admin/media/add') }}">{{ trans('labels.AddNewImage') }}</a></li>
    <li class="breadcrumb-item active">{{ trans('labels.ImageDetail') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-body">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    
                    @if(isset($result['images']))
                        @foreach($result['images'] as $key=>$image)
                            <div class="row mt-2">
                                <div class="col-md-12">

                                    <div class="caption">
                                        <h6>{{$image->image_type}}  ({{$image->height}} X {{$image->width}})</h6>
                                    </div>

                                    <div class="thumbnail">
                                        <img src="{{asset($image->path)}}" alt="{{$image->height}} X {{$image->width}}">
                                        <div class="row mt-2">
                                            <div class="col-md-6">
                                                <div class="caption">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">@lang('labels.Path')</span>
                                                        </div>
                                                        <input type="text" class="form-control" name="path" value="{{asset($image->path)}}">                                                
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                @if($image->image_type !='ACTUAL')
                                                    {!! Form::open(array('url' =>'admin/media/regenerateimage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                                                        {!! Form::hidden('image_id',  $image->image_id, array('class'=>'form-control', 'id'=>'id')) !!}
                                                        {!! Form::hidden('id',  $image->id, array('class'=>'form-control', 'id'=>'id')) !!}
                                                        <div class="caption">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">@lang('labels.Size')</span>
                                                                </div>
                                                                <input required type="text" class="form-control" name="height" value="{{$image->height}}">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"> X </span>
                                                                </div>
                                                                <input required type="text" class="form-control" name="width" value="{{$image->width}}">
                                                                <span class="input-group-addon" style="padding: 0">
                                                                    <button type="submit" class="btn btn-primary btn-flat"> @lang('labels.Regenerate')</button>
                                                                </span>                                                
                                                            </div>
                                                        </div>
                                                    {!! Form::close() !!}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <p id="demo"></p>
    </div>
    
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Adicionar Imagens</h4>
                </div>
                <div class="modal-body">
                    <p>Clique aqui para adicionar imagens</p>
                    <form action="{{ url('admin/media/uploadimage') }}" enctype="multipart/form-data" class="dropzone " id="my-dropzone">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-flat" disabled="disabled" id="compelete" data-dismiss="modal">Guardar</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModaldetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title text-primary" id="myModalLabel">Detalhes da Imagens</h3>
                </div>

                {!! Form::open(array('url' =>'admin/deleteimage', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data', 'onsubmit' => 'return ConfirmDelete()')) !!}
                
                <div class="image_embed"></div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-flat" id="myDeleteImage"data-toggle="modal">Remover</button>
                    {{--<a href="#myModal2" role="button" type="submit" class="btn btn-danger" data-toggle="modal">Delete</a>--}}
                    <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal">Fechar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Aviso!!</h4>
                </div>
                <div class="modal-body">
                    <p>Tem Certeza que deseja remover!</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-flat" id="myDeleteImage" data-toggle="modal">Remover</button>
                    <button class="btn btn-default btn-flat" data-dismiss="modal" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
