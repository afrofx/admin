@extends('admin.layout')
@section('title-dash') {{ trans('labels.AddNewImage') }}... @endsection
@section('title-link') <li class="breadcrumb-item active"> {{ trans('labels.AddNewImage') }}@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card  card-danger card-outline">
                <div class="card-header">

                    <h3 class="card-title">{{ trans('labels.ListingAllImage') }} </h3>

                    <div class="card-tools left">
                        <button id="btn" type="button" class="btn btn-block btn-flat btn-danger">Remover</button>
                    </div>

                    <div class="card-tools left">
                        <button id="btn11" type="button" class="btn btn-block btn-flat btn-success" >Selecionar Todas</button>
                    </div>

                    <div class="card-tools left">
                        <button id="btn12" type="button" class="btn btn-block btn-flat btn-info" >Não Selecionar</button>
                    </div>

                    <div class="card-tools left">
                        <button type="button" class="btn btn-block btn-flat btn-primary" data-toggle="modal" data-target="#myModal">{{ trans('labels.AddNew') }}</button>
                    </div>

                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12">
                            @if (count($errors) > 0)
                                @if($errors->any())
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{$errors->first()}}
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>

                    <style>
                        article, aside, figure, footer, header, hgroup,
                        menu, nav, section { display: block; }
                        ul { list-style: none; }
                        ul li { display: inline; }
                        img { border: 2px solid white; cursor: pointer; }
                        img:hover { border: 2px solid black; }
                        img.hover { border: 2px solid black; }
                        .margin-bottomset .thumbnail { margin-bottom: 0; }
                    </style>

                    <form class="hidden" action="" method="" id="images_form">
                        <input id="images" type="hidden" name="images" value=""/>
                    </form>
                    <div class="row">
                        @if(isset($images))
                            @foreach($images as $image)
                                <div class="col-xs-4 col-md-2 margin-bottomset">
                                    <div class="thumbnail thumbnail-imges">
                                        <img class="test_image" image_id="{{$image->id}}" src="{{asset($image->path)}}" alt="...">
                                    </div>
                                    <a class="btn btn-block btn-flat btn-primary" href="{{url('admin/media/detail')}}/{{$image->id}}"> @lang('labels.ViewDetail')</a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <p id="demo"></p>
    </div>
    
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add File Here</h4>
                </div>
                <div class="modal-body">
                    <p>Clique aqui para carregar uma imagem.</p>
                    <form action="{{ url('admin/media/uploadimage') }}" enctype="multipart/form-data" class="dropzone " id="my-dropzone">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-flat" disabled="disabled" id="compelete"data-dismiss="modal">Done</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="myModaldetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-primary" id="myModalLabel">Detalhes da Imagem</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                {!! Form::open(array('url' =>'admin/deleteimage', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data', 'onsubmit' => 'return ConfirmDelete()')) !!}
                    <div class="image_embed"></div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-flat" id="myDeleteImage" data-toggle="modal">Remover</button>
                        {{--<a href="#myModal2" role="button" type="submit" class="btn btn-danger" data-toggle="modal">Delete</a>--}}
                        <button type="button" class="btn btn-primary btn-flat" data-bs-dismiss="modal">Fechar</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Aviso!!</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <p>Deseja Remover Esta Imagem!</p>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-flat" id="myDeleteImage" data-toggle="modal">Remover</button>
                    <button type="button" class="btn btn-default  btn-flat" data-bs-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
