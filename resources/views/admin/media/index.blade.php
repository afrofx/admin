

@extends('admin.layout')
@section('title-dash') {{ trans('labels.MediaSetting') }}... @endsection
@section('title-link') <li class="breadcrumb-item active"> {{ trans('labels.ImageSize') }} @endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">

                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.ImageSize') }}</h3>
                </div>
                
                <div class="card-body"> 
                    @if (session('update'))
                        <div class="alert alert-success alert-dismissable custom-success-card" style="margin: 15px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> {{ session('update') }} </strong>
                        </div>
                    @endif

                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="icon fa fa-check" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.ImageSize') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif

                    {!! Form::open(array('url' =>'admin/media/updatemediasetting', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                        <div class="row">
                            <div class="col-md-4">
                                <h5>{{ trans('labels.ThumbnailSetting') }}</h5>
                                <hr>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Thumbnail_height') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {!! Form::text($web_setting[87]->value,  $web_setting[87]->value, array('class'=>'form-control number-validate', 'id'=>$web_setting[87]->value, 'name'=>'ThumbnailHeight')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Thumbnail_height') }}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Thumbnail_width') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {!! Form::text($web_setting[88]->value,  $web_setting[88]->value, array('class'=>'form-control number-validate', 'id'=>$web_setting[88]->value, 'name'=>'ThumbnailWidth')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Thumbnail_width') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <h5>{{ trans('labels.MediumSetting') }}</h5>
                                <hr>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Medium_height') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {!! Form::text($web_setting[89]->value,  $web_setting[89]->value, array('class'=>'form-control number-validate', 'id'=>$web_setting[89]->value, 'name'=>'MediumHeight')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Medium_height') }}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                        <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Medium_width') }}</label>
                                        <div class="col-sm-12 col-md-12">
                                            {!! Form::text($web_setting[90]->value,  $web_setting[90]->value, array('class'=>'form-control number-validate', 'id'=>$web_setting[90]->value, 'name'=>'MediumWidth')) !!}
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Medium_width') }}</span>
                                        </div>
                                </div>
                            </div>

                            <div class="col-md-4">                   
                                <h5>{{ trans('labels.LargeSetting') }}</h5>
                                <hr>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Large_height') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {!! Form::text($web_setting[91]->value,  $web_setting[91]->value, array('class'=>'form-control number-validate', 'id'=>$web_setting[91]->value, 'name'=>'LargeHeight')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Large_height') }}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 col-md-12 control-label">{{ trans('labels.Large_width') }}</label>
                                    <div class="col-sm-12 col-md-12">
                                        {!! Form::text($web_setting[92]->value,  $web_setting[92]->value, array('class'=>'form-control number-validate', 'id'=>$web_setting[92]->value, 'name'=>'LargeWidth')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">{{ trans('labels.Large_width') }}</span>
                                    </div>
                                </div>
                            </div>    

                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                                <button type="submit" class="btn btn-success btn-flat" id="regenrate" name="regenrate" value="yes">{{ trans('labels.SaveRegenerate') }}</button>
                                <a href="{{ URL::to('admin/dashboard/this_month')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
