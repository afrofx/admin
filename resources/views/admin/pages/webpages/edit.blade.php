@extends('admin.layout')
@section('title-dash')Editar Pagina...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/webpages')}}">{{ trans('labels.ListingAllPages') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.EditPage') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updatewebpage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('labels.EditPage') }} </h3>
                </div>
                
                <div class="card-body">
                    @if( count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">{{ trans('labels.Error') }}:</span>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        {!! Form::hidden('id',  $result['editPage'][0]->page_id, array('class'=>'form-control', 'id'=>'id')) !!}
                        
                        <div class="col-md-12">    
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.PageSlug') }}</label>
                                <div class="col-md-12">
                                    {!! Form::text('slug',  $result['editPage'][0]->slug, array('class'=>'form-control field-validate', 'id'=>'slug')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.pageSlugWithDashesText') }}</span>
                                </div>
                            </div>
                        </div>
                        
                        @foreach($result['description'] as $description_data)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.PageName') }} ({{ $description_data['language_name'] }}) </label>
                                    <div class="col-md-12">
                                        <input type="text" name="name_<?=$description_data['languages_id']?>" class="form-control field-validate" value="{{$description_data['name']}}" >
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.PageName') }} ({{ $description_data['language_name'] }})</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }} ({{ $description_data['language_name'] }})</label>
                                    <div class="col-md-12">
                                        <textarea id="editor_<?=$description_data['languages_id']?>" name="description_<?=$description_data['languages_id']?>" class="form-control"  rows="10">{{$description_data['description']}}</textarea>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Description') }} ({{ $description_data['language_name'] }})</span>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
                                <div class="col-md-12">
                                    <select name="status" id="status" class="form-control">
                                        <option value="1"  @if($result['editPage'][0]->status=='1') selected @endif>{{ trans('labels.Active') }}</option>
                                        <option value="0"  @if($result['editPage'][0]->status=='0') selected @endif>{{ trans('labels.InActive') }}</option>
                                    </select>
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusPageText') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                    <a href="{{ URL::to('admin/webpages')}}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@endsection

@section('addjs')
<script type="text/javascript">
	$(function() {
		@foreach ($result['languages'] as $languages)
			CKEDITOR.replace('editor{{ $languages->languages_id }}');
		@endforeach
		$(".textarea").wysihtml5();
	});
</script>
@endsection