@extends('admin.layout')
@section('title-dash')Paginas...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.Pages') }}</li>
@endsection
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-tools pull-right">
                        <a href="{{ URL::to('admin/addwebpage') }}" type="button" class="btn btn-block btn-primary btn-flat">{{ trans('labels.AddNew') }}</a>
                    </div><br>
                </div>
                
                <div class="card-body">
                    @if (count($errors) > 0)
                        @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                        @endif
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>@sortablelink('page_id', trans('labels.ID') )</th>
                                        <th>@sortablelink('Name', trans('labels.Name') )</th>
                                        <th>@sortablelink('slug', trans('labels.Slug') )</th>
                                        <th>{{ trans('labels.Status') }}</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($result["pages"])>0)
                                    @foreach ($result["pages"] as  $key=>$data)
                                        <tr>
                                            <td>{{ $data->page_id }}</td>
                                            <td>
                                                {{ $data->name }}
                                            </td>
                                            <td>
                                                {{ $data->slug }}
                                            </td>
                                            <td>
                                                @if($data->status==0)
                                                    <span class="badge badge-warning">
                                                        {{ trans('labels.InActive') }}
                                                    </span>
                                                @else
                                                    <a href="{{ URL::to("admin/pageStatus")}}?id={{ $data->page_id}}&active=no" class="method-status">
                                                        {{ trans('labels.InActive') }}
                                                    </a>
                                                @endif
                                                &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;
                                                @if($data->status==1)
                                                    <span class="badge badge-success">
                                                        {{ trans('labels.Active') }}
                                                    </span>
                                                @else
                                                    <a href="{{ URL::to("admin/pageStatus")}}?id={{ $data->page_id}}&active=yes" class="method-status">
                                                        {{ trans('labels.Active') }}
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                <a data-bs-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.Edit') }}" href="editwebpage/{{ $data->page_id }}" class="badge badge-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">{{ trans('labels.NoRecordFound') }}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="col-md-12 text-right">
                                {!! $result["pages"]->appends(\Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- deletePageModal -->
<div class="modal fade" id="deletePageModal" tabindex="-1" role="dialog" aria-labelledby="deletePageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deletePageModalLabel">{{ trans('labels.DeletePage') }}</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(array('url' =>'admin/deletepage', 'name'=>'deletePage', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeletePageDilogue') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">{{ trans('labels.Close') }}</button>
                <button type="submit" class="btn btn-primary btn-flat" id="deletePage">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
