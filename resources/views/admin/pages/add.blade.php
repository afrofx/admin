@extends('admin.layout')
@section('title-dash'){{ trans('labels.AddPage') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/pages')}}">{{ trans('labels.ListingAllPages') }}</a></li>
<li class="breadcrumb-item active">{{ trans('labels.AddPage') }}</li>
@endsection
@section('content')
<section class="content">
	{!! Form::open(['url' => 'admin/addnewpage', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<h3 class="card-title">{{ trans('labels.AddPage') }} </h3>
				</div>
				
				<div class="card-body">
					@if (count($errors) > 0)
						@foreach ($errors->all() as $error)
							<div class="alert alert-success" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
								{{ $error }}
							</div>
						@endforeach
					@endif
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="name" class="col-md-12 control-label">{{ trans('labels.PageSlug') }}</label>
								<div class="col-md-12">
									{!! Form::text('slug', '', ['class' => 'form-control field-validate', 'id' => 'slug']) !!}
									<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.pageSlugWithDashesText') }}</span>
								</div>
							</div>
						</div>
						
						@foreach ($result['languages'] as $languages)
							<div class="col-md-12">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.PageName') }} ({{ $languages->name }}) </label>
									<div class="col-md-12">
										<input type="text" name="name_<?= $languages->languages_id ?>" class="form-control field-validate">
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.PageName') }} ({{ $languages->name }})</span>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Description') }} ({{ $languages->name }}) </label>
									<div class="col-md-12">
										<textarea id="editor<?= $languages->languages_id ?>" name="description_<?= $languages->languages_id ?>" class="form-control" rows="10"></textarea>
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Description') }}({{ $languages->name }})</span>
									</div>
								</div>
							</div>
						@endforeach

						<div class="col-md-6">
							<div class="form-group">
								<label for="name" class="col-md-12 control-label">{{ trans('labels.Status') }}</label>
								<div class="col-md-12">
									<select name="status" id="status" class="form-control">
										<option value="1">{{ trans('labels.Active') }}</option>
										<option value="0">{{ trans('labels.InActive') }}</option>
									</select>
									<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusPageText') }}</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card-footer text-center">
					<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
					<a href="{{ URL::to('admin/pages') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</section>
@endsection

@section('addjs')
<script type="text/javascript">
	$(function() {
		@foreach ($result['languages'] as $languages)
			CKEDITOR.replace('editor{{ $languages->languages_id }}');
		@endforeach
		$(".textarea").wysihtml5();
	});
</script>
@endsection
