@extends('admin.layout')
@section('title-dash')
	@if ($result['shppingMethods'][0]->table_name == 'free_shipping')
		{{ trans('labels.FreeShipping') }}...
	@elseif($result['shppingMethods'][0]->table_name == 'local_pickup')
		{{ trans('labels.LocalPickup') }}...
	@elseif($result['shppingMethods'][0]->table_name == 'shipping_by_weight')
		{{ trans('labels.shppingbyweight') }}...
	@endif
@endsection

@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/shippingmethods/display')}}">{{ trans('labels.ShippingMethods') }}</a></li>
<li class="breadcrumb-item active">
	@if ($result['shppingMethods'][0]->table_name == 'free_shipping')
		{{ trans('labels.FreeShipping') }}...
	@elseif($result['shppingMethods'][0]->table_name == 'local_pickup')
		{{ trans('labels.LocalPickup') }}...
	@elseif($result['shppingMethods'][0]->table_name == 'shipping_by_weight')
		{{ trans('labels.shppingbyweight') }}...
	@endif
</li>
@endsection
@section('content')
<section class="content">
	{!! Form::open(['url' => 'admin/shippingmethods/update', 'method' => 'post', 'class' => 'form-horizontal form-validate', 'enctype' => 'multipart/form-data']) !!}
	<div class="row">
		<div class="col-md-12">
			<div class="card card-danger card-outline">
				<div class="card-header">
					<h3 class="card-title">
						<b>
							@if ($result['shppingMethods'][0]->table_name == 'free_shipping')
								{{ trans('labels.FreeShipping') }}
							@elseif($result['shppingMethods'][0]->table_name == 'local_pickup')
								{{ trans('labels.LocalPickup') }}
							@elseif($result['shppingMethods'][0]->table_name == 'shipping_by_weight')
								{{ trans('labels.shppingbyweight') }}
							@endif
						</b>
					</h3>
				</div>
				
				<div class="card-body">
					@if (count($errors) > 0)
						@if ($errors->any())
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								{{ $errors->first() }}
							</div>
						@endif
					@endif

					<div class="row">
						{!! Form::hidden('table_name', $result['shppingMethods'][0]->table_name, ['class' => 'form-control', 'id' => 'table_name']) !!}
						@foreach($result['description'] as $description_data)
							<div class="col-md-6">
								<div class="form-group">
									<label for="name" class="col-md-12 control-label">{{ trans('labels.Name') }}({{ $description_data['language_name'] }})</label>
									<div class="col-md-12">
										<input type="text" name="name_<?= $description_data['languages_id'] ?>" class="form-control field-validate" value="{{ $description_data['name'] }}">
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ShippingmethodName') }} ({{ $description_data['language_name'] }}).</span>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>

				<div class="card-footer text-center">
					<button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
					<a href="{{ URL::to('admin/shippingmethods/display') }}" type="button" class="btn btn-default btn-flat">{{ trans('labels.back') }}</a>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</section>
@endsection
