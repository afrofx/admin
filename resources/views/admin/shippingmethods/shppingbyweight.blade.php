@extends('admin.layout')
@section('title-dash')Transporte Preço por peso...@endsection
@section('title-link') 
<li class="breadcrumb-item"><a href="{{ URL::to('admin/shippingmethods/display')}}">{{ trans('labels.ShippingMethods') }}</a></li>
<li class="breadcrumb-item active">Transporte Preço por peso</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/shippingmethods/updateShppingWeightPrice', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h3 class="card-title"><b>Transporte Preço por peso</b></h3>
                    </div>
                    
                    <div class="card-body">
                        @if( count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-success" role="alert">
                                    <span class="icon fa fa-check" aria-hidden="true"></span>
                                    <span class="sr-only">{{ trans('labels.Setting') }}:</span> {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight From') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_from_0', $result['products_shipping'][0]->weight_from, array('class'=>'form-control', 'id'=>'weight_from_0')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight To') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_to_0',  $result['products_shipping'][0]->weight_to, array('class'=>'form-control', 'id'=>'weight_to_0')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight Price') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_price_0',  $result['products_shipping'][0]->weight_price, array('class'=>'form-control', 'id'=>'weight_price_0')) !!}
                                    </div>
                                </div>
                            </div>

                            <hr style="border-radius: 0.25px !important">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight From') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_from_1', $result['products_shipping'][1]->weight_from, array('class'=>'form-control', 'id'=>'weight_from_1')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight To') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_to_1',  $result['products_shipping'][1]->weight_to, array('class'=>'form-control', 'id'=>'weight_to_1')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight Price') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_price_1',  $result['products_shipping'][1]->weight_price, array('class'=>'form-control', 'id'=>'weight_price_1')) !!}
                                    </div>
                                </div>
                            </div>

                            <hr style="border-radius: 0.25px !important">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight From') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_from_2', $result['products_shipping'][2]->weight_from, array('class'=>'form-control', 'id'=>'weight_from_2')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight To') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_to_2',  $result['products_shipping'][2]->weight_to, array('class'=>'form-control', 'id'=>'weight_to_2')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight Price') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_price_2',  $result['products_shipping'][2]->weight_price, array('class'=>'form-control', 'id'=>'weight_price_2')) !!}
                                    </div>
                                </div>
                            </div>

                            <hr style="border-radius: 0.25px !important">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight From') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_from_3', $result['products_shipping'][3]->weight_from, array('class'=>'form-control', 'id'=>'weight_from_3')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight To') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_to_3',  $result['products_shipping'][3]->weight_to, array('class'=>'form-control', 'id'=>'weight_to_3')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight Price') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_price_3',  $result['products_shipping'][3]->weight_price, array('class'=>'form-control', 'id'=>'weight_price_3')) !!}
                                    </div>
                                </div>
                            </div>

                            <hr style="border-radius: 0.25px !important">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Limit Upto') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_from_4', $result['products_shipping'][4]->weight_from, array('class'=>'form-control', 'id'=>'weight_from_4')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4" style="visibility:hidden">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight To') }}</label>
                                    <div  class="col-md-12">
                                        {!! Form::text('weight_to_4',  $result['products_shipping'][4]->weight_to, array('class'=>'form-control', 'id'=>'weight_to_4')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">{{ trans('labels.Weight Price') }}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('weight_price_4',  $result['products_shipping'][4]->weight_price, array('class'=>'form-control', 'id'=>'weight_price_4')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{ URL::to('admin/shippingmethods/display')}}" type="button" class="btn btn-default pull-left btn-flat"> <i class="fa fa-angle-left"></i> {{ trans('labels.back') }}</a>
                        <button type="submit" class="btn btn-primary pull-right btn-flat">{{ trans('labels.Submit') }}</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</section>
@endsection
