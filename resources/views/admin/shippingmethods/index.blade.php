@extends('admin.layout')
@section('title-dash'){{ trans('labels.ShippingMethods') }}...@endsection
@section('title-link') 
<li class="breadcrumb-item active">{{ trans('labels.ShippingMethods') }}</li>
@endsection
@section('content')
<section class="content">
    {!! Form::open(array('url' =>'admin/updateSetting', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h4 class="card-title"><b>Preco minimo de frete grátis</b></label>
                    </div>

                    <div class="card-body">
                        @if (count($errors) > 0)
                            @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                            @endif
                        @endif
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        {!! Form::text('free_shipping_limit', $result['commonContent']['setting']['free_shipping_limit'], array('class'=>'form-control', 'id'=>'free_shipping_limit')) !!}
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;margin-top: 0;">O frete sera gratis se as compras ultrapassarem este valor</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-flat">{{ trans('labels.Submit') }}</button>
                            </div>
                        </div>
                    </div>                     
               </div>
            </div>
        </div>
    {!! Form::close() !!}

    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">

                <div class="card-header">
                    <h3 class="card-title"><b>{{ trans('labels.ShippingMethods') }}</b> </h3>
                </div>
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped" style="text-align: center;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">{{ trans('labels.Default') }}</th>
                                        <th style="text-align: center;">Tipo de Envio</th>
                                        <th style="text-align: center;">{{ trans('labels.Price') }}</th>
                                        <th style="text-align: center;">{{ trans('labels.Status') }}</th>
                                        <th style="text-align: center;">{{ trans('labels.Action') }}</th>
                                        <th style="text-align: center;">Gerir Peso</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($result['shipping_methods'] as $key=>$shipping_methods)
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="shipping_methods_id" value="1" shipping_id = '{{ $shipping_methods->shipping_methods_id}}' class="default_method" @if($shipping_methods->isDefault==1) checked @endif >
                                                </label>
                                            </td>

                                            <td>
                                                {{ $shipping_methods->name }}
                                            </td>

                                            @if($shipping_methods->methods_type_link=='upsShipping' and $shipping_methods->shipping_methods_id=='1')

                                                <td>---</td>

                                                <td>
                                                    @if($shipping_methods->status==0)
                                                        <span class="badge badge-warning">
                                                            {{ trans('labels.InActive') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=no" class="method-status">
                                                            {{ trans('labels.InActive') }}
                                                        </a>
                                                    @endif

                                                    &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;
                                                    
                                                    @if($shipping_methods->status==1)
                                                        <span class="badge badge-success">
                                                            {{ trans('labels.Active') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=yes" class="method-status">
                                                            {{ trans('labels.Active') }}
                                                        </a>
                                                    @endif
                                                </td>
                                                <td><a href="{{ $shipping_methods->methods_type_link }}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                </td>
                                            @endif

                                            @if($shipping_methods->methods_type_link=='freeShipping' and $shipping_methods->shipping_methods_id=='2')
                                                <td>---</td>
                                                <td>
                                                    @if($shipping_methods->status==0)
                                                        <span class="badge badge-warning">
                                                            {{ trans('labels.InActive') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=no" class="method-status">
                                                            {{ trans('labels.InActive') }}
                                                        </a>
                                                    @endif
                                                    &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;
                                                    @if($shipping_methods->status==1)
                                                        <span class="badge badge-success">
                                                            {{ trans('labels.Active') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=yes" class="method-status">
                                                            {{ trans('labels.Active') }}
                                                        </a>
                                                    @endif
                                                </td>
                                                <td><a href="{{ URL::to("admin/shippingmethods/detail/".$shipping_methods->table_name)}}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                            @endif

                                            @if($shipping_methods->methods_type_link=='localPickup' and $shipping_methods->shipping_methods_id=='3')
                                                <td>---</td>
                                                <td>
                                                    @if($shipping_methods->status==0)
                                                        <span class="badge badge-warning">
                                                            {{ trans('labels.InActive') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=no" class="method-status">
                                                            {{ trans('labels.InActive') }}
                                                        </a>
                                                    @endif
                                                    &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;
                                                    @if($shipping_methods->status==1)
                                                        <span class="badge badge-success">
                                                            {{ trans('labels.Active') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=yes" class="method-status">
                                                            {{ trans('labels.Active') }}
                                                        </a>
                                                    @endif
                                                </td>
                                                <td><a href="{{ URL::to("admin/shippingmethods/detail/".$shipping_methods->table_name)}}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                            @endif

                                            @if($shipping_methods->methods_type_link=='flateRate' and $shipping_methods->shipping_methods_id=='4')
                                                <td>{{ $result['flate_rate']['flate_rate']->currency }}{{ $result['flate_rate']['flate_rate']->flate_rate }} </td>
                                                <td>
                                                    @if($shipping_methods->status==0)
                                                        <span class="badge badge-warning">
                                                            {{ trans('labels.InActive') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=no" class="method-status">
                                                            {{ trans('labels.InActive') }}
                                                        </a>
                                                    @endif
                                                    &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;
                                                    @if($shipping_methods->status==1)
                                                        <span class="badge badge-success">
                                                            {{ trans('labels.Active') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=yes" class="method-status">
                                                            {{ trans('labels.Active') }}
                                                        </a>
                                                    @endif
                                                </td>
                                                <td><a href="{{ $shipping_methods->methods_type_link }}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                </td>
                                            @endif

                                            @if($shipping_methods->methods_type_link=='shippingByWeight' and $shipping_methods->shipping_methods_id=='5')

                                                <td>---</td>
                                                <td>
                                                    @if($shipping_methods->status==0)
                                                        <span class="badge badge-warning">
                                                            {{ trans('labels.InActive') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=no" class="method-status">
                                                            {{ trans('labels.InActive') }}
                                                        </a>
                                                    @endif
                                                    &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;
                                                    @if($shipping_methods->status==1)
                                                        <span class="badge badge-success">
                                                            {{ trans('labels.Active') }}
                                                        </span>
                                                    @else
                                                        <a href="{{ URL::to("admin/shippingmethods/display")}}?id={{ $shipping_methods->shipping_methods_id}}&active=yes" class="method-status">
                                                            {{ trans('labels.Active') }}
                                                        </a>
                                                    @endif
                                                </td>
                                                <td><a href="{{ URL::to("admin/shippingmethods/detail/".$shipping_methods->table_name)}}" class="btn btn-primary btn-flat"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                                <td><a href="{{ URL::to('admin/shippingmethods/shppingbyweight/')}}" class="btn btn-default btn-flat">{{ trans('labels.Manage Weight') }}</a></td> 
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
